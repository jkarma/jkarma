/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.model;

import java.time.Instant;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;


/**
 * This class implements a temporal labeled graph of labeled edges.
 * A graph of this type possess labeled edges between String nodes.
 * Furthermore, the class implements the Transaction interface since every
 * temporal labeled graph can be seen as a transaction of labeled edges.
 * Moreover a total temporal ordering between temporal graph is implemented
 * via the Comparable interface.  
 * @author Angelo Impedovo
 *
 */
public class TemporalGraph implements Graph<String>, Transaction<LabeledEdge>, Comparable<TemporalGraph>{

	/**
	 * The id associated to the temporal graph.
	 */
	private int id;
	
	
	
	/**
	 * The timestamp at which the temporal graph has been observed.
	 */
	private Instant timestamp;
	
	
	
	/**
	 * The set of labeled edges of the temporal graph.
	 */
	private Set<LabeledEdge> edges;
	
	
	/**
	 * Builds a temporal labeled graph given its id, timestamp and collection of labeled edges.
	 * @param id The id of the temporal graph.
	 * @param timestamp The timestamp at which the temporal graph has been observed.
	 * @param edges The collection of labeled edges.
	 */
	public TemporalGraph(int id, Instant timestamp, Collection<? extends LabeledEdge> edges) {
		this.id = id;
		this.timestamp = timestamp;
		this.edges = new TreeSet<LabeledEdge>(edges);
	}

	
	
	/**
	 * Returns the timestamp at which the temporal graph has been recorded.
	 * @return The timestamp at which the temporal graph has been recorded.
	 */
	@Override
	public Instant getTimestamp() {
		return this.timestamp;
	}

	
	
	/**
	 * Returns the items (labeled edges) in the temporal graph. 
	 * @return The items (labeled edgeS) in the temporal graph. 
	 */
	@Override
	public Set<LabeledEdge> getItems() {
		return this.edges;
	}

	
	
	/**
	 * Returns the temporal graph id.
	 * @return The temporal graph id.
	 */
	@Override
	public int getId() {
		return this.id;
	}

	
	
	/**
	 * Returns the set of edges from the graph.
	 * @return The set of edges from the graph.
	 */
	@Override
	public Set<? extends Edge<String>> getEdges() {
		return this.edges;
	}

	
	
	/**
	 * Returns the set of nodes from the graph.
	 * @return The set of nodes from the graph.
	 */
	@Override
	public Set<String> getNodes() {
		throw new UnsupportedOperationException();
	}

	
	
	/**
	 * Compares two temporal graphs resorting to the temporal ordering
	 * of their timestamp.
	 * @param g The temporal graph to be compared.
	 * @return -1 if this graph has been recorded before g, 0 if they
	 *  have been recorded with same timestamp, 1 otherwise.
	 */
	@Override
	public int compareTo(TemporalGraph g) {
		int result = 0;
		if(this.timestamp.isAfter(g.timestamp)) {
			return 1;
		}else if(this.timestamp.isBefore(g.timestamp)) {
			return -1;
		}
		return result;
	}

	
	
	/**
	 * Returns an iterator over the labeled edges.
	 * @return An iterator over the labeled edges.
	 */
	@Override
	public Iterator<LabeledEdge> iterator() {
		return this.edges.iterator();
	}
	
	
	
	/**
	 * Returns the string representation of the temporal graph.
	 * @return The string representation of the temporal graph.
	 */
	@Override
	public String toString() {
		return "TemporalGraph{timestamp="+this.timestamp+", id="+this.id+", #edges="+this.edges.size()+"}";
	}

}

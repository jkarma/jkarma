/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.model;

import java.time.Instant;
import java.util.Date;

import com.univocity.parsers.annotations.Format;
import com.univocity.parsers.annotations.Parsed;


/**
 * Implements a temporal edge object as a labeled edge with timestamp.
 * This class extends the LabeledEdge class and implements the Timeable interface.
 * @author Angelo Impedovo
 * @see Timeable
 * @see LabeledEdge
 */
public class TemporalEdge extends LabeledEdge implements Timeable {
	
	
	/**
	 * The timestamp associated to the temporal edge.
	 */
	@Format(formats = {"yyyy-MM-dd HH:mm:ss:SS Z"})
	@Parsed(index = 0)
	public Date timestamp;
	
	public TemporalEdge() {
		
	}
	
	/**
	 * Builds a labeled temporal edge given the timestamp, the two nodes and the label.
	 * @param timestamp The timestamp of the edge.
	 * @param startNode The first node connected by the edge.
	 * @param endNode The second node connected by the edge.
	 * @param label The edge label.
	 */
	public TemporalEdge(Date timestamp, String startNode, String endNode, String label) {
		super(startNode, endNode, label);
		this.timestamp = timestamp;
	}
	

	
	/**
	 * Returns the timestamp associated to this temporal edge.
	 * @return The timestamp associated to this temporal edge.
	 */
	public Instant getTimestamp() {
		return this.timestamp.toInstant();
	}

}

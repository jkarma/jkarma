/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.model;

import com.univocity.parsers.annotations.Parsed;

/**
 * This class implements a labeled edge between nodes in a graph.
 * The edge connects two String nodes, and possess a String label.
 * For these reasons the Edge&lt;String&gt; and the Labelable interfaces are implemented.
 * Also a total lexicographic order is imposed via the Comparable interface.
 * @author Angelo Impedovo
 */
public class LabeledEdge implements Edge<String>, Labelable, Comparable<LabeledEdge>{
	
	/**
	 * The edge label.
	 */
	@Parsed(index = 2)
	public String label;
	
	
	
	/**
	 * The first node of the edge.
	 */
	@Parsed(index = 1)
	public String startNode;
	
	
	
	/**
	 * The second node of the edge.
	 */
	@Parsed(index = 3)
	public String endNode;
	
	
	
	/**
	 * Convenience status flag denoting connected subgraphs or subtrees.
	 */
	public boolean connected;
	
	
	/**
	 * Builds a labeled edge.
	 */
	public LabeledEdge() {
		this.connected = true;
	}

	
	/**
	 * Builds a labeled edge by copy-construction.
	 * @param u The labeled edge to be cloned.
	 */
	public LabeledEdge(LabeledEdge u) {
		this(u.startNode, u.endNode, u.label);
	}
	
	
	
	/**
	 * Builds a labeled edge given the nodes and the label.
	 * @param startNode The first node.
	 * @param endNode The second node.
	 * @param label The edge label.
	 */
	public LabeledEdge(String startNode, String endNode, String label) {
		this.startNode = startNode;
		this.endNode = endNode;
		this.label = label;
		this.connected = true;
	}

	
	
	/**
	 * Returns the edge label.
	 * @return The edge label.
	 */
	public String getLabel() {
		return this.label;
	}

	
	
	/**
	 * Sets the edge label.
	 * @param label The label to be set.
	 */
	public void setLabel(String label) {
		if(label==null) {
			throw new NullPointerException();
		}
		this.label = label;
	}

	
	
	/**
	 * Returns the first node of the edge.
	 * @return The first node of the edge.
	 */
	public String getStartNode() {
		return this.startNode;
	}

	
	
	/**
	 * Returns the second node of the edge.
	 * @return The second node of the edge.
	 */
	public String getEndNode() {
		return this.endNode;
	}

	
	
	/**
	 * Returns the string representation of the labeled edge.
	 * @return The string representation of the labeled edge.
	 */
	@Override
	public String toString() {
		return "e("+startNode+", "+label+", "+endNode+")";
	}


	
	/**
	 * Compares this labeled edge to another with respect to the
	 * lexicographic total ordering.
	 * The two edges are seen as the two triples (n1,label,n2), the lexicographic
	 * ordering between triples is built by exploiting the string lexicographic
	 * ordering from the left to right.
	 * @param o The labeled edge to be compared.
	 * @return -1 if this edge is lexicographically lower than o, 0 if they are
	 *  equal, 1 otherwise. 
	 */
	@Override
	public int compareTo(LabeledEdge o) {
		int result = this.startNode.compareTo(o.startNode);
		if(result==0) {
			result = this.label.compareTo(o.label);
			if(result==0) {
				result = this.endNode.compareTo(o.endNode);
			}
		}
		return result;
	}
	
	
	
	/**
	 * Checks the equality between two labeled edges.
	 * This method is coherent with the compareTo method.
	 * @param o The labeled edge to be compared.
	 * @return True if the two labeled edges are equals, false otherwise.
	 */
	@Override
	public boolean equals(Object o) {
		boolean result = false;
		if(o instanceof LabeledEdge) {
			LabeledEdge edge = (LabeledEdge)o;
			result = (this.compareTo(edge)==0);
		}else {
			throw new ClassCastException();
		}
		
		return result;
	}

	
	
	/**
	 * Evaluates if this edge is connected to another edge.
	 * Two edges are connected if they have at least a node in common.
	 * @param edge The edge to be tested
	 * @return True if the two edge are connected, false otherwise.
	 */
	@Override
	public boolean isLinkedTo(Edge<String> edge) {
		String startNode = edge.getStartNode();
		String endNode = edge.getEndNode();
		boolean result = (
			this.startNode.equals(startNode) || 
			this.startNode.equals(endNode) ||
			this.endNode.equals(startNode) ||
			this.endNode.equals(endNode)
		);
			
		return result;
	}

	
	
	/**
	 * Returns true if the edge is reflexive, false otherwise.
	 * @return True if the edge is reflexive, false otherwise
	 */
	@Override
	public boolean isSelfLoop() {
		return startNode.equals(endNode);
	}
	
	
	
	/**
	 * Evaluates if this edge share the first node.
	 * @param edge The edge to be tested
	 * @return True if the two edges share the first node, false otherwise.
	 */
	@Override
	public boolean isLinkedByStartNode(Edge<String> edge) {
		String startNode = edge.getStartNode();
		return this.startNode.equals(startNode);
	}

	
	
	/**
	 * Evaluates if this edge share the second node.
	 * @param edge The edge to be tested
	 * @return True if the two edges share the second node, false otherwise.
	 */
	@Override
	public boolean isLinkedByEndNode(Edge<String> edge) {
		String endNode = edge.getStartNode();
		return this.endNode.equals(endNode);
	}

}

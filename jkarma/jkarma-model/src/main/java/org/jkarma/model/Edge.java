/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.model;

/**
 * Defines the public interface for edge between nodes in a graph.
 * In graph theory an edge is an entity which connects two nodes, of type N, in a graph.
 * @author Angelo Impedovo
 *
 * @param <N> Type of nodes to be connected by the edge
 */
public interface Edge<N>{
	
	/**
	 * Returns the first node of the edge.
	 * @return The first node of the edge.
	 */
	public N getStartNode();
	
	
	
	/**
	 * Returns the second node of the edge.
	 * @return The second node of the edge.
	 */
	public N getEndNode();
	
	
	
	/**
	 * Evaluates if this edge is connected to another edge.
	 * Two edges are connected if they have at least a node in common.
	 * @param edge The edge to be tested
	 * @return True if the two edge are connected, false otherwise.
	 */
	public boolean isLinkedTo(Edge<N> edge);
	
	
	
	/**
	 * Returns true if the edge is reflexive, false otherwise.
	 * @return True if the edge is reflexive, false otherwise.
	 */
	public boolean isSelfLoop();
	
	
	
	/**
	 * Evaluates if this edge share the first node.
	 * @param edge The edge to be tested
	 * @return True if the two edges share the first node, false otherwise.
	 */
	public boolean isLinkedByStartNode(Edge<N> edge);
	
	
	
	/**
	 * Evaluates if this edge share the second node.
	 * @param edge The edge to be tested
	 * @return True if the two edges share the second node, false otherwise.
	 */
	public boolean isLinkedByEndNode(Edge<N> edge);
	
}

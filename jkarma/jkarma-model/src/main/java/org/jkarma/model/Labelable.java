/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.model;

/**
 * Public interface for objects which can possess a text label.
 * @author Angelo Impedovo
 */
public interface Labelable {

	
	
	/**
	 * Returns the label associated to the object.
	 * @return The label associated to the object.
	 */
	public String getLabel();
	
	
	
	/**
	 * Assigns the label to the object.
	 * @param label The label to be associated to the object.
	 */
	public void setLabel(String label);
	
}

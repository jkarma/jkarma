/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.model;

import java.util.Set;

/**
 * Defines the public interface of a graph object.
 * A graph object is made of nodes (of type B) and edges connecting them.
 * The most general abstraction of graph objects prescribe the Edge type
 * to be unknown, the only requirement is that it implements the Edge
 * interface.
 * @author Angelo Impedovo
 *
 * @param <B> Type of nodes in the graph
 */
public interface Graph<B> {
	
	/**
	 * Returns the set of edges from the graph.
	 * @return The set of edges from the graph.
	 */
	public Set<? extends Edge<B>> getEdges();
	
	/**
	 * Returns the set of nodes from the graph.
	 * @return The set of nodes from the graph.
	 */
	public Set<B> getNodes();

}

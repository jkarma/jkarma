/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.model;

import java.time.Instant;

/**
 * The public interface for objects that can be associated
 * with a specific temporal point.
 * @author Angelo Impedovo
 */
public interface Timeable {
	
	/**
	 * Returns the timestamp associated to this object.
	 * @return The timestamp associated to this object.
	 */
	public Instant getTimestamp();

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.model;

import java.time.Instant;
import java.util.Collection;


/**
 * Public interface of a transactional object.
 * The concept of transaction is relevant in several disciplines,
 * in general, it is defined as a uniquely identified collection of
 * items.
 * Every transaction is identified by an id and can be temporally
 * located at a particular time point.
 * @author Angelo Impedovo
 *
 * @param <A> Type of the items in this transaction.
 */
public interface Transaction<A> extends Iterable<A> {
	
	/**
	 * Returns the timestamp at which the transaction has been recorded.
	 * @return The timestamp at which the transaction has been recorded.
	 */
	public Instant getTimestamp();
	
	
	/**
	 * Returns the items in the transaction. 
	 * @return Tthe items in the transaction.
	 */
	public Collection<A> getItems();
	
	
	/**
	 * Returns the transaction id.
	 * @return The transaction id.
	 */
	public int getId();
}

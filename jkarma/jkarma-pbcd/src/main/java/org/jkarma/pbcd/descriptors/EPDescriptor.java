/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.descriptors;

import java.util.stream.Stream;

import org.jkarma.pbcd.patterns.Pattern;
import org.jkarma.pbcd.patterns.Patterns;
import org.jkarma.mining.joiners.FrequencyEvaluation;

/**
 * Change descriptor which characterize the changes by selecting the
 * emerging patterns (EPs).
 * Emerging patterns are patterns whose support significantly differs
 * between two time windows. A pattern is said to be emerging between
 * two time windows w1 and w2 if his growth-rate (ratio of the supports
 * in w1 and w2) exceeds a minimum growth-rate.
 * @author Angelo Impedovo
 *
 * @param <A> Type defining the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 */
public class EPDescriptor<A extends Comparable<A>,B extends FrequencyEvaluation> implements Descriptor<A,B>{
	
	/**
	 * The minimum growth rate.
	 */
	private double minGr;
	
	
	
	/**
	 * Constructs a new EPDescriptor given a growth-rate threshold.
	 * @param minGr The minimum growth-rate threshold.
	 */
	public EPDescriptor(double minGr) {
		this.setMinGr(minGr);
	}
	
	
	
	/**
	 * Sets the minimum growth-rate of the descriptor.
	 * @param minGr The minimum growth-rate to be associated.
	 */
	public void setMinGr(double minGr) {
		if(minGr < 0) {
			throw new IllegalArgumentException();
		}
		this.minGr = minGr;
	}
	
	
	
	/**
	 * Returns the minimum growth-rate associated to the descriptor.
	 * @return The minimum growth-rate associated to the descriptor.
	 */
	public double getMinGr() {
		return this.minGr;
	}

	
	/**
	 * Filters only the emerging patterns from the stream of patterns.
	 */
	@Override
	public Stream<Pattern<A, B>> apply(Stream<Pattern<A, B>> stream) {
		return stream.filter(p -> Patterns.isEmerging(p, minGr));
	}
}

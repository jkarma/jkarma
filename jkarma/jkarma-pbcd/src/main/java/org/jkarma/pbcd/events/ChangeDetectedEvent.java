/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.events;

import java.util.stream.Stream;

import org.jkarma.pbcd.datastreams.Block;
import org.jkarma.pbcd.datastreams.TimeWindow;
import org.jkarma.pbcd.patterns.Pattern;
import org.jkarma.model.Transaction;

/**
 * This class implements the event-related data associated to the detection of a change
 * as result of the change detection and change characterization steps in a PBCD pipeline.
 * The change detection step happens after the pattern mining step and before the
 * change characterization step. 
 * In particular, this event is raised when a change has been detected and characterized, 
 * that is when both the change detection and the change characterization steps have been
 * completed.
 * @author Angelo Impedovo
 * 
 * @param <A> Type expressing the alphabet of allowed pattern symbols in the PBCD.
 * @param <B> Type expressing the evaluation of pattern in the PBCD.
 */
public class ChangeDetectedEvent<A extends Comparable<A>, B> extends PBCDEvent<A,B> {
	
	/**
	 * The change score associated to the detected change.
	 * @see Similarity
	 */
	private double changeAmount;
	
	
	
	/**
	 * A set of patterns which best characterize the detected change.
	 * @see Descriptor
	 */
	private Stream<Pattern<A,B>> changeDescription;
	
	
	
	/**
	 * Constructs an event considering a reference window and an incoming block of 
	 * transactions.
	 * Also, the event is annotated with the change score and the description model 
	 * of the detected change. 
	 * 
	 * @param landmark The reference window.
	 * @param latestBlock The last block of incoming transactions.
	 * @param amount The amount of change.
	 * @param patterns Set of patterns which best describe the detected change.
	 */
	public ChangeDetectedEvent(TimeWindow landmark, Block<? extends Transaction<A>> latestBlock, 
			double amount, Stream<Pattern<A,B>> patterns) {
		super(landmark, latestBlock);
		this.changeAmount = amount;
		this.changeDescription = patterns;
	}
	
	
	
	/**
	 * Returns the change score associated with this event.
	 * @return The change score associated with this event.
	 */
	public double getAmount() {
		return this.changeAmount;
	}
	
	
	
	/**
	 * Returns the set of patterns which best describe the change associated with this event.
	 * @return The set of patterns which best describe the change associated with this event.
	 */
	public Stream<Pattern<A,B>> getDescription(){
		return this.changeDescription;
	}
	
}

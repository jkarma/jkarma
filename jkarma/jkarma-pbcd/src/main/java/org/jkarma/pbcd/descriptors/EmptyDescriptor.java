/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.descriptors;

import java.util.stream.Stream;

import org.jkarma.pbcd.patterns.Pattern;


/**
 * Change descriptor which characterize the changes with an empty set of patterns.
 * @author Angelo Impedovo
 *
 * @param <A> Type defining the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 */
public class EmptyDescriptor<A extends Comparable<A>, B> implements Descriptor<A,B> {

	/**
	 * Returns an empty stream of patterns.
	 */
	@Override
	public Stream<Pattern<A, B>> apply(Stream<Pattern<A, B>> t) {
		return Stream.empty();
	}

}

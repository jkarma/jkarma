/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.descriptors;

import java.util.stream.Stream;

import org.jkarma.pbcd.patterns.Pattern;
import org.jkarma.pbcd.patterns.Patterns;
import org.jkarma.mining.joiners.FrequencyEvaluation;

/**
 * Change descriptor which characterize the changes by partially selecting the
 * emerging patterns (EPs) among the patterns who became frequent and infrequent
 * between two time windows.
 * The descriptor extends EPDescriptor because it offers the same functionality,
 * with the exception of limiting the search space of patterns only to those
 * whose support has crossed the minimum support threshold.
 * @author Angelo Impedovo
 *
 * @param <A> Type defining the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 */
public class PartialEPDescriptor<A extends Comparable<A>,B extends FrequencyEvaluation> extends EPDescriptor<A,B>{
	
	
	/**
	 * The minimum support threshold.
	 */
	private double minSup;

	
	
	/**
	 * Constructs a new EPPartialDescriptor given a support and a 
	 * growth-rate threshold.
	 * @param minSup The minimum support threshold.
	 * @param minGr The minimum growth-rate threshold.
	 */
	public PartialEPDescriptor(double minSup, double minGr) {
		super(minGr);
		if(minSup<0 || minSup>1) {
			throw new IllegalArgumentException();
		}
		this.minSup = minSup;
	}
	
	
	
	/**
	 * Sets the minimum support of the descriptor.
	 * @param minSup The minimum support to be associated.
	 */
	public void setMinSup(double minSup) {
		if(minSup<0 || minSup>1) {
			throw new IllegalArgumentException();
		}
		this.minSup = minSup;
	}
	
	
	
	
	/**
	 * Returns the minimum support associated to the descriptor.
	 * @return The minimum support associated to the descriptor.
	 */
	public double getMinSup() {
		return this.minSup;
	}

	
	
	/**
	 * Filters the emerging patterns whose support has crossed the minimum
	 * threshold from the stream of patterns.
	 */
	@Override
	public Stream<Pattern<A, B>> apply(Stream<Pattern<A, B>> stream) {
		return stream.filter(p -> Patterns.hasCrossedMinimumSupport(p, minSup))
				.filter(p -> Patterns.isEmerging(p, super.getMinGr()));
	}	
	
}

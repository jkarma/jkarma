/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.events;

import org.jkarma.pbcd.datastreams.Block;
import org.jkarma.pbcd.datastreams.TimeWindow;
import org.jkarma.model.Transaction;


/**
 * This class implements the event-related data associated to the failure in the detection 
 * of a change as result of the change detection step in a PBCD pipeline.
 * The change detection step happens after the pattern mining step and before the
 * change characterization step. 
 * In particular, this event is raised when a change has been detected, that is when the 
 * change detection has been completed.
 * @author Angelo Impedovo
 * 
 * @param <A> Type expressing the alphabet of allowed pattern symbols in the PBCD.
 * @param <B> Type expressing the evaluation of pattern in the PBCD.
 */
public class ChangeNotDetectedEvent<A extends Comparable<A>,B> extends PBCDEvent<A,B>{

	/**
	 * The change score associated to this event.
	 * @see Similarity
	 */
	private double changeAmount;
	
	
	
	/**
	 * Constructs an event considering a reference window and an incoming block of 
	 * transactions.
	 * Also, the event is annotated with the change score. 
	 * 
	 * @param landmark The reference window.
	 * @param latestBlock The last block of incoming transactions.
	 * @param amount The amount of change.
	 */
	public ChangeNotDetectedEvent(TimeWindow landmark, Block<? extends Transaction<A>> latestBlock, double amount) {
		super(landmark, latestBlock);
		this.changeAmount = amount;
	}
	
	
	
	/**
	 * Returns the change score associated with this event.
	 * @return The change score associated with this event.
	 */
	public double getAmount() {
		return this.changeAmount;
	}
	
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.similarities;

import java.util.Iterator;
import java.util.Vector;

/**
 * This class defines a weighted jaccard dissimilarity between two vectors
 * of Double. The weighted jaccard dissimilarity is defined as the 1-complement
 * of the associated weighted jaccard similarity, which is defined as the
 * ratio between the sum of the pairwise minimum elements, and the sum of the pairwise
 * maximum elements of the two vectors.
 * @author Angelo Impedovo
 */
public class WeightedJaccard implements Similarity<Double> {

	/**
	 * Computes the weighted jaccard dissimilarity score between two vectors u and v.
	 * @param u the first vector.
	 * @param v the second vector.
	 * @return the weighted jaccard dissimilarity score between u and v.
	 */
	@Override
	public Double apply(Vector<Double> u, Vector<Double> v) {
		Iterator<Double> uIterator = u.iterator();
		Iterator<Double> vIterator = v.iterator();
		double sumMin = 0;
		double sumMax = 0;
		double result = 0;
		
		while(uIterator.hasNext()) {
			double uIth = uIterator.next();
			double vIth = vIterator.next();
			sumMin += Math.min(uIth, vIth);
			sumMax += Math.max(uIth, vIth);
		}
		
		if(sumMin==0 && sumMax==0) {
			result = 0;
		}else {
			result = sumMin/sumMax;
		}
		
		return 1 - result;
	}

}

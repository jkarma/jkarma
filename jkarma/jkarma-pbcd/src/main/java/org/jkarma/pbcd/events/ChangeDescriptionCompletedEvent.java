/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.events;

import org.jkarma.pbcd.datastreams.Block;
import org.jkarma.pbcd.datastreams.TimeWindow;
import org.jkarma.model.Transaction;

/**
 * This class implements the event-related data associated to the completion
 * of the change characterization step in a PBCD pipeline.
 * The change characterization step happens after the mining step and the change 
 * detection step, in particular this event is raised when a change has been 
 * detected and characterized.
 * @author Angelo Impedovo
 * 
 * @param <A> Type expressing the alphabet of allowed pattern symbols in the PBCD.
 * @param <B> Type expressing the evaluation of pattern in the PBCD.
 */
public class ChangeDescriptionCompletedEvent<A extends Comparable<A>, B> extends PBCDEvent<A,B> {

	/**
	 * Constructs an event considering a reference window and an incoming block of 
	 * transactions.
	 * @param landmark The reference window.
	 * @param latestBlock The last block of incoming transactions.
	 */
	public ChangeDescriptionCompletedEvent(TimeWindow landmark, Block<? extends Transaction<A>> latestBlock) {
		super(landmark, latestBlock);
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.detectors;

import org.jkarma.mining.structures.MiningStrategy;

/**
 * Convenience class serving as the main facility for defining
 * customized pattern-based change detectors (PBCDs).
 * @author Angelo Impedovo
 */
public class Detectors {
	
	/**
	 * This method builds a DetectorProxy generic object
	 * which acts as a proxy for the definition of a fully functional PBCD.
	 * In particular this method defines the skeleton of a PBCD relying on a
	 * a MiningStrategy object built with jMiner.
	 * The mining strategy will be used in the mining step of the PBCD to
	 * discover patterns. 
	 * The patterns will be used as multivariate features 
	 * in the detection step of the PBCD pipeline.
	 * 
	 * @see MiningStrategy
	 * @param <B> Type of allowed pattern symbols.
	 * @param <C> Type of pattern evaluations.
	 * @param miningStrategy The mining strategy used to discover patterns
	 *  useful to the PBCD pipeline.
	 * @return The DetectorProxy object to be finalized for constructing the PBCD.
	 */
	public static <B extends Comparable<B>, C>
	DetectorProxy<B,C,?> upon(MiningStrategy<B,C> miningStrategy){
		return new DetectorProxy<B,C,Object>(miningStrategy);
	}

}

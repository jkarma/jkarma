/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.descriptors;

import java.util.stream.Stream;

import org.jkarma.pbcd.patterns.Pattern;
import org.jkarma.pbcd.patterns.Patterns;
import org.jkarma.mining.joiners.FrequencyEvaluation;

/**
 * Change descriptor which characterize the changes by partially selecting the
 * maximal emerging patterns (MEPs) among the patterns who became frequent and infrequent
 * between two time windows.
 * The descriptor extends PartialEPDescriptor because it offers the same functionality,
 * with the exception of considering the maximality of patterns.
 * @author Angelo Impedovo
 *
 * @param <A> Type defining the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 */
public class PartialMEPDescriptor<A extends Comparable<A>,B extends FrequencyEvaluation> extends PartialEPDescriptor<A,B>{
	
	
	/**
	 * Flag indicating if EPs should be discovered from maximal patterns
	 * or vice-versa.
	 */
	private boolean deferred;

	
	
	/**
	 * Constructs a new PartialMEPDescriptor given the minimum support and
	 * growth-rate thresholds.
	 * @param minSup The minimum support threshold.
	 * @param minGr The minimum growth-rate threshold.
	 * @param deferred If true the maximality is evaluated after the growth-rate, false otherwise.
	 */
	public PartialMEPDescriptor(double minSup, double minGr, boolean deferred) {
		super(minSup, minGr);
		this.deferred = deferred;
	}

	
	
	/**
	 * Filters the maximal emerging patterns (according to the deferred flag) whose 
	 * support has crossed the minimum threshold from the stream of patterns.
	 */
	@Override
	public Stream<Pattern<A, B>> apply(Stream<Pattern<A, B>> patterns) {
		Stream<Pattern<A,B>> stream = patterns.filter(p -> Patterns.hasCrossedMinimumSupport(p, super.getMinSup()))
				.filter(p -> Patterns.isEmerging(p, super.getMinGr()));
		
		if(!this.deferred) {
			//the evaluation of maximality is NOT done contextually to the emergence
			stream = stream.filter(pattern -> Patterns.isMaximalByStorage(pattern, this.getMinSup()));
		}else {
			//the evaluation of maximality is done contextually to the emergence
			stream = stream.filter(pattern -> this.isDeferredMaximal(pattern));
		}
		
		return stream;
	}
	
	
	
	/**
	 * Set this descriptor as deferred or not.
	 * @param deferred Status flag to be set.
	 */
	public void setDeferred(boolean deferred) {
		this.deferred = deferred;
	}
	
	
	/**
	 * Returns the deferred status flag of this descriptor.
	 * @return The deferred status flag of this descriptor.
	 */
	public boolean isDeferred() {
		return this.deferred;
	}
	
	
	
	private boolean isDeferredMaximal(Pattern<A,B> pattern) {
		boolean isDeferredMaximal = true;
		boolean posEmerge = (pattern.getFirstEval().getRelativeFrequency() < pattern.getSecondEval().getRelativeFrequency());
		boolean negEmerge = (pattern.getFirstEval().getRelativeFrequency() > pattern.getSecondEval().getRelativeFrequency());
		int emergingSuperCount = 0;

		for(Pattern<A,B> superPattern : pattern.getLattice().getLowerElements(pattern)){
			double superGr = Patterns.getGrowthRate(superPattern);
			
			if(negEmerge) {
				//negative emergency test on the superset
				if(superGr > this.getMinGr()) {
					emergingSuperCount++;
				}
			}else if(posEmerge){
				//positive emergency test on the superset
				if(superGr > this.getMinGr()) {
					emergingSuperCount++;
				}
			}
		}
		
		//maximality condition
		isDeferredMaximal = (emergingSuperCount == 0);

		return isDeferredMaximal;
	}

}

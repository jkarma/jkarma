/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.descriptors;

import java.util.function.Function;
import java.util.stream.Stream;

import org.jkarma.pbcd.patterns.Pattern;

/**
 * Public interface for components defining the change characterization step of a PBCD pipeline
 * The change characterization step arranges a descriptive model, in form of set of patterns,
 * for every change that has been detected.
 * In particular, the change characterization step selects the set of patterns which best
 * describe every detected change.
 * @author Angelo Impedovo
 *
 * @param <A> Type defining the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 */
public interface Descriptor<A extends Comparable<A>, B> extends Function<Stream<Pattern<A,B>>, Stream<Pattern<A,B>>> {
	
}

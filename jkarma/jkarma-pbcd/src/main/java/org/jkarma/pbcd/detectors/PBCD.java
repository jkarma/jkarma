/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.detectors;

import java.util.Vector;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.jkarma.pbcd.datastreams.Block;
import org.jkarma.pbcd.datastreams.TimeWindow;
import org.jkarma.pbcd.descriptors.Descriptor;
import org.jkarma.pbcd.events.ChangeDescriptionCompletedEvent;
import org.jkarma.pbcd.events.ChangeDescriptionStartedEvent;
import org.jkarma.pbcd.events.ChangeDetectedEvent;
import org.jkarma.pbcd.events.ChangeNotDetectedEvent;
import org.jkarma.pbcd.events.PBCDEventListener;
import org.jkarma.pbcd.events.PatternUpdateCompletedEvent;
import org.jkarma.pbcd.events.PatternUpdateStartedEvent;
import org.jkarma.pbcd.patterns.Pattern;
import org.jkarma.pbcd.similarities.Similarity;
import org.jkarma.mining.interfaces.ItemSet;
import org.jkarma.mining.interfaces.Lattice;
import org.jkarma.mining.structures.MiningStrategy;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.structures.TransactionException;
import org.jkarma.mining.windows.WindowingStrategy;
import org.jkarma.model.Transaction;

import com.google.common.eventbus.EventBus;

/**
 * Class defining the implementation of a pattern-based change detector (PBCD).
 * A PBCD is a pipeline which consumes blocks of incoming transactions and runs
 * three different steps: the pattern mining step, the change detection step and
 * eventually the change characterization step.
 * Every PBCD is defined on a precise language of pattern, a particular pattern 
 * evaluation, a pattern weighting scheme, a change detection measure and a change
 * characterization strategy.
 * @author Angelo Impedovo
 *
 * @param <A> Type of instances to be consumed by the PBCD.
 * @param <B> Type denoting the alphabet of allowed pattern symbols.
 * @param <C> Type denoting the pattern evaluation.
 * @param <D> Type denoting the weighting scheme of pattern vectors.
 */
public class PBCD<A extends Transaction<B>, B extends Comparable<B>, C, D> 
implements Consumer<A>{
	
	/**
	 * The block size factor controlling every which transaction the PBCD 
	 * should detect changes.
	 */
	protected int blockSize;
	
	/**
	 * The minimum change threshold to be used in the detection step
	 */
	protected double minMc;
	
	/**
	 * The trade-off between memory capacity and change sensitivity of the algorithm 
	 */
	protected double memoryTradeoff;
	
	/**
	 * The index of current block of transactions to be analyzed.
	 */
	protected int blockIndex = -1;
	
	/**
	 * Binary flag indicating if the analysis of the first block has been completed.
	 */
	protected boolean firstBlockCompleted = false;
	
	/**
	 * The landmark window over the data stream.
	 */
	protected TimeWindow currentLandmark = null;
	
	/**
	 * The current block of transactions to be analyzed.
	 */
	protected Block<A> currentBlock;
	
	/**
	 * The lattice of patterns built on the landmark windows according to the mining strategy.
	 */
	protected Lattice<ItemSet<B,Pair<C>>> lattice;
	
	/**
	 * An inner event bus for event listening.
	 */
	protected EventBus eventBus;
	
	/**
	 * The mining strategy to be used to discover patterns in the pattern mining step.
	 */	
	protected MiningStrategy<B, C> miningStrategy;
	
	/**
	 * The weighting scheme responsible of giving weights to patterns in time windows.
	 */
	protected BiFunction<Pattern<B,C>,Boolean,D> weighter;
	
	/**
	 * The similarity measure in charge of quantifying the change
	 */
	protected Similarity<D> changeMeasure;

	/**
	 * The descriptor component in charge of selecting a sample of patterns which describe the spotted changes
	 */
	protected Descriptor<B,C> descriptor;
	
	
	
	/**
	 * Builds a PBCD by specifying the details about the pattern mining step, the change detection step
	 * and the change characterization step.
	 * 
	 * @param miningStrategy The mining strategy used to discover patterns.
	 * @param weighter The weighting scheme used to assign weights to patterns in time windows.
	 * @param measure The similarity measure used to quantify the change between pattern vectors.
	 * @param describer The change characterization strategy to be used when describing detected changes.
	 * @param blockSize The number of transactions after which to detect the change
	 * @param memoryTradeoff The memory trade-off used to discard previous data.
	 * @param minMc The minimum change threshold used to detect changes in the change detection step.
	 */
	protected PBCD(MiningStrategy<B,C> miningStrategy, BiFunction<Pattern<B,C>,Boolean,D> weighter, Similarity<D> measure,
			Descriptor<B,C> describer, int blockSize, float memoryTradeoff, float minMc) {
		if(blockSize < 1) {
			throw new IllegalArgumentException("blockSize must be a positive integer.");
		}
		if(memoryTradeoff < 0 || memoryTradeoff > 1) {
			throw new IllegalArgumentException("memoryTradeoff must be a float between 0 and 1.");
		}
		if(minMc < 0 || minMc > 1) {
			throw new IllegalArgumentException("minMc must be a float between 0 and 1.");
		}
		if(miningStrategy==null || weighter==null || measure==null || describer==null) {
			throw new NullPointerException();
		}
		this.minMc = minMc;
		this.memoryTradeoff = memoryTradeoff;
		this.blockSize = blockSize;
		this.miningStrategy = miningStrategy;
		this.weighter = weighter;
		this.changeMeasure = measure;
		this.descriptor = describer;
		this.eventBus = new EventBus();
	}
	
	
	
	/**
	 * Register an event listener to this PBCD.
	 * @param eventListener The EventListener object.
	 */
	public void registerListener(PBCDEventListener<B,C> eventListener) {
		this.eventBus.register(eventListener);
	}
	
	
	/**
	 * Unregister an event listener previously registered with this PBCD.
	 * @param eventListener The EventListener object.
	 */
	public void unregisterListener(PBCDEventListener<B,C> eventListener) {
		this.eventBus.unregister(eventListener);
	}
	
	

	/**
	 * Returns the block size factor of this PBCD.
	 * @return The block size factor of this PBCD.
	 */
	public int getBlockSize() {
		return this.blockSize;
	}
	
	
	
	/**
	 * Returns the minimum amount of change to be detected by this PBCD.
	 * @return The minimum amount of change to be detected by this PBCD.
	 */
	public double getMinimumChange() {
		return this.minMc;
	}
	
	
	
	/**
	 * Returns the memory-sensitivity tradeoff of this PBCD.
	 * @return The memory-sensitivity tradeoff of this PBCD.
	 */
	public double getMemoryTradeoff() {
		return this.memoryTradeoff;
	}
	
	
	
	/**
	 * Method to be called for letting the PBCD consume a single transaction.
	 * This is the main method of the PBCD object, as the detector consumes
	 * transactions (which will be internally grouped in blocks) and reacts to the
	 * detection of a change.
	 */
	@Override
	public void accept(A transaction) {
		if(currentBlock == null) {
			//first object arrived, we initialize a block of default batch size
			this.currentBlock = new Block<A>(this.getBlockSize());
			this.currentLandmark = new TimeWindow(this.currentBlock);
		}
		
		//we add the transaction to the block and to the provider
		this.currentBlock.addTransaction(transaction);
		
		//we add the corresponding items as generators of the lattice;
		this.miningStrategy.getEvaluationProvider().accept(transaction);
		for(B item : transaction) {
			miningStrategy.setGenerator(item);
		}
		
		//we check if we filled the current block;
		if(this.currentBlock.isFull()) {
			//we should inspect for changes here
			try {
				//we raise an event of lattice update start
				eventBus.post(new PatternUpdateStartedEvent<B,Pair<C>>(this.currentLandmark, this.currentBlock));
				//we update the lattice of pattern
				lattice = miningStrategy.execute();
				//we raise an event of lattice update completed
				eventBus.post(new PatternUpdateCompletedEvent<B,Pair<C>>(this.currentLandmark, this.currentBlock));
				
				//change detection only makes sense starting from the second block
				if(this.firstBlockCompleted) {
					//we compute the macroscopic change intensity
					double detectedMacroChange = this.getChangeAmount();
					//so we can test it against the minimum threshold provided by the user...
					boolean changeDetected = (detectedMacroChange > this.minMc);
					//...and against the memory-sensitivity threshold
					boolean tradeoffExceeded = (detectedMacroChange > this.memoryTradeoff);
					
					if(changeDetected) {
						//we raise an event of microscopic detection start
						eventBus.post(new ChangeDescriptionStartedEvent<B,Pair<C>>(this.currentLandmark, this.currentBlock));
						//then we check for microscopic changes
						Stream<Pattern<B, C>> descriptiveModel = this.descriptor.apply(this.getChangeDescription());
						//we raise an event of microscopic detection completed
						eventBus.post(new ChangeDescriptionCompletedEvent<B,Pair<C>>(this.currentLandmark, this.currentBlock));
						
						//we raise an event of macroscopic change found
						eventBus.post(new ChangeDetectedEvent<B,C>(
							this.currentLandmark, this.currentBlock, detectedMacroChange, descriptiveModel
						));
					}else {
						//we raise an event of macroscopic change not found
						eventBus.post(new ChangeNotDetectedEvent<B,C>(this.currentLandmark, this.currentBlock, detectedMacroChange));
					}
					
					if(tradeoffExceeded) {						
						//there is no need to notify the events from this point on
						//willNotifyEvents = false;
						
						//we commit any pending modifications
						miningStrategy.commit();
						
						//we set the transaction count to that of a single block...
						miningStrategy.getEvaluationProvider().forgetCached();
						
						//...then we forget everything in the past
						//and then we compute any missing pattern in the latest block
						miningStrategy.forget();
						miningStrategy.getEvaluationProvider().flatten();
						
						//then we re-enable the events
						//willNotifyEvents = true;
						this.currentLandmark = new TimeWindow(this.currentBlock);
						this.blockIndex = -1;
					}else{
						//and, finally we commit and we flatten the provider
						miningStrategy.commit();
						miningStrategy.getEvaluationProvider().flatten();
						this.currentLandmark.expand(this.currentBlock);
					}
				}else{
					//we commit any pending modifications of the lattice since we are only building
					//the lattice on the first block;
					miningStrategy.commit();
					miningStrategy.getEvaluationProvider().flatten();
					this.currentLandmark = new TimeWindow(this.currentBlock);
				}
			} catch (TransactionException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
			
			//we set the first block flag to true
			if(!this.firstBlockCompleted) {
				this.firstBlockCompleted = true;
			}
			blockIndex++;
			
			//then we should set a new current empty block
			this.currentBlock = new Block<A>(this.blockSize);
		}
	}
	
	
	/**
	 * Private method implementing the change detection step according to
	 * the weighting scheme and the similarity measure of the PBCD.
	 * @return The change score measured.
	 */
	private double getChangeAmount() {
		Vector<D> w1 = new Vector<D>();
		Vector<D> w2 = new Vector<D>();
		
		this.getChangeDescription().forEach(p -> {
			//System.out.println(p);
			D v1 = this.weighter.apply(p,true);
			D v2 = this.weighter.apply(p,false);
			w1.add(v1);
			w2.add(v2);
		});
		
		/*
		System.out.println("============");
		System.out.println(w1);
		System.out.println(w2);
		System.out.println("============");
		*/
		return this.changeMeasure.apply(w1, w2);
	} 
	
	
	
	/**
	 * Private method implementing the change characterization step according to
	 * the change characterization strategy of the PBCD.
	 * @return The set of patterns which best describe the detected change.
	 */
	private Stream<Pattern<B,C>> getChangeDescription(){
		//we get the virtual lattice of patterns
		Lattice<Pattern<B,C>> vLattice = this.getLattice();
		
		//and we skip the most general pattern
		Stream<Pattern<B,C>> nonEmptyPatterns = vLattice.stream().skip(1);
		
		//and we return the stream
		return nonEmptyPatterns;
	}
	
	
	
	/**
	 * Convenience method which returns a virtual view of the pattern lattice.
	 * The virtual view makes use of the aggregator delegate in a windowing strategy.
	 * @return The lattice of patterns backed by this detector.
	 */
	public Lattice<Pattern<B,C>> getLattice(){
		//we get the windowing strategy adopted by the mining strategy
		WindowingStrategy<C> windowing = this.miningStrategy.getWindowing();
		
		//we build a virtual lattice upon the windowing strategy adopted
		return new VirtualLattice<B,C>(this.lattice, windowing);
	}
}

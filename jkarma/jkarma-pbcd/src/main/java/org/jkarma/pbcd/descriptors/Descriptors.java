/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.descriptors;

import org.jkarma.mining.joiners.FrequencyEvaluation;
import org.jkarma.mining.joiners.ProjectedDB;

/**
 * Class serving as a main point of access for constructing the various
 * Descriptor objects implementing the change characterization strategies.
 * @author Angelo Impedovo
 */
public class Descriptors {
	
	/**
	 * Builds an empty change characterization step which does not select
	 * any pattern, thus producing empty change characterizations.
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @return The EmptyDescriptor object.
	 */
	public static <A extends Comparable<A>, B>
	EmptyDescriptor<A,B> empty(){
		return new EmptyDescriptor<A,B>();
	}

	
	/**
	 * Builds a change characterization step which selects only the emerging
	 * patterns when characterizing a change.
	 * Only emerging patterns whose growth-rate exceeds minGr are selected.
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param minGr The minimum growth-rate threshold.
	 * @return The EPDescriptor object.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	EPDescriptor<A,B> eps(double minGr){
		return new EPDescriptor<A,B>(minGr);
	}

	
	
	/**
	 * Builds a change characterization step which selects only the emerging 
	 * patterns from those whose support has crossed the minimum support when 
	 * characterizing a change.
	 * The emerging patterns whose growth-rate exceeds minGr are selected among the patterns 
	 * which have crossed the minimum support threshold minSup between the past and the recent 
	 * time windows.
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param minSup The minimum support threshold.
	 * @param minGr The minimum growth-rate threshold.
	 * @return The PartialEPDescriptor object.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	PartialEPDescriptor<A,B> partialEps(double minSup, double minGr){
		return new PartialEPDescriptor<A,B>(minSup, minGr);
	}
	
	
	
	/**
	 * Builds a change characterization step which selects only the closed
	 * emerging patterns from those whose support has crossed the minimum support when 
	 * characterizing a change.
	 * The closed emerging patterns whose growth-rate exceeds minGr are selected among the patterns 
	 * which have crossed the minimum support threshold minSup between the past and the recent 
	 * time windows.
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param minSup The minimum support threshold.
	 * @param mingr The minimum growth-rate threshold.
	 * @param deferred Status flag for deferred evaluation.
	 * @return The PartialCEPDescriptor on the given thresholds.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	PartialCEPDescriptor<A,B> closedPartialEps(double minSup, double mingr, boolean deferred){
		return new PartialCEPDescriptor<A,B>(minSup, mingr, deferred);
	}
	
	
	/**
	 * Builds a change characterization step which selects only the maximal
	 * emerging patterns from those whose support has crossed the minimum support when 
	 * characterizing a change.
	 * The maximal emerging patterns whose growth-rate exceeds minGr are selected among the patterns 
	 * which have crossed the minimum support threshold minSup between the past and the recent 
	 * time windows.
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param minSup The minimum support threshold.
	 * @param mingr The minimum growth-rate threshold.
	 * @param deferred Status flag for deferred evaluation.
	 * @return The PartialMEPDescriptor on the given thresholds.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	PartialMEPDescriptor<A, B> maxPartialEps(double minSup, double mingr, boolean deferred){
		return new PartialMEPDescriptor<A,B>(minSup, mingr, deferred);
	}
	
	
	/**
	 * Builds a change characterization step which selects only the emerging patterns
	 * from those closed (as in the LCM algorithm) whose support has crossed the minimum support
	 * when characterizing a change.
	 * The emerging patterns whose growth-rate exceeds minGr are selected among the patterns 
	 * which have crossed the minimum support threshold minSup between the past and the recent 
	 * time windows.
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param minsup The minimum support threshold.
	 * @param mingr The minimum growth-rate threshold.
	 * @return The PartialEPLCMDescriptor on the given thresholds.
	 */
	public static <A extends Comparable<A>, B extends ProjectedDB<A>>
	PartialEPLCMDescriptor<A, B> lcmPartialEps(double minsup, double mingr){
		return new PartialEPLCMDescriptor<A,B>(minsup, mingr);
	}
	
	
	/**
	 * Builds a change characterization step which selects only the emerging patterns
	 * from those maximal (as in the LCM-max algorithm) whose support has crossed the minimum support
	 * when characterizing a change.
	 * The emerging patterns whose growth-rate exceeds minGr are selected among the patterns 
	 * which have crossed the minimum support threshold minSup between the past and the recent 
	 * time windows.
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param minsup The minimum support threshold.
	 * @param mingr The minimum growth-rate threshold.
	 * @return The PartialEPLCMMaxDescriptor on the given thresholds.
	 */
	public static <A extends Comparable<A>, B extends ProjectedDB<A>>
	PartialEPLCMMaxDescriptor<A, B> lcmMaxPartialEps(double minsup, double mingr){
		return new PartialEPLCMMaxDescriptor<A,B>(minsup, mingr);
	}
	
}

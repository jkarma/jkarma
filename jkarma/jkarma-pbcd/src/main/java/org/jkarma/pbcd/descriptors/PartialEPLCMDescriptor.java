/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.descriptors;

import java.util.stream.Stream;

import org.jkarma.pbcd.patterns.Pattern;
import org.jkarma.pbcd.patterns.Patterns;
import org.jkarma.mining.joiners.ProjectedDB;


/**
 * Change descriptor which characterize the changes by partially selecting the
 * closed emerging patterns (CEPs) among the patterns who became frequent and infrequent
 * between two time windows.
 * The descriptor extends PartialEPDescriptor because it offers the same functionality,
 * with the exception of limiting the search space of patterns only to i) those
 * whose support has crossed the minimum support threshold, and ii) those
 * who satisfy the closure condition as in the LCM algorithm.
 * @author Angelo Impedovo
 *
 * @param <A> Type defining the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 */
public class PartialEPLCMDescriptor<A extends Comparable<A>,B extends ProjectedDB<A>> extends PartialEPDescriptor<A,B>{

	
	/**
	 * Constructs a new PartialEPLCMDescriptor given a support and a 
	 * growth-rate threshold.
	 * @param minSup The minimum support threshold.
	 * @param minGr The minimum growth-rate threshold.
	 */
	public PartialEPLCMDescriptor(double minSup, double minGr) {
		super(minSup, minGr);
	}
	
	
	/**
	 * Filters the closed emerging patterns from those closed (as in LCM) whose 
	 * support has crossed the minimum threshold from the stream of patterns.
	 */
	@Override
	public Stream<Pattern<A, B>> apply(Stream<Pattern<A, B>> stream) {
		double minSup = super.getMinSup();
		return stream.filter(p -> Patterns.hasCrossedMinimumSupport(p, minSup))
				.filter(p -> Patterns.isPPC(p))
				.filter(p -> Patterns.isClosedByClosure(p))
				.filter(p -> Patterns.isEmerging(p, super.getMinGr()));
	}	

}

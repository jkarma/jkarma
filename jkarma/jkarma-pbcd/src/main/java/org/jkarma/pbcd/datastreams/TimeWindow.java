/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.datastreams;

import java.time.Instant;

import org.jkarma.model.Transaction;

/**
 * Convenience class implementing the temporal extrema of a time window.
 * This class is a bean object used to pass event-related temporal information
 * to the listener of PBCD events. 
 * @author Angelo Impedovo
 */
public class TimeWindow{
	
	private Instant firstInstant;
	private Instant lastInstant;
	
	
	/**
	 * Builds a time window bean object by considering the temporal bounds of a block.
	 * The constructed window represent a temporal interval comprised between the
	 * timestamps of the first and the last transaction of the block
	 * @param firstBlock The block to be considered.
	 */
	public TimeWindow(Block<? extends Transaction<?>> firstBlock) {
		if(firstBlock==null) {
			throw new NullPointerException();
		}
		
		this.firstInstant = firstBlock.getFirstInstant();
		this.lastInstant = firstBlock.getLastInstant();
	}
	
	
	
	/**
	 * Returns the temporal lower bound of the time window. 
	 * @return The temporal lower bound of the time window.
	 */
	public Instant getFirstInstant() {
		return this.firstInstant;
	}
	
	
	
	/**
	 * Returns the temporal higher bound of the time window. 
	 * @return The temporal higher bound of the time window.
	 */
	public Instant getLastInstant() {
		return this.lastInstant;
	}
	
	
	
	/**
	 * Expand the time window by considering another block.
	 * This operation sets the temporal higher bound of the time window equal
	 * to the timestamp of the last transaction in the block.
	 * @param block The block to be considered.
	 */
	public void expand(Block<? extends Transaction<?>> block) {
		this.lastInstant = block.getLastInstant();
	}
	
	@Override
	public String toString() {
		return this.getFirstInstant()+", "+this.getLastInstant();
	}

}

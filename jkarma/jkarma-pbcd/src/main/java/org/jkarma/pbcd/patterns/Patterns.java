/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.patterns;

import org.jkarma.mining.joiners.ClosureEvaluation;
import org.jkarma.mining.joiners.FrequencyEvaluation;
import org.jkarma.mining.joiners.MaximalityEvaluation;
import org.jkarma.mining.joiners.PeriodicityEvaluation;
import org.jkarma.mining.joiners.ProjectedDB;

/**
 * This class contains different utility static methods for accessing
 * and evaluating different pattern properties.
 * 
 * @author Angelo Impedovo
 */
public class Patterns {
	
	/**
	 * Computes the relative frequency associated to a Pattern.
	 * Necessary condition for evaluating the relative frequency is that B
	 * implements the FrequencyEvaluation interface.
	 * Since every Pattern encapsulates two frequency evaluations of type B
	 * on two time windows, past and recent, the method allows the user to choose
	 * on which one to act.
	 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern of which the relative frequency is computed
	 * @param shouldUseFirstEval If true, the frequency will be computed on the past
	 *  time window, false otherwise.
	 * @return The relative frequency associated to the pattern's FrequencyEvaluation.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation> 
	double getRelativeFrequency(Pattern<A,B> pattern, boolean shouldUseFirstEval) {
		if(shouldUseFirstEval) {
			return pattern.getFirstEval().getRelativeFrequency();
		}else {
			return pattern.getSecondEval().getRelativeFrequency();
		}
	}
	
	
	
	/**
	 * Computes the absolute frequency associated to a Pattern.
	 * Necessary condition for evaluating the absolute frequency is that B
	 * implements the FrequencyEvaluation interface.
	 * Since every Pattern encapsulates two frequency evaluations of type B
	 * on two time windows, past and recent, the method allows the user to choose
	 * on which one to act.
	 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern of which the absolute frequency is computed
	 * @param shouldUseFirstEval If true, the frequency will be computed on the past
	 *  time window, false otherwise.
	 * @return The absolute frequency associated to the pattern's FrequencyEvaluation.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation> 
	double getAbsoluteFrequency(Pattern<A,B> pattern, boolean shouldUseFirstEval) {
		if(shouldUseFirstEval) {
			return pattern.getFirstEval().getAbsoluteFrequency();
		}else {
			return pattern.getSecondEval().getAbsoluteFrequency();
		}
	}
	

	
	/**
	 * Evaluates it a Pattern was frequent in the associated past time window.
	 * The pattern was frequent if the relative frequency associated to the past
	 * time window exceeds a minimum threshold minSup.
	 * Necessary condition for evaluating the relative frequency is that B
	 * implements the FrequencyEvaluation interface.
	 * Every Pattern encapsulates two frequency evaluations of type B
	 * on two time windows, past and recent, this method acts only on the past window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minSup The minimum frequency threshold.
	 * @return True if the pattern was frequent in the past window, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation> 
	boolean wasFrequent(Pattern<A,B> pattern, double minSup) {
		return pattern.getFirstEval().isFrequent(minSup);
	}
	
	
	
	/**
	 * Evaluates if a Pattern is frequent in the associated recent time window.
	 * The pattern is frequent if the relative frequency associated to the recent
	 * time window exceeds a minimum threshold minSup.
	 * Necessary condition for evaluating the relative frequency is that B
	 * implements the FrequencyEvaluation interface.
	 * Every Pattern encapsulates two frequency evaluations of type B
	 * on two time windows, past and recent, this method acts only on the recent window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minSup The minimum frequency threshold.
	 * @return True if the pattern is frequent in the recent window, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean isFrequent(Pattern<A,B> pattern, double minSup) {
		return pattern.getSecondEval().isFrequent(minSup);
	}
	
	
	
	/**
	 * Evaluates if a Pattern is frequent in an associated time window.
	 * The pattern is frequent if the relative frequency associated to the 
	 * time window exceeds a minimum threshold minSup.
	 * Necessary condition for evaluating the relative frequency is that B
	 * implements the FrequencyEvaluation interface.
	 * Every Pattern encapsulates two frequency evaluations of type B
	 * on two time windows, past and recent, this method allows the user to choose
	 * which time window consider during the evaluation.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minSup The minimum frequency threshold.
	 * @param shouldUseFirstEval If true, the predicate will be evaluated on the past
	 *  time window, false otherwise.
	 * @return True if the pattern is frequent in the chosen window, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean isFrequent(Pattern<A,B> pattern, double minSup, boolean shouldUseFirstEval) {
		if(shouldUseFirstEval) {
			return Patterns.wasFrequent(pattern, minSup);
		}else{
			return Patterns.isFrequent(pattern, minSup);
		}
	}
	
	
	
	/**
	 * Evaluates it a Pattern was periodic in the associated past time window.
	 * The periodicity evaluation is done according to the method proposed by Fourner et al.
	 * in the PFPM algorithm.
	 * Necessary condition for evaluating the periodicity is that B implements the 
	 * PeriodicityEvaluation interface.
	 * Every Pattern encapsulates two periodicity evaluations of type B
	 * on two time windows, past and recent, this method acts only on the past window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minPer Pattern periodicity lower bound.
	 * @param maxPer Pattern periodicity upper bound.
	 * @param minAvgPer Average pattern periodicity lower bound.
	 * @param maxAvgPer Average pattern periodicity upper bound.
	 * @return True if the pattern was periodic in the past window, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends PeriodicityEvaluation> 
	boolean wasPeriodic(Pattern<A,B> pattern, int minPer, int maxPer, double minAvgPer, double maxAvgPer) {
		return pattern.getFirstEval().isPeriodic(minPer, maxPer, minAvgPer, maxAvgPer);
	}
	
	
	
	/**
	 * Evaluates if a Pattern is periodic in the associated recent time window.
	 * The periodicity evaluation is done according to the method proposed by Fourner et al.
	 * in the PFPM algorithm.
	 * Necessary condition for evaluating the periodicity is that B implements the 
	 * PeriodicityEvaluation interface.
	 * Every Pattern encapsulates two periodicity evaluations of type B
	 * on two time windows, past and recent, this method acts only on the recent window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minPer Pattern periodicity lower bound.
	 * @param maxPer Pattern periodicity upper bound.
	 * @param minAvgPer Average pattern periodicity lower bound.
	 * @param maxAvgPer Average pattern periodicity upper bound.
	 * @return True if the pattern is periodic in the recent window, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends PeriodicityEvaluation>
	boolean isPeriodic(Pattern<A,B> pattern, int minPer, int maxPer, double minAvgPer, double maxAvgPer) {
		return pattern.getSecondEval().isPeriodic(minPer, maxPer, minAvgPer, maxAvgPer);
	}
	
	
	
	/**
	 * Evaluates if a Pattern is periodic in an associated time window.
	 * The periodicity evaluation is done according to the method proposed by Fourner et al.
	 * in the PFPM algorithm.
	 * Necessary condition for evaluating the periodicity is that B implements the 
	 * PeriodicityEvaluation interface.
	 * Every Pattern encapsulates two periodicity evaluations of type B
	 * on two time windows, past and recent, this method allows the user to choose
	 * which time window consider during the evaluation.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minPer Pattern periodicity lower bound.
	 * @param maxPer Pattern periodicity upper bound.
	 * @param minAvgPer Average pattern periodicity lower bound.
	 * @param maxAvgPer Average pattern periodicity upper bound.
	 * @param shouldUseFirstEval If true, the predicate will be evaluated on the past
	 *  time window, false otherwise.
	 * @return True if the pattern is periodic in the recent window, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends PeriodicityEvaluation>
	boolean isPeriodic(Pattern<A,B> pattern, int minPer, int maxPer, double minAvgPer, 
			double maxAvgPer, boolean shouldUseFirstEval) {
		if(shouldUseFirstEval) {
			return Patterns.wasPeriodic(pattern, minPer, maxPer, minAvgPer, maxAvgPer);
		}else{
			return Patterns.isPeriodic(pattern, minPer, maxPer, minAvgPer, maxAvgPer);
		}
	}
	
	
	
	/**
	 * Computes the growth-rate associated to a Pattern.
	 * The growth-rate of a Pattern is computed as the ratio of the
	 * maximum relative frequency associated to the past and recent time windows,
	 * and the minimum relative frequency associated to the past and recent time
	 * windows. Therefore, the growth-rate computed by this method is asymmetric, 
	 * for this reason is always greater than 1.
	 * Necessary condition for evaluating the relative frequency is that B
	 * implements the FrequencyEvaluation interface.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @return The growth-rate associated to the pattern.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	double getGrowthRate(Pattern<A,B> pattern) {
		double s1 = pattern.getFirstEval().getRelativeFrequency();
		double s2 = pattern.getSecondEval().getRelativeFrequency();
		return Math.max(s1, s2)/Math.min(s1, s2);
	}
	
	
	
	/**
	 * Evaluates if a pattern is emerging between two time windows.
	 * A pattern is emerging if his growth-rate exceeds a minimum growth-rate threshold.
	 * Necessary condition for evaluating the emergency of a Pattern is that B
	 * implements the FrequencyEvaluation interface.
	 * The minimum growth-rate threshold should be greater or equal to 1.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minGr The minimum growth-rate threshold.
	 * @return The growth-rate associated to the pattern.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean isEmerging(Pattern<A,B> pattern, double minGr) {
		if(minGr<1) {
			throw new IllegalArgumentException();
		}
		double gr = Patterns.getGrowthRate(pattern);
		return (gr >= minGr);
	}
	
	
	
	/**
	 * Checks if the pattern's relative frequency has crossed the minimum support threshold
	 * minSup between the past and the recent time windows.
	 * Necessary condition for evaluating the frequency of a Pattern is that B
	 * implements the FrequencyEvaluation interface.
	 * The minimum support threshold should be comprised between 0 and 1.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minSup The minimum support threshold.
	 * @return The growth-rate associated to the pattern.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean hasCrossedMinimumSupport(Pattern<A,B> pattern, double minSup) {
		if(minSup<0 && minSup>1) {
			throw new IllegalArgumentException();
		}
		
		boolean result = false;
		boolean isFrequent = Patterns.isFrequent(pattern, minSup);
		boolean wasFrequent = Patterns.wasFrequent(pattern, minSup);
		
		if(!wasFrequent && isFrequent) {
			result = true;
		}else if(wasFrequent && !isFrequent){
			result = true;
		}
		
		return result;
	}
	
	
	
	
	/**
	 * Evaluates if a Pattern is closed in the associated recent time window.
	 * The closure is evaluated according to the closure system implemented in
	 * the evaluation type B.
	 * Therefore, necessary condition for evaluating the closure is that B implements the 
	 * ClosureEvaluation interface.
	 * Every Pattern encapsulates two closure evaluations of type B on 
	 * two time windows, past and recent, this method acts only on the recent window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @return True if the pattern is closed, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends ClosureEvaluation>
	boolean isClosedByClosure(Pattern<A,B> pattern) {
		B patternEval = pattern.getSecondEval();
		return patternEval.isClosed();
	}
	
	
	
	/**
	 * Evaluates if a Pattern is closed in the associated past time window.
	 * The closure is evaluated according to the closure system implemented in
	 * the evaluation type B.
	 * Therefore, necessary condition for evaluating the closure is that B implements the 
	 * ClosureEvaluation interface.
	 * Every Pattern encapsulates two closure evaluations of type B on 
	 * two time windows, past and recent, this method acts only on the past window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @return True if the pattern was closed, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends ClosureEvaluation>
	boolean wasClosedByClosure(Pattern<A,B> pattern) {
		B patternEval = pattern.getFirstEval();
		return patternEval.isClosed();
	}
	
	
	
	/**
	 * Evaluates if a Pattern is closed in an associated time window.
	 * The closure is evaluated according to the closure system implemented in
	 * the evaluation type B.
	 * Therefore, necessary condition for evaluating the closure is that B implements the 
	 * ClosureEvaluation interface.
	 * Every Pattern encapsulates two closure evaluations of type B on 
	 * two time windows, past and recent, this method allows the user to choose
	 * which time window consider during the evaluation.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param shouldUseFirstEval If true, the predicate will be evaluated on the past
	 *  time window, false otherwise.
	 * @return True if the pattern was closed, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends ClosureEvaluation>
	boolean isClosedByClosure(Pattern<A,B> pattern, boolean shouldUseFirstEval) {
		if(shouldUseFirstEval) {
			return Patterns.wasClosedByClosure(pattern);
		}else{
			return Patterns.isClosedByClosure(pattern);
		}
	}
	
	
	
	/**
	 * Evaluates if a Pattern is frequent closed in the associated recent time window.
	 * The closure is evaluated according to a storage-logic, that is whether there is not
	 * another more specific frequent pattern covered by the same transactions.
	 * Therefore, necessary condition for evaluating the closure is that B implements the 
	 * FrequencyEvaluation interface.
	 * Every Pattern encapsulates two closure evaluations of type B on 
	 * two time windows, past and recent, this method acts only on the recent window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @return True if the pattern is frequent closed, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean isClosedByStorage(Pattern<A,B> pattern) {
		boolean isClosed = true;
		B patternEval = pattern.getSecondEval();
		
		for(Pattern<A,B> superPattern : pattern.getLattice().getLowerElements(pattern)) {
			B superEvaluation = superPattern.getSecondEval();
			if(superEvaluation.equals(patternEval)) {
				isClosed = false;
				break;
			}
		}
		
		return isClosed;
	}
	
	
	
	/**
	 * Evaluates if a Pattern was frequent closed in the associated past time window.
	 * The closure is evaluated according to a storage-logic, that is whether there is not
	 * another more specific frequent pattern covered by the same transactions.
	 * Therefore, necessary condition for evaluating the closure is that B implements the 
	 * FrequencyEvaluation interface.
	 * Every Pattern encapsulates two closure evaluations of type B on 
	 * two time windows, past and recent, this method acts only on the past window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @return True if the pattern is frequent closed, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean wasClosedByStorage(Pattern<A,B> pattern) {
		boolean isClosed = true;
		B patternEval = pattern.getFirstEval();
		
		for(Pattern<A,B> superPattern : pattern.getLattice().getLowerElements(pattern)) {
			B superEvaluation = superPattern.getFirstEval();
			if(superEvaluation.equals(patternEval)) {
				isClosed = false;
				break;
			}
		}
		
		return isClosed;
	}

	
	
	/**
	 * Evaluates if a Pattern was frequent closed in an associated time window.
	 * The closure is evaluated according to a storage-logic, that is whether there is not
	 * another more specific frequent pattern covered by the same transactions.
	 * Therefore, necessary condition for evaluating the closure is that B implements the 
	 * FrequencyEvaluation interface.
	 * Every Pattern encapsulates two closure evaluations of type B on 
	 * two time windows, past and recent, this method allows the user to choose
	 * which time window consider during the evaluation.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param shouldUseFirstEval If true, the predicate will be evaluated on the past
	 *  time window, false otherwise.
	 * @return True if the pattern was frequent closed, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean isClosedByStorage(Pattern<A,B> pattern, boolean shouldUseFirstEval) {
		if(shouldUseFirstEval) {
			return Patterns.wasClosedByStorage(pattern);
		}else {
			return Patterns.isClosedByStorage(pattern);
		}
	}

	
	
	/**
	 * Evaluates if a Pattern is maximal in the associated recent time window.
	 * The closure is evaluated according to the maximality system implemented in
	 * the evaluation type B.
	 * Therefore, necessary condition for evaluating the maximality is that B implements the 
	 * MaximalityEvaluation interface.
	 * Every Pattern encapsulates two maximality evaluations of type B on 
	 * two time windows, past and recent, this method acts only on the recent window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minSup The minimum relative frequency threshold.
	 * @return True if the pattern is maximal, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends MaximalityEvaluation>
	boolean isMaximalByClosure(Pattern<A,B> pattern, double minSup) {
		B patternEval = pattern.getSecondEval();
		return patternEval.isMaximal(minSup);
	}
	
	
	
	/**
	 * Evaluates if a Pattern is maximal in the associated past time window.
	 * The closure is evaluated according to the maximality system implemented in
	 * the evaluation type B.
	 * Therefore, necessary condition for evaluating the maximality is that B implements the 
	 * MaximalityEvaluation interface.
	 * Every Pattern encapsulates two maximality evaluations of type B on 
	 * two time windows, past and recent, this method acts only on the past window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minSup The minimum relative frequency threshold.
	 * @return True if the pattern was maximal, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends MaximalityEvaluation>
	boolean wasMaximalByClosure(Pattern<A,B> pattern, double minSup) {
		B patternEval = pattern.getFirstEval();
		return patternEval.isMaximal(minSup);
	}
	
	
	
	/**
	 * Evaluates if a Pattern is maximal in an associated time window.
	 * The closure is evaluated according to the maximality system implemented in
	 * the evaluation type B.
	 * Therefore, necessary condition for evaluating the maximality is that B implements the 
	 * MaximalityEvaluation interface.
	 * Every Pattern encapsulates two maximality evaluations of type B on 
	 * two time windows, past and recent, this method allows the user to choose
	 * which time window consider during the evaluation.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minSup The minimum relative frequency threshold.
	 * @param shouldUseFirstEval If true, the predicate will be evaluated on the past
	 *  time window, false otherwise.
	 * @return True if the pattern was maximal, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends MaximalityEvaluation>
	boolean isMaximalByClosure(Pattern<A,B> pattern, double minSup, boolean shouldUseFirstEval) {
		if(shouldUseFirstEval) {
			return Patterns.wasMaximalByClosure(pattern, minSup);
		}else {
			return Patterns.isMaximalByClosure(pattern, minSup);
		}
	}
	
	
	
	/**
	 * Evaluates if a Pattern is frequent maximal in the associated recent time window.
	 * The maximality is evaluated according to a storage-logic, that is whether there is not
	 * another more specific frequent pattern.
	 * Therefore, necessary condition for evaluating the maximality is that B implements the 
	 * FrequencyEvaluation interface.
	 * Every Pattern encapsulates two maximality evaluations of type B on 
	 * two time windows, past and recent, this method acts only on the recent window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minSup The minimum relative frequency threshold.
	 * @return True if the pattern is frequent maximal, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean isMaximalByStorage(Pattern<A,B> pattern, double minSup) {
		boolean result = true;
		boolean pFrequent = Patterns.isFrequent(pattern, minSup);
		for(Pattern<A,B> q : pattern.getLattice().getLowerElements(pattern)) {
			if(pFrequent) {
				//check among the frequent ones
				if(Patterns.isFrequent(q, minSup)) {
					result = false;
					break;
				}
			}else {
				//check among the infrequent ones
				if(!Patterns.isFrequent(q, minSup)) {
					result = false;
					break;
				}
			}
		}
		return result;
	}
	
	
	
	/**
	 * Evaluates if a Pattern is frequent maximal in the associated past time window.
	 * The maximality is evaluated according to a storage-logic, that is whether there is not
	 * another more specific frequent pattern.
	 * Therefore, necessary condition for evaluating the maximality is that B implements the 
	 * FrequencyEvaluation interface.
	 * Every Pattern encapsulates two maximality evaluations of type B on 
	 * two time windows, past and recent, this method acts only on the past window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minSup The minimum relative frequency threshold.
	 * @return True if the pattern is frequent maximal, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean wasMaximalByStorage(Pattern<A,B> pattern, double minSup) {
		boolean result = true;
		boolean pFrequent = Patterns.wasFrequent(pattern, minSup);
		for(Pattern<A,B> q : pattern.getLattice().getLowerElements(pattern)) {
			if(pFrequent) {
				//check among the frequent ones
				if(Patterns.wasFrequent(q, minSup)) {
					result = false;
					break;
				}
			}else {
				//check among the infrequent ones
				if(!Patterns.wasFrequent(q, minSup)) {
					result = false;
					break;
				}
			}
		}
		return result;
	}
	
	
	
	/**
	 * Evaluates if a Pattern is frequent maximal in an associated time window.
	 * The maximality is evaluated according to a storage-logic, that is whether there is not
	 * another more specific frequent pattern.
	 * Therefore, necessary condition for evaluating the maximality is that B implements the 
	 * FrequencyEvaluation interface.
	 * Every Pattern encapsulates two maximality evaluations of type B on 
	 * two time windows, past and recent, this method allows the user to choose
	 * which time window consider during the evaluation.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minSup The minimum relative frequency threshold.
	 * @param shouldUseFirstEval If true, the predicate will be evaluated on the past
	 *  time window, false otherwise.
	 * @return True if the pattern was frequent maximal, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean isMaximalByStorage(Pattern<A,B> pattern, double minSup, boolean shouldUseFirstEval) {
		if(shouldUseFirstEval) {
			return Patterns.wasMaximalByStorage(pattern, minSup);
		}else{
			return Patterns.isMaximalByStorage(pattern, minSup);
		}
	}
	
	
	
	/**
	 * Evaluates whether a pattern satisfy the PPC condition, expressed by Uno et al.
	 * in the LCM algorithm, in the associated past time window.
	 * PPC stands for "prefix preserving closed", it is a pruning condition used to
	 * generated closed pattern with a pattern-growth approach in the LCM algorithm.
	 * Necessary condition for evaluating the PPC condition is that B extends the  
	 * ProjectedDB class.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @see <a href="http://ceur-ws.org/Vol-126/uno.pdf">LCM ver. 2: Efficient Mining 
	 * Algorithms for Frequent/Closed/Maximal Itemsets. FIMI 2004</a>
	 * @param pattern The pattern to be evaluated.
	 * @return True if the pattern satisfies the PPC condition, false otherwise
	 */
	public static <A extends Comparable<A>, B extends ProjectedDB<A>>
	boolean wasPPC(Pattern<A,B> pattern) {
		return pattern.getFirstEval().isPPC();
	}
	
	
	
	/**
	 * Evaluates whether a pattern satisfy the PPC condition, expressed by Uno et al.
	 * in the LCM algorithm, in the associated recent time window.
	 * PPC stands for "prefix preserving closed", it is a pruning condition used to
	 * generated closed pattern with a pattern-growth approach in the LCM algorithm.
	 * Necessary condition for evaluating the PPC condition is that B extends the  
	 * ProjectedDB class.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @see <a href="http://ceur-ws.org/Vol-126/uno.pdf">LCM ver. 2: Efficient Mining 
	 * Algorithms for Frequent/Closed/Maximal Itemsets. FIMI 2004</a>
	 * @param pattern The pattern to be evaluated.
	 * @return True if the pattern satisfies the PPC condition, false otherwise
	 */
	public static <A extends Comparable<A>, B extends ProjectedDB<A>>
	boolean isPPC(Pattern<A,B> pattern) {
		return pattern.getSecondEval().isPPC();
	}
	
	
	
	
	/**
	 * Evaluates if a Pattern is rare in an associated time window.
	 * The pattern is rare if the relative frequency associated to the 
	 * time window exceeds a minimum threshold minRarity and does not exceed
	 * a maximum threshold maxRarity.
	 * Necessary condition for evaluating the relative frequency is that B
	 * implements the FrequencyEvaluation interface.
	 * Every Pattern encapsulates two frequency evaluations of type B
	 * on two time windows, past and recent, this method allows the user to choose
	 * which time window consider during the evaluation.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minRarity The minimum frequency threshold.
	 * @param maxRarity The maximum frequency threshold.
	 * @param shouldUseFirstEval If true, the predicate will be evaluated on the past
	 *  time window, false otherwise.
	 * @return True if the pattern is rare in the chosen window, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean isRare(Pattern<A,B> pattern, double minRarity, double maxRarity, boolean shouldUseFirstEval) {
		if(shouldUseFirstEval) {
			return Patterns.wasRare(pattern, minRarity, maxRarity);
		}else {
			return Patterns.isRare(pattern, minRarity, maxRarity);
		}
	}
	
	
	
	
	
	/**
	 * Evaluates if a Pattern is rare in the associated recent time window.
	 * The pattern is rare if the relative frequency associated to the recent
	 * time window exceeds a minimum threshold minRarity and does not exceed a 
	 * maximum threshold maxRarity.
	 * Necessary condition for evaluating the relative frequency is that B
	 * implements the FrequencyEvaluation interface.
	 * Every Pattern encapsulates two frequency evaluations of type B
	 * on two time windows, past and recent, this method acts only on the recent window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minRarity The minimum frequency threshold.
	 * @param maxRarity The maximum frequency threshold.
	 * @return True if the pattern is rare in the recent window, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean isRare(Pattern<A,B> pattern, double minRarity, double maxRarity) {
		double freq = Patterns.getRelativeFrequency(pattern, false);
		return freq >= minRarity && freq < maxRarity;
	}
	
	
	
	/**
	 * Evaluates it a Pattern was rare in the associated past time window.
	 * The pattern was rare if the relative frequency associated to the past
	 * time window exceeds a minimum threshold minRarity and does not exceed
	 * a maximum threshold maxRarity.
	 * Necessary condition for evaluating the relative frequency is that B
	 * implements the FrequencyEvaluation interface.
	 * Every Pattern encapsulates two frequency evaluations of type B
	 * on two time windows, past and recent, this method acts only on the past window.
	 * 
	 * @param <A> Type of allowed pattern symbols.
 	 * @param <B> Type of pattern evaluations.
	 * @param pattern The pattern to be evaluated.
	 * @param minRarity The minimum frequency threshold.
	 * @param maxRarity The maximum frequency threshold.
	 * @return True if the pattern was rare in the past window, false otherwise.
	 */
	public static <A extends Comparable<A>, B extends FrequencyEvaluation>
	boolean wasRare(Pattern<A,B> pattern, double minRarity, double maxRarity) {
		double freq = Patterns.getRelativeFrequency(pattern, true);
		return freq >= minRarity && freq < maxRarity;
	}
	
}

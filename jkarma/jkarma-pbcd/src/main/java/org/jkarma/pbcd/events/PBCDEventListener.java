/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.events;

import com.google.common.eventbus.Subscribe;

/**
 * This interface prescribes the public interface of a custom Event Listener 
 * specifically designed for listening to pattern-based change detection (PBCD) 
 * related events.
 * 
 * @author Angelo Impedovo
 * @param <A> Type expressing the alphabet of allowed pattern symbols in the PBCD.
 * @param <B> Type expressing the evaluation of pattern in the PBCD.
 */
public interface PBCDEventListener<A extends Comparable<A>, B> {
	
	/**
	 * Method called at the beginning of the mining step in a PBCD pipeline.  
	 * @param event A PatternUpdateStartedEvent associated to the PBCD at hand.
	 */
	@Subscribe
	public void patternUpdateStarted(PatternUpdateStartedEvent<A,B> event);
	
	
	
	/**
	 * Method called at the end of the mining step in a PBCD pipeline.  
	 * @param event A PatternUpdateCompletedEvent associated to the PBCD at hand.
	 */
	@Subscribe
	public void patternUpdateCompleted(PatternUpdateCompletedEvent<A,B> event);

	
	
	/**
	 * Method called at the beginning of the change characterization step in a PBCD pipeline.  
	 * @param event A ChangeDescriptionStartedEvent associated to the PBCD at hand.
	 */
	@Subscribe
	public void changeDescriptionStarted(ChangeDescriptionStartedEvent<A,B> event);
	
	
	
	/**
	 * Method called at the end of the change characterization step in a PBCD pipeline.  
	 * @param event A ChangeDescriptionCompletedEvent associated to the PBCD at hand.
	 */
	@Subscribe
	public void changeDescriptionCompleted(ChangeDescriptionCompletedEvent<A,B> event);
	
	
	
	/**
	 * Method called when the PBCD pipeline succeed in detecting a change, that is when
	 * the change score exceeds the minimum change threshold.  
	 * @param event A ChangeDetectedEvent associated to the PBCD at hand.
	 */
	@Subscribe
	public void changeDetected(ChangeDetectedEvent<A,B> event);

	
	
	/**
	 * Method called when the PBCD pipeline fails in detecting a change, that is when
	 * the change score do not exceed the minimum change threshold.  
	 * @param event A ChangeNotDetectedEvent associated to the PBCD at hand.
	 */
	@Subscribe
	public void changeNotDetected(ChangeNotDetectedEvent<A,B> event);
	
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.datastreams;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;

import org.jkarma.model.Transaction;

/**
 * Class used for representing a block of transactions.
 * The block is a list of transactions of fixed size, the size of the block
 * is termed as capacity, and is known in advance.
 * @author Angelo Impedovo
 *
 * @param <A> Type of transactions in the block.
 */
public final class Block<A extends Transaction<?>> implements Iterable<A> {
	
	/**
	 * The list of transactions.
	 */
	private ArrayList<A> transactions;
	
	/**
	 * The capacity of the block.
	 */
	private int capacity;
	
	
	/**
	 * Constructs a block having given capacity.
	 * The constructed block can contain up to capacity transactions.
	 * @param capacity The capacity of the block.
	 */
	public Block(int capacity) {
		this.transactions = new ArrayList<A>(capacity);
		this.capacity = capacity;
	}
	
	
	
	/**
	 * Returns the timestamp associated with the first transaction of the block.
	 * @return The timestamp associated with the first transaction of the block.
	 */
	public Instant getFirstInstant() {
		Instant result = null;
		if(!this.isEmpty()) {
			A firstTransaction = this.getFirstTransaction();
			result = firstTransaction.getTimestamp();
		}
		return result;
	}
	
	
	
	/**
	 * Returns the timestamp associated with the last transaction of the block.
	 * @return The timestamp associated with the last transaction of the block.
	 */
	public Instant getLastInstant() {
		Instant result = null;
		if(!this.isEmpty()) {
			A lastTransaction = this.getLastTransaction();
			result = lastTransaction.getTimestamp();
		}
		return result;
	}
	
	
	
	/**
	 * Returns the first transaction of the block.
	 * @return The first transaction of the block.
	 */
	public A getFirstTransaction(){
		A transaction = null;
		if(!this.isEmpty()) {
			transaction = this.transactions.get(0);
		}
		return transaction;
	}
	
	
	
	/**
	 * Returns the last transaction of the block.
	 * @return The last transaction of the block.
	 */
	public A getLastTransaction(){
		A transaction = null;
		if(!this.isEmpty()) {
			transaction = (A) this.transactions.get(this.getCount()-1);
		}
		return transaction;
	}
	
	
	
	/**
	 * Returns the number of transaction currently in the block.
	 * This value is always lower or equal.
	 * @return The number of transaction currently in the block.
	 */
	public int getCount() {
		return this.transactions.size();
	}

	
	
	/**
	 * Returns the capacity of the block.
	 * @return The capacity of the block.
	 */
	public int getCapacity() {
		return this.capacity;
	}
	
	
	
	/**
	 * Returns true if the block does not contains transactions, false otherwise.
	 * @return True if the block does not contains transactions, false otherwise.
	 */
	public boolean isEmpty() {
		return this.getCount()==0;
	}
	
	
	
	/**
	 * Returns true if the block contains a number of transactions equal to capacity, false otherwise.
	 * @return True if the block contains a number of transactions equal to capacity, false otherwise.
	 */
	public boolean isFull() {
		return this.getCount()==this.getCapacity();
	}
	
	
	
	/**
	 * Adds a transaction to the block.
	 * If the block is full, the transaction is not added.
	 * @param transaction The transaction to be added.
	 * @return True if the transaction has been added to the block, false otherwise.
	 */
	public boolean addTransaction(A transaction) {
		boolean result = false;
		if(!this.isFull()) {
			this.transactions.add(transaction);
			result = true;
		}
		return result;
	}

	@Override
	public Iterator<A> iterator() {
		return this.transactions.iterator();
	}
	
	@Override
	public String toString() {
		return this.getFirstInstant()+", "+this.getLastInstant();
	}
	
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.detectors;

import java.util.function.BiFunction;

import org.jkarma.pbcd.descriptors.Descriptor;
import org.jkarma.pbcd.descriptors.EmptyDescriptor;
import org.jkarma.pbcd.patterns.Pattern;
import org.jkarma.pbcd.similarities.Similarity;
import org.jkarma.mining.structures.MiningStrategy;
import org.jkarma.model.Transaction;

/**
 * Builder class serving as a proxy for defining the various aspects of
 * a fully functional PBCD pipeline.
 * @author Angelo Impedovo
 *
 * @param <A> Type specifying the alphabet of symbols allowed in patterns.
 * @param <B> Type specifying the pattern evaluation.
 * @param <C> Type specifying the weight associated to patterns in a time window.
 */
public class DetectorProxy<A extends Comparable<A>, B, C> {
	
	/**
	 * The mining strategy to be used in the mining step of the PBCD.
	 */
	private MiningStrategy<A,B> miningStrategy;
	
	/**
	 * The characterization strategy to be used in the change characterization
	 * step of the PBCD.
	 */
	private Descriptor<A,B> descriptor;
	
	/**
	 * A weighting scheme used to assign weights to patterns in
	 * the time windows.
	 */
	private BiFunction<Pattern<A,B>,Boolean,C> weighter;
	
	/**
	 * The similarity criteria to be used in the detection step when
	 * measuring the amount of change between patterns.
	 * In particular, the similarity acts on two vector encodings
	 * of patterns in which a weight is associated to patterns in time windows.
	 * @see DetectorProxy#weighter
	 */
	private Similarity<C> similarity;
	
	
	/**
	 * Partial clone-constructor of a DetectorProxy<A,B,?> object.
	 * The constructor clone all infos except the weighting scheme and the
	 * similarity.
	 * @param proxy The cloned DetectorProxy object.
	 */
	private DetectorProxy(DetectorProxy<A,B,?> proxy) {
		this.miningStrategy = proxy.miningStrategy;
		this.descriptor = proxy.descriptor;
	}

	
	
	/**
	 * Constructs a DetectorProxy<A,B,?> with a coherent MiningStrategy<A,B> object.
	 * The DetectorProxy is by default equipped with an EmptyDescriptor<A,B>, this
	 * means that the PBCD will not possess a change characterization step.
	 * @param miningStrategy The DetectorProxy object.
	 */
	protected DetectorProxy(MiningStrategy<A,B> miningStrategy) {
		if(miningStrategy==null) {
			throw new IllegalArgumentException();
		}
		this.miningStrategy = miningStrategy;
		this.descriptor = new EmptyDescriptor<A,B>();
	}
	
	
	
	/**
	 * Assigns a Descriptor to the DetectorProxy.
	 * The change characterization step in the PBCD pipeline will be executed
	 * according to the strategy in the Descriptor object.
	 * @param descriptor A Descriptor implementing the change characterization step.
	 * @return The DetectorProxy object.
	 */
	public DetectorProxy<A,B,C> describe(Descriptor<A,B> descriptor){
		if(descriptor==null) {
			throw new NullPointerException();
		}
		this.descriptor = descriptor;
		return this;
	}


	
	/**
	 * Returns a DetectorProxy object in which a real-valued weighting scheme
	 * for patterns will be used in the detection step. 
	 * @param weighter The weighting scheme assigning real-valued weights to 
	 *  patterns in time windows.
	 * @param measure The similarity measure working on real-valued vectors of weights.
	 * @return The DetectorProxy object.
	 */
	public DetectorProxy<A,B,Double> weighted(BiFunction<Pattern<A,B>,Boolean,Double> weighter, Similarity<Double> measure) {
		if(weighter==null || measure==null) {
			throw new NullPointerException();
		}
		return this.customize(weighter, measure);
	}
	
	
	
	/**
	 * Returns a DetectorProxy object in which a binary weighting scheme
	 * for patterns will be used in the detection step. 
	 * @param weighter The weighting scheme assigning binary weights to 
	 *  patterns in time windows.
	 * @param measure The similarity measure working on binary vectors of weights.
	 * @return The DetectorProxy object.
	 */
	public DetectorProxy<A,B,Boolean> unweighted(BiFunction<Pattern<A,B>,Boolean,Boolean> weighter, Similarity<Boolean> measure) {
		if(weighter==null || measure==null) {
			throw new NullPointerException();
		}
		return this.customize(weighter, measure);
	}
	
	
	
	/**
	 * Returns a DetectorProxy object in which a custom weighting scheme
	 * for patterns will be used in the detection step. 
	 * @param <D> Type of weights.
	 * @param weighter The weighting scheme assigning custom weights to 
	 *  patterns in time windows.
	 * @param measure The similarity measure working on custom-valued vectors of weights.
	 * @return The DetectorProxy object.
	 */
	public <D> DetectorProxy<A,B,D> customize(BiFunction<Pattern<A,B>,Boolean,D> weighter, Similarity<D> measure){
		DetectorProxy<A,B,D> proxy = new DetectorProxy<A,B,D>(this);
		proxy.weighter = weighter;
		proxy.similarity = measure;		
		return proxy;
	}
	
	
	
	/**
	 * Finalizes the PBCD object according to the strategies defined.
	 * @param <D> The type of transaction to be consumed by the PBCD.
	 * @param minChange The minimum change threshold to be used in the detection step.
	 * @param blockSize The number of transactions after which to detect a new change.
	 * @return The fully functional PBCD.
	 */
	public <D extends Transaction<A>> PBCD<D,A,B,C> build(float minChange, int blockSize){
		return new PBCD<D,A,B,C>(
			miningStrategy, weighter, similarity, descriptor, blockSize, minChange, minChange
		);
	}
	
	
	/**
	 * Finalizes the PBCD object according to the strategies defined.
	 * This methods creates a PBCD which uses two threshold: the first for detecting changes
	 * and the second serving as a memory tradeoff.
	 * @param <D> The type of transaction to be consumed by the PBCD.
	 * @param minChange The minimum change threshold to be used in the detection step.
	 * @param blockSize The number of transactions after which to detect a new change.
	 * @param tradeoff The minimum change threshold to be used for discarding previous transactions.
	 * @return The fully functional PBCD.
	 */
	public <D extends Transaction<A>> PBCD<D,A,B,C> buildWithTradeoff(float minChange, int blockSize, float tradeoff){
		return new PBCD<D,A,B,C>(
			miningStrategy, weighter, similarity, descriptor, blockSize, tradeoff, minChange
		);
	}
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.events;

import org.jkarma.pbcd.datastreams.Block;
import org.jkarma.pbcd.datastreams.TimeWindow;
import org.jkarma.model.Transaction;


/**
 * Abstract implementation of a generic event associated to a PBCD pipeline.
 * Every PBCDEvent is associated to a reference window and to a block of incoming
 * transactions. The window and the block are disjoint.
 * @author Angelo Impedovo
 * 
 * @param <A> Type expressing the alphabet of allowed pattern symbols in the PBCD.
 * @param <B> Type expressing the evaluation of pattern in the PBCD.
 */
public abstract class PBCDEvent<A extends Comparable<A>, B> {
	
	/**
	 * The reference window on which the PBCDEvent has been raised.
	 */
	private TimeWindow landmarkWindow;
	
	/**
	 * The last block of incoming transaction participating in the PBCDEvent.
	 */
	private Block<? extends Transaction<A>> latestBlock;
	
	
	/**
	 * Constructs a PBCDEvent considering a reference window and an incoming block of 
	 * transactions.
	 * @param landmark The reference window.
	 * @param latestBlock The last block of incoming transactions.
	 */
	public PBCDEvent(TimeWindow landmark, Block<? extends Transaction<A>> latestBlock) {
		if(landmark==null || latestBlock==null) {
			throw new NullPointerException();
		}
		this.landmarkWindow = landmark;
		this.latestBlock = latestBlock;
	}

	
	
	/**
	 * Returns the reference window associated to the event.
	 * @return The reference window associated to the event.
	 */
	public TimeWindow getLandmarkWindow() {
		return landmarkWindow;
	}

	
	
	/**
	 * Returns the last block of incoming transaction associated to the event.
	 * @return The last block of incoming transaction associated to the event.
	 */
	public Block<? extends Transaction<A>> getLatestBlock() {
		return latestBlock;
	}	

}

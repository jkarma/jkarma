/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.patterns;

import org.jkarma.mining.interfaces.ItemSet;
import org.jkarma.mining.interfaces.Lattice;
import org.jkarma.mining.structures.Pair;

/**
 * Convenience class for abstracting the content of an ItemSet
 * as intended in the PBCD process.
 * The main difference between a Pattern object and an ItemSet
 * object is that: i) Pattern includes a reference to the lattice from which
 * the ItemSet is drawn, ii) Pattern exposes the two evaluations of
 * type B according to the WindowingStrategy adopted in the PBCD process.
 * @author Angelo Impedovo
 *
 * @param <A> Type indicating the pattern's alphabet of symbols.
 * @param <B> Type indicating the pattern's evaluation.
 */
public class Pattern<A extends Comparable<A>, B> {
	
	/**
	 * Reference to the lattice from which the pattern is drawn.
	 */
	private Lattice<Pattern<A,B>> owner;
	
	/**
	 * ItemSet backing this Pattern instance.
	 */
	private ItemSet<A,Pair<B>> pattern;
	
	/**
	 * First pattern evaluation according to the WindowingStrategy used in the PBCD.
	 * w1 is referred to as the "past time window".
	 */
	private B w1;
	
	/**
	 * Second pattern evaluation according to the WindowingStrategy used in the PBCD.
	 * ww is referred to as the "recent time window".
	 */
	private B w2;
	
	
	public Pattern(Lattice<Pattern<A,B>> owner, ItemSet<A,Pair<B>> pattern, B w1, B w2) {
		super();
		if(owner==null || pattern==null) {
			throw new NullPointerException();
		}
		this.owner = owner;
		this.pattern = pattern;
		this.w1 = w1;
		this.w2 = w2;
	}

	
	
	/**
	 * Returns the lattice from which this pattern is drawn.
	 * @return The lattice from which this pattern is drawn.
	 */
	public Lattice<Pattern<A,B>> getLattice(){
		return this.owner;
	}

	
	
	/**
	 * Returns the ItemSet instance backing the pattern.
	 * @return The ItemSet instance backing the pattern.
	 */
	public ItemSet<A,Pair<B>> getItemSet(){
		return this.pattern;
	}
	
	
	
	/**
	 * Returns the pattern evaluation associated to the past time window.
	 * @return The pattern evaluation associated to the past time window.
	 */
	public B getFirstEval() {
		return this.w1;
	}
	
	
	
	/**
	 * Returns the pattern evaluation associated to the recent time window.
	 * @return The pattern evaluation associated to the recent time window.
	 */
	public B getSecondEval() {
		return this.w2;
	}
	
	
	
	/**
	 * Returns a string representation of the pattern.
	 * @return A string representation of the pattern.
	 */
	@Override
	public String toString() {
		return this.pattern+"\n\t"+this.w1+"\n\t"+this.w2;
	}
}

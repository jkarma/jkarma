/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.detectors;

import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jkarma.pbcd.patterns.Pattern;
import org.jkarma.mining.interfaces.ItemSet;
import org.jkarma.mining.interfaces.Lattice;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.windows.WindowingStrategy;


/**
 * A convenience wrapper turning the lattice mined by the mining strategy
 * (which is a Lattice) to a virtual lattice of patterns(which is a Lattice).
 * Any PBCD benefits of a virtual lattice that acts as an abstraction layer
 * when computing the evaluation accordingly to the windowing system adopted.
 * NOTE: both the detection step (weights construction and distance measuring)
 * and the change characterization step will act on a lattice of Pattern 
 * rather than on a lattice of ItemSet, this allows to straightforwardly use 
 * the methods defined in the Patterns class.
 * @author Angelo Impedovo
 * @see Pattern
 */
public class VirtualLattice<B extends Comparable<B>, C> implements Lattice<Pattern<B,C>> {
	
	
	/**
	 * The lattice backing the virtual lattice.
	 */
	private Lattice<ItemSet<B,Pair<C>>> lattice;
	
	
	
	/**
	 * The time window model used to build the virtual content of the virtual lattice.
	 */
	private WindowingStrategy<C> windowing;
	
	public VirtualLattice(Lattice<ItemSet<B, Pair<C>>> realLattice, WindowingStrategy<C> windowing) {
		if(realLattice==null || windowing==null) {
			throw new IllegalArgumentException();
		}
		this.lattice = realLattice;
		this.windowing = windowing;
	}

	@Override
	public Iterator<Pattern<B, C>> iterator() {
		return this.stream().iterator();
	}

	@Override
	public int getCount() {
		return this.lattice.getCount();
	}

	@Override
	public Collection<Pattern<B, C>> getGreaterElements(Pattern<B, C> element) {
		return this.lattice.getGreaterElements(element.getItemSet()).stream()
			.map(itemset -> this.toPattern(itemset))
			.collect(Collectors.toList());
		
	}

	@Override
	public Collection<Pattern<B, C>> getLowerElements(Pattern<B, C> element) {
		return this.lattice.getLowerElements(element.getItemSet()).stream()
			.map(itemset -> this.toPattern(itemset))
			.collect(Collectors.toList());
	}

	@Override
	public Pattern<B, C> getJoin(Pattern<B, C> firstElement, Pattern<B, C> secondElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Pattern<B, C> getMeet(Pattern<B, C> firstElement, Pattern<B, C> secondElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Pattern<B, C> getMinimum() {
		return this.toPattern(this.lattice.getMinimum());
	}

	@Override
	public Pattern<B, C> getMaximum() {
		return this.toPattern(this.lattice.getMaximum());
	}

	@Override
	public boolean isLimited() {
		return this.isLimited();
	}

	@Override
	public Stream<Pattern<B, C>> stream() {
		return this.lattice.stream().map(itemset -> this.toPattern(itemset));
	}
	
	
	/**
	 * Convenience method used to convert an ItemSet to a Pattern object.
	 * @param itemset The itemset to be converted.
	 * @return The converted virtual pattern object.
	 */
	private Pattern<B,C> toPattern(ItemSet<B,Pair<C>> itemset){
		Pattern<B,C> result = null;
		
		if(itemset!= null) {
			//then we arrange the itemset evaluations accordingly
			Pair<C> eval = itemset.getEval();
			C e1 = null;
			C e2 = null;
			if(eval!=null) {
				e1 = this.windowing.getRemoteWindow(eval.getAggregate(), eval.getIncrement());
				e2 = this.windowing.getRecentWindow(eval.getAggregate(), eval.getIncrement());
			}
			
			//and we build the pattern
			result = new Pattern<B,C>(this,itemset,e1,e2);
		}
		
		return result;
	}

}

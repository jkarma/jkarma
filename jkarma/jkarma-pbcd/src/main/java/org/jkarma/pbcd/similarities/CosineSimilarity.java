/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.similarities;

import java.util.Iterator;
import java.util.Vector;

/**
 * This class defines the cosine dissimilarity between two vectors
 * of Double. The cosine dissimilarity is defined as the 1-complement
 * of the associated cosine similarity, which is measure the how similar
 * two vectors are based on the cosine of the angle between them.
 * @author Angelo Impedovo
 */
public class CosineSimilarity implements Similarity<Double> {
	
	/**
	 * Computes the cosine dissimilarity score between two vectors u and v.
	 * @param u the first vector.
	 * @param v the second vector.
	 * @return the cosine dissimilarity score between u and v.
	 */
	@Override
	public Double apply(Vector<Double> u, Vector<Double> v) {
		Iterator<Double> uIterator = u.iterator();
		Iterator<Double> vIterator = v.iterator();
		double uSquaredSum = 0;
		double vSquaredSum = 0;
		double dot = 0;
		
		while(uIterator.hasNext()) {
			double uIth = uIterator.next();
			double vIth = vIterator.next();
			dot += uIth * vIth;
			uSquaredSum += Math.pow(uIth, 2);
			vSquaredSum += Math.pow(vIth, 2);
		}
		
		return 1 - dot/(Math.sqrt(uSquaredSum) * Math.sqrt(vSquaredSum));
	}

}

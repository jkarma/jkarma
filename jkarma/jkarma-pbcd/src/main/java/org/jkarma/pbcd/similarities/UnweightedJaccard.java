/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.pbcd.similarities;

import java.util.Iterator;
import java.util.Vector;

/**
 * This class defines a unweighted jaccard dissimilarity between two vectors
 * of Boolean. The unweighted jaccard dissimilarity is defined as the 1-complement
 * of the associated unweighted jaccard similarity, which is equivalent to the ratio
 * between the cardinality of the intersection (pairwise-and) and the cardinality 
 * of the union (pairwise-or).
 * @author Angelo Impedovo
 */
public class UnweightedJaccard implements Similarity<Boolean> {

	/**
	 * Computes the unweighted jaccard dissimilarity score between two vectors u and v.
	 * @param u the first vector.
	 * @param v the second vector.
	 * @return the unweighted jaccard dissimilarity score between u and v.
	 */
	@Override
	public Double apply(Vector<Boolean> u, Vector<Boolean> v) {
		Iterator<Boolean> uIterator = u.iterator();
		Iterator<Boolean> vIterator = v.iterator();
		long unionCardinality = 0;
		long intersectCardinality = 0;
		double result = 0;
		
		while(uIterator.hasNext()) {
			boolean uIth = uIterator.next();
			boolean vIth = vIterator.next();
			
			if(uIth && vIth) {
				intersectCardinality++;
			}
		
			
			if(uIth || vIth) {
				unionCardinality++;
			}
		}
		
		if(intersectCardinality==0 && unionCardinality==0) {
			result = 1;
		}else {
			result = (double)intersectCardinality/(double)unionCardinality;
		}
		
		return 1 - result;
	}

}

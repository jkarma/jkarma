/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.test.providers;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Consumer;

import org.jkarma.mining.joiners.TidSet;
import org.jkarma.mining.providers.TidSetProvider;
import org.jkarma.mining.structures.TransactionException;
import org.jkarma.mining.test.utils.Utilities;
import org.jkarma.mining.windows.Windows;
import org.jkarma.model.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

public class TidlistProviderTest extends AbstractProviderTest<String, TidSet> {

	@Parameters(name = "configuration {index} -> |D|={0}, |I|={1}, seed={2}")
    public static Collection<Object[]> data() {
    	Object[][] data = new Object[][]{
    		{100, 20, 10},
    		{200, 20, 10},
    		{300, 20, 10},
    		{100, 40, 20},
    		{200, 40, 20},
    		{300, 40, 20}
        };
        return Arrays.asList(data);
    }
	
	@Before
	public void initializeTest() {
		//random generator
		Random random = new Random(this.seed);
		
		//genero l'insieme degli oggetti
		this.items = Utilities.getGenerators(this.symbolCount);
		
		//genero uno stream di transazioni sugli oggetti osservati
		this.stream = Utilities.getRandomTransactionStream(this.items, this.transactionCount, random);

		//costruisco i data providers
		this.provider = new TidSetProvider<>(Windows.cumulativeLandmark());
	} 
	

	
	@Test
	public void testAlternedCorrectness() throws TransactionException{
		TreeMap<String, Set<Integer>> data = new TreeMap<>();
		
		//we consume the stream
		this.stream.forEach(
			new Consumer<Transaction<String>>() {

				@Override
				public void accept(Transaction<String> transaction) {
					//read the transaction id...
					int tid = transaction.getId();
					
					//we consume the stream...
					provider.accept(transaction);
					if(transaction.getId() % 5 != 0) {
						provider.flatten();
						for(String item : transaction) {
							Set<Integer> basket;
							if(data.containsKey(item)) {
								basket = data.get(item);
								basket.add(tid);
							}else{
								basket = new HashSet<>();
								basket.add(tid);
								data.put(item, basket);
							}
						}
						
						assertCorrectness(data);
					}else {
						provider.forgetRecent();
						assertCorrectness(data);
					}
				}
			}
		);
	}
	
	
	
	private void assertCorrectness(TreeMap<String, Set<Integer>> data) {
		for(String item : this.items) {
			TidSet cached = this.provider.getPastByKey(item);
			Set<Integer> real = data.get(item);
			//System.out.println(cached+" vs "+real);
			if(real!=null) {
				assertTrue(cached.equals(real));
				assertTrue(cached.getTransactionCount()==this.provider.getContext().consumedTransactionsCount);
			}			
		}
	}
}

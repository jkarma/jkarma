/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.test.utils;

import org.jkarma.mining.events.ItemSetAddedEvent;
import org.jkarma.mining.events.ItemSetAddedRegisteredEvent;
import org.jkarma.mining.events.ItemSetEventListener;
import org.jkarma.mining.events.ItemSetRemovedEvent;
import org.jkarma.mining.events.ItemSetRemovedRegisteredEvent;
import org.jkarma.mining.events.ItemSetUpdatedEvent;


/**
 * A dummy event listener to be used only in the various test cases.
 * The event listener is used to count the itemsets updated, deleted and
 * added to a lattice by a mining strategy.
 * 
 * @author Angelo Impedovo
 *
 * @param <A> The type of the items of which the itemset consist.
 * @param <B> The type of the evaluation of an itemset.
 */
public class LatticeEventListener<A,B> implements ItemSetEventListener<A, B> {
	
	private int itemsetAddedCount = 0;
	private int itemsetRemovedCount = 0;
	private int itemsetUpdatedCount = 0;
	private int itemsetRegisteredAsAddedCount = 0;
	private int itemsetRegisteredAsRemovedCount = 0;
	
	/**
	 * Callback function called whenever the strategy adds
	 * permanently a new itemset to the lattice.
	 * The function is alwasy called during a commit operation.
	 */
	@Override
	public void itemsetAdded(ItemSetAddedEvent<A, B> event) {
		this.itemsetAddedCount++;
	}

	
	/**
	 * Callback function called whenever the strategy removes
	 * permanently an itemset from the lattice.
	 * The function is always called during a commit operation.
	 */
	@Override
	public void itemsetRemoved(ItemSetRemovedEvent<A, B> event) {
		this.itemsetRemovedCount++;
	}

	
	
	/**
	 * Callback function called whenever the strategy updates
	 * an itemset from the lattice.
	 * The function is always called before any commit operation.
	 */
	@Override
	public void itemsetUpdated(ItemSetUpdatedEvent<A, B> event) {
		//System.out.println("UPD itemset: "+event.getItemSet()+" evaluated as "+event.getItemSet().getEval());
		this.itemsetUpdatedCount++;
	}

	
	
	/**
	 * Callback function called whenever the strategy marks an itemset
	 * as "added" to the lattice, the itemset will be actually added
	 * during the next commit.
	 * The function is always called before any commit operation.
	 */
	@Override
	public void addedItemsetRegistered(ItemSetAddedRegisteredEvent<A, B> event) {
		System.out.println("ADD itemset: "+event.getItemSet()+" evaluated as "+event.getItemSet().getEval());
		this.itemsetRegisteredAsAddedCount++;
	}

	
	
	/**
	 * Callback function called whenever the strategy marks an itemset
	 * as "removed" from the lattice, the itemset will be actually removed
	 * during the next commit.
	 * The function is always called before any commit operation.
	 */
	@Override
	public void removedItemsetRegistered(ItemSetRemovedRegisteredEvent<A, B> event) {
		//System.err.println("REM itemset: "+event.getItemSet()+" evaluated as "+event.getItemSet().getEval());
		this.itemsetRegisteredAsRemovedCount++;
	}
	
	
	
	/**
	 * Helper function that resets any counter of this event listener.
	 * Used only in the various test cases.
	 */
	public void reset() {
		this.itemsetAddedCount = 0;
		this.itemsetRegisteredAsAddedCount = 0;
		this.itemsetRegisteredAsRemovedCount = 0;
		this.itemsetRemovedCount = 0;
		this.itemsetUpdatedCount = 0;
	}
	
	
	
	/**
	 * Returns the number of itemsets actually added to the lattice
	 * during the last commit.
	 * 
	 * @return int
	 */
	public int getItemsetAddedCount() {
		return itemsetAddedCount;
	}
	
	
	
	/**
	 * Returns the number of itemsets actually removed from the lattice
	 * during the last commit.
	 * 
	 * @return int
	 */
	public int getItemsetRemovedCount() {
		return itemsetRemovedCount;
	}
	
	
	
	/**
	 * Returns the number of itemsets actually updated during the last
	 * operation on the lattice.
	 * 
	 * @return int
	 */
	public int getItemsetUpdatedCount() {
		return itemsetUpdatedCount;
	}
	
	

	/**
	 * Returns the number of itemsets that will be actually added to the
	 * lattice during the next commit operation.
	 * 
	 * @return int
	 */
	public int getItemsetRegisteredAsAddedCount() {
		return itemsetRegisteredAsAddedCount;
	}

	
	
	/**
	 * Returns the number of itemsets that will be actually removed from
	 * the lattice during the next commit operation.
	 * 
	 * @return int 
	 */
	public int getItemsetRegisteredAsRemovedCount() {
		return itemsetRegisteredAsRemovedCount;
	}	
}

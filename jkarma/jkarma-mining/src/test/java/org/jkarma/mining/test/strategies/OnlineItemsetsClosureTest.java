/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.test.strategies;

import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import org.jkarma.mining.joiners.ProjectedDB;
import org.jkarma.mining.providers.ProjectedDBProvider;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.structures.Strategies;
import org.jkarma.mining.test.utils.LatticeEventListener;
import org.jkarma.mining.test.utils.Utilities;
import org.jkarma.mining.windows.Windows;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


/**
 * Test suite designed to check the correctness of
 * mining frequent itemsets using the Eclat algorithm in an online setting.
 * The test cases will use an incremental block-wise mining strategy
 * implementing the Eclat algorithm.
 * 
 * @author Angelo Impedovo
 */
@RunWith(Parameterized.class)
public class OnlineItemsetsClosureTest extends AbstractOnlineItemsetTest<String, ProjectedDB<String>>{
	
	@Parameters(name = "configuration {index} -> minSup={0}, blocks={1}, block size={2}, maxDepth={3}, seed={4}, |I|={5}")
    public static Collection<Object[]> data() {
    	Object[][] data = new Object[][]{
    		{0.01f, 2, 10,   10, 1, 10}
        };
        return Arrays.asList(data);
    }
	
    
    
	@Before
	public void initializeTest() {
		//random generator
		Random random = new Random(this.seed);
		    	
		//genero n generatori
		this.items = Utilities.getGenerators(this.itemCount);
		
		//genero uno stream di transazioni sugli oggetti osservati
		this.stream = Utilities.getBlockwiseContiguousTransactionStream(this.items, this.blockCount, this.blockSize, random).sequential();

		//costruisco i data providers
		this.provider = new ProjectedDBProvider<String>(Windows.cumulativeLandmark());
						
		//costruisco il reticolo
		this.strategy = Strategies.uponItemsets(items)
				.limitDepth(this.maxDepth)
				.lcm(this.minSupport)
				.dfs(this.provider);
		
		
		//registro l'ascoltatore di eventi
		this.listener = new LatticeEventListener<String,Pair<ProjectedDB<String>>>();
		this.strategy.registerListener(this.listener);
		System.out.println("begin------");
	} 
}

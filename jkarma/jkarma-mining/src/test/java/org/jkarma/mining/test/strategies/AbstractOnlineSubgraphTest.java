/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.test.strategies;

import static org.junit.Assert.assertTrue;

import java.util.function.Consumer;

import org.jkarma.mining.interfaces.ItemSet;
import org.jkarma.mining.interfaces.Lattice;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.structures.TransactionException;
import org.jkarma.model.LabeledEdge;
import org.jkarma.model.Transaction;
import org.graphstream.algorithm.Toolkit;
import org.graphstream.graph.implementations.MultiGraph;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

@RunWith(Parameterized.class)
public abstract class AbstractOnlineSubgraphTest<B> extends AbstractOnlineLatticeTest<LabeledEdge,B> {
	
	@Parameter(5)
	public int labelCount;
	
	@Parameter(6)
	public int nodeCount;
	
	
	@Test
	public void testConnectedness(){
		//we consume the stream
		this.stream.forEach(
			new Consumer<Transaction<LabeledEdge>>() {
				
				private int transactionCount = 0;
				
				@Override
				public void accept(Transaction<LabeledEdge> t){
					transactionCount++;
					provider.accept(t);
					
					//we check if a block is exhausted
					if(transactionCount % blockSize == 0) {
						//update of the lattice
						Lattice<ItemSet<LabeledEdge, Pair<B>>> lattice;
						try {
							lattice = strategy.execute();
							strategy.commit();
							provider.flatten();
							
							//check the connectedness of any subgraph
							for(ItemSet<LabeledEdge,Pair<B>> subgraph : lattice) {
								ItemSet<LabeledEdge,Pair<B>> edge = subgraph;
								System.out.println(edge);
								MultiGraph graph = new MultiGraph("graph", false, true);
								
								int edgeCount=0;
								while(edge.getSuffix()!=null) {
									String node1 = edge.getKey().getEndNode();
									String node2 = edge.getKey().getStartNode();
									graph.addEdge(Integer.toString(edgeCount), node1, node2, true);
									edgeCount++;
									
									edge = edge.getPrefix();
								}
								
								//weak connectedness test
								boolean connected = Toolkit.isConnected(graph);
								assertTrue(connected);
							}
							//resetto i contatori nel listener
							listener.reset();
						} catch (TransactionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				}
			}
		);
	}
	
}

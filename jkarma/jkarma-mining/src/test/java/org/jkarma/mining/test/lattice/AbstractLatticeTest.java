/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.test.lattice;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.jkarma.mining.interfaces.ItemSet;
import org.jkarma.mining.interfaces.Lattice;
import org.jkarma.mining.providers.BaseProvider;
import org.jkarma.mining.structures.MiningStrategy;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.structures.TransactionException;
import org.jkarma.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

import com.google.common.collect.Sets;

@RunWith(Parameterized.class)
public class AbstractLatticeTest<A extends Comparable<A>, B> {
	
	@Parameter(0)
	public float minSupport;
	
	@Parameter(1)
	public int transactionCount;
	
	@Parameter(2)
	public int maxDepth;
	
	@Parameter(3)
	public long seed;
	
	@Parameter(4)
	public int itemCount;
	
	protected Set<A> items;
	
	protected Stream<Transaction<A>> stream;
	
	protected BaseProvider<A,B> provider;
	
	protected MiningStrategy<A, B> strategy;
	
	private Lattice<ItemSet<A,Pair<B>>> lattice;
	
	
	/**
	 * Test case designed in order to check if the itemset count is consistent with
	 * the detected deletion and addition of itemsets from the lattice.
	 * The test succeed if:
	 * a) the number of removed itemsets at the i-th transaction equals
	 * the number of itemsets marked as "removed".
	 * b) the number of added itemsets at the i-th transaction equals 
	 * the number of itemsets marked as "added".
	 * c) the number of added itemsets plus the number of updated itemsets at the
	 * i-th transaction equals the number of itemsets in the lattice.
	 */
	@Test
	public void testSuperPatternsCorrectness(){
		//we consume the whole stream
		this.stream.forEach(
			new Consumer<Transaction<A>>() {
				
				int tCount = 0;
				
				@Override
				public void accept(Transaction<A> transaction) {
					provider.accept(transaction);
					
					//we check if a block is exhausted
					if(tCount % transactionCount == 0) {
						try {
							lattice = strategy.execute();
							strategy.commit();
							provider.flatten();
						} catch (TransactionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					tCount++;
				}
			}
		);
		
		
		//then we check the correctness of each super pattern
		for(ItemSet<A,Pair<B>> pattern : this.lattice) {
			if(pattern.getSuffix()!=null) {
				//then we iterate on his supersets;
				Collection<ItemSet<A,Pair<B>>> superSets = this.lattice.getLowerElements(pattern);
				Set<A> patternWord = this.patternToWord(pattern);
				
				//counters
				int superSetsCount = superSets.size();
				int counter = 0;
				
				//we manually iterate over the supersets and count them
				for(ItemSet<A,Pair<B>> superSet : this.lattice) {
					Set<A> superWord = this.patternToWord(superSet);
					if(superWord.size()==patternWord.size()+1) {
						if(superWord.containsAll(patternWord)) {
							System.out.println(pattern+ " ---> "+superSet);
							counter++;
						}
					}
				}
				System.out.println("counted: "+counter);
				System.out.println("effective: "+superSetsCount);
				
				assertEquals(counter, superSetsCount);
			}
		}
	}
	
	
	private Set<A> patternToWord(ItemSet<A,Pair<B>> pattern){
		Set<A> word = Sets.newHashSet();
		ItemSet<A,Pair<B>> currentNode = pattern;
		
		while(currentNode.getSuffix()!=null) {
			word.add(currentNode.getSuffix());
			currentNode = currentNode.getPrefix();
		}
		
		return word;
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.test.utils;

import java.time.Instant;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.jkarma.model.Transaction;

class GenericTransaction<A> implements Transaction<A>{
	
	private Collection<A> items;
	private Instant timestamp;
	private int id;
	
	public GenericTransaction(int id, Instant timestamp, Collection<A> items) {
		this.id = id;
		this.timestamp = timestamp;
		this.items = new LinkedList<>(items);
	}

	@Override
	public Iterator<A> iterator() {
		return items.iterator();
	}

	@Override
	public Instant getTimestamp() {
		return this.timestamp;
	}

	@Override
	public Collection<A> getItems() {
		return this.items;
	}

	@Override
	public int getId() {
		return this.id;
	}
	
	@Override
	public String toString() {
		return "{"+this.timestamp+" id:"+this.id+" items:"+this.items+"}";
	}
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.test.strategies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.jkarma.mining.interfaces.ItemSet;
import org.jkarma.mining.interfaces.Lattice;
import org.jkarma.mining.joiners.Aggregator;
import org.jkarma.mining.providers.BaseProvider;
import org.jkarma.mining.structures.MiningStrategy;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.structures.TransactionException;
import org.jkarma.mining.test.utils.LatticeEventListener;
import org.jkarma.mining.test.utils.Utilities;
import org.jkarma.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

@RunWith(Parameterized.class)
public abstract class AbstractOnlineLatticeTest<A extends Comparable<A>, B> {
	
	@Parameter(0)
	public float minSupport;
	
	@Parameter(1)
	public int blockCount;
	
	@Parameter(2)
	public int blockSize;
	
	@Parameter(3)
	public int maxDepth;
	
	@Parameter(4)
	public long seed;
	
	protected Set<A> items;
	
	protected Stream<Transaction<A>> stream;
	
	protected BaseProvider<A,B> provider;
	
	protected MiningStrategy<A, B> strategy;
	
	protected LatticeEventListener<A, Pair<B>> listener;
	
	
	/**
	 * Test case designed in order to check if the itemset count is consistent with
	 * the detected deletion and addition of itemsets from the lattice.
	 * The test succeed if:
	 * a) the number of removed itemsets at the i-th transaction equals
	 * the number of itemsets marked as "removed".
	 * b) the number of added itemsets at the i-th transaction equals 
	 * the number of itemsets marked as "added".
	 * c) the number of added itemsets plus the number of updated itemsets at the
	 * i-th transaction equals the number of itemsets in the lattice.
	 */
	@Test
	public void testPatternAdd(){
		//we consume the stream
		this.stream.forEach(
			new Consumer<Transaction<A>>() {
				
				int transactionCount = 0;
				
				@Override
				public void accept(Transaction<A> transaction){
					System.out.println(transaction);
					provider.accept(transaction);
					
					//we check if a block is exhausted
					if(transactionCount % blockSize == 0) {
						System.out.println("block #"+(transactionCount / blockSize));
						int itemsetCount = 0;
						int presentCount = 0;
						
						try {
							Lattice<ItemSet<A,Pair<B>>> lattice = strategy.execute();
							strategy.commit();
							provider.flatten();
							System.out.println("node count: "+lattice.getCount());
							
							//conto
							itemsetCount = lattice.getCount()-1;
							
							//confronto i conteggi con quanto riportato nel listener
							presentCount = listener.getItemsetAddedCount() + listener.getItemsetUpdatedCount();
							
							//test
							assertEquals(listener.getItemsetRemovedCount(), 
								listener.getItemsetRegisteredAsRemovedCount());
							assertEquals(listener.getItemsetAddedCount(), 
								listener.getItemsetRegisteredAsAddedCount());
							assertEquals(itemsetCount, presentCount);
							
							//resetto i contatori nel listener
							listener.reset();
						} catch (TransactionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					transactionCount++;
					
				}
			}
		);
	}
	
	
	
	/**
	 * Test case designed in order to check if the itemsets count is consistent
	 * with the detected addition and deletion across different commits.
	 * The test succeed if the number of itemsets at the i-th transaction equals
	 * the number of itemsets at the (i-1)-th transaction plus the number of new
	 * added itemsets and minus the number of pruned itemsets.
	 */
	@Test
	public void testConsistencyAcrossCommits(){
		//we consume the stream
		this.stream.forEach(
			new Consumer<Transaction<A>>() {
				
				private int transactionCount = 0;
				private int lastItemsetCount = 0;
				
				@Override
				public void accept(Transaction<A> t){
					transactionCount++;
					
					//we check if a block is exhausted
					if(transactionCount % blockSize == 0) {
						int itemsetCount = 0;
						int calculatedCount = 0;
						
						//update the lattice
						Lattice<ItemSet<A, Pair<B>>> lattice;
						try {
							lattice = strategy.execute();
							strategy.commit();
							provider.flatten();
							
							//conto
							itemsetCount = lattice.getCount()-1;
							calculatedCount = lastItemsetCount + 
									listener.getItemsetAddedCount() - 
									listener.getItemsetRemovedCount();
							
							//test only at the second block
							if(transactionCount > blockSize) {
								assertEquals(itemsetCount, calculatedCount);
							}
							
							//resetto i contatori nel listener
							listener.reset();
							lastItemsetCount = itemsetCount;
						} catch (TransactionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		);
	}
	
	
	
	
	
	/**
	 * Test case designed in order to check whether the evaluation of each itemset
	 * is correctly computed.
	 * The test succeed if the evaluation computed by the algorithm equals the
	 * evaluation manually computed considering by "manually" applying the evaluator
	 * to the complete evaluation of each item of the itemsets.
	 */
	@Test
	public void testCorrectness(){
		Aggregator<B> evaluator = this.strategy.getEvaluator();
		
		//we consume the stream
		this.stream.forEach(
			new Consumer<Transaction<A>>() {
				
				private int transactionCount = 0;
				
				@Override
				public void accept(Transaction<A> t){
					transactionCount++;
					
					//we check if a block is exhausted
					if(transactionCount % blockSize == 0) {
						//update of the lattice
						Lattice<ItemSet<A, Pair<B>>> lattice;
						try {
							lattice = strategy.execute();
							strategy.commit();
							provider.flatten();
							
							//check the correctness of the associated interval for each itemset...
							for(ItemSet<A,Pair<B>> itemset : lattice) {
								//...except from the rood node!
								if(itemset.getSuffix()!=null) {
									//we iterate on the itemset's items using the cached data in the provider
									//in order to reconstruct the overall evaluation of the itemsets.
									ItemSet<A,Pair<B>> node = itemset;
									B total = evaluator.apply(
										itemset.getEval().getAggregate(),  itemset.getEval().getIncrement(), 2);
									List<B> evals = new LinkedList<>();
									
									while(node.getSuffix()!=null) {
										Pair<B> provided = provider.apply(node.getKey());
										B cachedSet = provided.getIncrement();
										evals.add(cachedSet);
										node = node.getPrefix();
									}
									B recomputed = Utilities.mergeEvals(evals, evaluator);
									
									//equality test
									boolean equalityTest = total.equals(recomputed);
									assertTrue(equalityTest);
								}
							}
							
							
						}catch(TransactionException ex) {
							ex.printStackTrace();
						}
					}
				}
			}
		);
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.test.providers;

import java.util.Set;
import java.util.stream.Stream;

import org.jkarma.mining.providers.BaseProvider;
import org.jkarma.model.Transaction;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

@RunWith(Parameterized.class)
public abstract class AbstractProviderTest<A extends Comparable<A>, B> {
	
	@Parameter(0)
	public int transactionCount;
	
	@Parameter(1)
	public int symbolCount;
	
	@Parameter(2)
	public long seed;
    
	protected Stream<? extends Transaction<A>> stream;
    protected BaseProvider<A,B> provider;
    protected Set<A> items;
	
}

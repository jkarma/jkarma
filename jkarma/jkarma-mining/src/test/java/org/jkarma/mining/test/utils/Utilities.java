/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.test.utils;

import java.time.Instant;
import java.time.Period;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.NavigableSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.jkarma.mining.joiners.Aggregator;
import org.jkarma.model.LabeledEdge;
import org.jkarma.model.Transaction;

/**
 * Convenience class that contains various utility functions.
 * @author Angelo Impedovo
 *
 */
public class Utilities {
	
	/**
	 * Generates a set consisting of numberOfItems (from 1 to numberOfItems) 
	 * different strings.
	 * @param numberOfItems the number of items to be returned.
	 * @return A random set of string items.
	 */
	public static Set<String> getGenerators(int numberOfItems){
		return new TreeSet<String>(
			IntStream.rangeClosed(1, numberOfItems)
			.boxed()
			.map(num -> "i"+num)
			.collect(Collectors.toSet())
		);
	}
	
	
	
	/**
	 * Generates the set of all the possible edges that can be constructed
	 * considering numberOfNodes nodes and numberOfLabels edge types.
	 * 
	 * @param numberOfNodes the number of distinct nodes
	 * @param numberOfLabels the number of distinct edges
	 * @return A random set of labeled edges.
	 */
	public static Set<LabeledEdge> getGenerators(int numberOfNodes, int numberOfLabels) {
		TreeSet<LabeledEdge> edges = new TreeSet<>();
		TreeSet<String> labels = new TreeSet<>(
			IntStream.rangeClosed(1, numberOfLabels).boxed().map(n -> "label"+n).collect(Collectors.toSet())
		);
		TreeSet<String> nodes = new TreeSet<>(
			IntStream.rangeClosed(1, numberOfNodes).boxed().map(n -> "n"+n).collect(Collectors.toSet())
		);
		
		for(String node : nodes) {
			NavigableSet<String> otherNodes = nodes.tailSet(node, true);
			for(String otherNode : otherNodes) {
				for(String label : labels) {
					LabeledEdge edge = new LabeledEdge(node, otherNode, label);
					edges.add(edge);
				}
			}
		}
		
		return edges;
	}
	
	
	
	/**
	 * Utility function that computes the global reduction of different pattern evaluations.
	 * @param <A> Type of evaluations.
	 * @param evals a list of sets of the same type
	 * @param joiner a joiner that implements the intersection logic
	 * @return The reduced evaluation.
	 */
	public static <A> A mergeEvals(List<A> evals, Aggregator<A> joiner) {
		A partialEval = null;
		for(int i=0; i<evals.size(); i++) {
			if(i==0) {
				partialEval = evals.get(0);
			}else {
				partialEval = joiner.apply(partialEval, evals.get(i), 2);
			}
		}
		
		return partialEval;
	}



	/**
	 * Utility function that generate a stream of transactions composed of
	 * several items among a set of reference itemsets.
	 * The stream generated is not guaranteed to contain a certain
	 * contiguity of the items over the transaction (@see getBlockwiseContiguousTransactionStream).
	 * @param <A> Type of items inside transactions.
	 * @param items The items composing the transactions.
	 * @param transactionCount The number of random transactions to be generated. 
	 * @param random The random seed.
	 * @return A stream of random transactions.
	 */
	public static <A extends Comparable<A>> Stream<Transaction<A>> getRandomTransactionStream(
			Set<A> items, int transactionCount, Random random){
		List<A> itemList = new ArrayList<>(items);
		List<Transaction<A>> transactions = new LinkedList<>();
		TemporalAmount delta = Period.ofDays(1);
		Instant instant = Instant.now();
		
		for(int id=1; id<=transactionCount; id++) {
			//we build a transaction using a random contiguous selection of items
			int startIndex = random.nextInt(itemList.size());
			int endIndex = startIndex + random.nextInt(itemList.size()-startIndex);
			Collection<A> sample = itemList.subList(startIndex, endIndex);
			Transaction<A> transaction = new GenericTransaction<A>(id, instant, sample);
			
			//we add the transaction
			instant = instant.plus(delta);
			transactions.add(transaction);
		}
		return transactions.stream();
	}
	
	
	
	
	
	/**
	 * Utility function that generate a stream of transactions composed of
	 * several items among a set of reference itemsets.
	 * The stream generated is guaranteed to contain a certain
	 * contiguity of the items over the transaction.
	 * @param <A> Type of items inside transactions.
	 * @param items The items composing the transactions.
	 * @param blockCount The number of transaction blocks to be generated.
	 * @param blockSize The number of transaction per each block.
	 * @param random The random seed.
	 * @return A stream of random transactions.
	 */
	public static <A extends Comparable<A>> Stream<Transaction<A>> getBlockwiseContiguousTransactionStream(
			Set<A> items, int blockCount, int blockSize, Random random){
		int transactionCount = blockCount * blockSize;
		List<A> itemList = new ArrayList<>(items);
		Vector<Transaction<A>> transactions = new Vector<>(transactionCount, 0);
		TemporalAmount delta = Period.ofDays(1);
		Instant instant = Instant.now();
		
		//we create the stream
		for(int id=0; id<transactionCount; id++) {
			//create an empty transaction
			Transaction<A> transaction = new GenericTransaction<A>(id, instant, new HashSet<>());
			transactions.add(transaction);
			instant = instant.plus(delta);
		}
		
		//now we populate the transactions
		for(int block=0; block<blockCount; block++) {
			int blockOffset = block * blockSize;
			
			//for each item we select a proper range in the block
			for(A item : itemList) {
				int startIndex = random.nextInt(blockSize);
				int endIndex = startIndex + random.nextInt(blockSize-startIndex);
				
				while(startIndex!=endIndex) {
					Transaction<A> transaction = transactions.get(blockOffset+startIndex);
					transaction.getItems().add(item);
					startIndex++;
				}
			}
		}
		
		return transactions.stream();
	}
	
	
	
}

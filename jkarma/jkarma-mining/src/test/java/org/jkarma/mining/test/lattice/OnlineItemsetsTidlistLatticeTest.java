/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.test.lattice;

import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import org.jkarma.mining.joiners.TidSet;
import org.jkarma.mining.providers.TidSetProvider;
import org.jkarma.mining.structures.Strategies;
import org.jkarma.mining.test.utils.Utilities;
import org.jkarma.mining.windows.Windows;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class OnlineItemsetsTidlistLatticeTest extends AbstractLatticeTest<String, TidSet>{

	@Parameters(name = "configuration {index} -> minSup={0}, transactions={1}, maxDepth={2}, seed={3}, |I|={4}")
    public static Collection<Object[]> data() {
    	Object[][] data = new Object[][]{
    		{0.00f, 100,   5, 1, 5},
    		{0.05f, 100,   5, 1, 5},
    		{0.10f, 100,   5, 1, 5},
    		{0.15f, 100,   5, 1, 5},
    		{0.20f, 100,   5, 1, 5}
        };
        return Arrays.asList(data);
    }
	
    
    
    
	@Before
	public void initializeTest() {
		//random generator
		Random random = new Random(this.seed);
		    	
		//genero n generatori
		this.items = Utilities.getGenerators(this.itemCount);
		
		//genero uno stream di transazioni sugli oggetti osservati
		this.stream = Utilities.getRandomTransactionStream(this.items, this.transactionCount, random).sequential();

		//costruisco i data providers
		this.provider = new TidSetProvider<>(Windows.cumulativeLandmark());
						
		//costruisco il reticolo
		this.strategy = Strategies.uponItemsets(items)
				.limitDepth(this.maxDepth)
				.eclat(this.minSupport)
				.dfs(this.provider);
			
		System.out.println("========================");
		System.out.println("BEGIN");
		System.out.println("========================");
	}
}

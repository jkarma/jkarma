/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.providers;

import java.util.Map.Entry;

import org.jkarma.mining.joiners.FrequencyEvaluation;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.windows.WindowingStrategy;


/**
 * Defines an abstract based data provider which acts as a mapping
 * from the domain of the items to the domain of the evaluation.
 * The WindowedProvider extends the BaseProvider with the definition of
 * a windowing aggregation scheme.
 * The elements consumed by the provider will be serialized in proper
 * evaluations object according to a windowing strategy.
 * This means that each evaluation keeps track of the window size 
 * accordingly.
 * Note that the window size should be also "copied" in B
 * The class is a Consumer of Transactions, but the logic is not implemented as
 * this clearly depends on the type of evaluations to be provided.
 * @author Angelo Impedovo
 *
 * @param <A> Type of allowed pattern symbols.
 * @param <B> Type of pattern evaluations.
 */
public abstract class WindowedProvider<A,B extends FrequencyEvaluation> extends BaseProvider<A,B> {

	public WindowedProvider(WindowingStrategy<B> windowing) {
		super(windowing);
	}
	
	
	/**
	 * Convenience override, inject transaction count caveat.
	 * We force consistency about the semantics
	 */
	@Override
	public Pair<B> apply(A key){
		Pair<B> result = super.apply(key);
		
		B first = result.getAggregate();
		B second = result.getIncrement();
		
		first.setTransactionCount(this.summary.getCachedCount());
		second.setTransactionCount(this.summary.getConsumedCount());
		return result;
	}
	
	
	/**
	 * Convenience override of method BaseProvider.flatten().
	 * The override is necessary because Context, BaseProvider and TidSet share the notion
	 * of "transaction count" that should be kept consistent with the semantics of the 
	 * WindowingStrategy.andThen() method (<b>this is not guaranteed in the BaseProvider class</b>).
	 */
	@Override
	public void flatten() {
		//we update the context accordingly
		for(Entry<A, Pair<B>> entry : this.cache.entrySet()) {
			Pair<B> memoryContent = entry.getValue();
			B aggregated = this.windowing.andThen(
				memoryContent.getAggregate(), memoryContent.getIncrement() 
			);
			memoryContent.setAggregate(aggregated);
			memoryContent.setIncrement(this.getNewInstance());
			this.summary.cachedTransactionsCount = aggregated.getTransactionCount();
			this.summary.consumedTransactionsCount = 0;
		}		
	}
}

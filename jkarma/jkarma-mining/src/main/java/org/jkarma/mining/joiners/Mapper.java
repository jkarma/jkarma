/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

/**
 * Defines the public interface for a Mapper object.
 * A mapper is a delegate which is responsible of joining two
 * instances of type A into a new instance of type A.
 * From this perspective the Mapper implement the Map semantics
 * of a Map-Reduce-like programming paradigms.
 * However, a mapper is specifically designed for working 
 * in a pattern-mining scenario, so a third parameter, the height
 * in the lattice of patterns, occurs.
 * As to the pattern-mining necessity, the Mapper interface can be
 * implemented by two components: lexicographic joiners joining
 * symbols into itemsets, and pattern evaluators computing an evaluation
 * starting from two previously computed ones.
 * @author Angelo Impedovo
 *
 * @param <A> The type of input objects on which to apply the map.
 * @param <B> The type of the output value.
 */
public interface Mapper<A,B> {	
	
	/**
	 * Maps two object of the same type at a given height in the lattice
	 * of patterns to a new value.
	 * @param t The first object to be mapped.
	 * @param u The second object to be mapped.
	 * @param height The height shared by the two objects in the lattice
	 *  of patterns.
	 * @return The mapped value.
	 */
	public B apply(A t, A u, int height);	
}

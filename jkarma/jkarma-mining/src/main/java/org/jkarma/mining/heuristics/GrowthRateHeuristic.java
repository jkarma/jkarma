package org.jkarma.mining.heuristics;

import org.jkarma.mining.joiners.FrequencyEvaluation;
import org.jkarma.mining.providers.Context;


/**
 * This class implements a pattern heuristic evaluation based on the pattern's growth
 * rate between two time windows.
 * The growth-rate of a pattern is a contrast measure defined as the ratio between 
 * the support on the first window and the support on the second one, respectively.
 * Necessary condition to compute the absolute frequency is that B implements the
 * FrequencyEvaluation interface. 
 * The growth-rate is computed so to have value in [1,Infinity) by dividing the greatest
 * support in the two windows with the lowest, vice-versa the reciprocal version has 
 * values in [0,1]. Growth-rate close to 1 (0 in the reciprocal version) denotes patterns 
 * with very different supports on the two windows.
 * @author Angelo Impedovo
 *
 * @param <A> Type denoting the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 * @see <a href='https://dblp.org/rec/bib/books/crc/dong13/Bailey13'>
 * Statistical Measures for Contrast Patterns, Contrast Data Mining: 
 * Concepts, Algorithms, and Applications</a>
 */
public class GrowthRateHeuristic<A,B extends FrequencyEvaluation> implements Heuristic<A,B>{
	
	/**
	 * If true the heuristic will compute the reciprocal growth-rate in [0,1].
	 * Otherwise the classic growth-rate in [1,Infinity) will be computed.
	 */
	boolean isReciprocal = false;
	
	
	
	/**
	 * Instantiate a growth-rate heuristic.
	 * If the reciprocal flag is set to true, then the reciprocal growth-rate in [0,1]
	 * will be computed. Otherwise, the classic growth-rate in [1, Infinity) will be
	 * computed.
	 * @param reciprocal The reciprocal flag.
	 */
	public GrowthRateHeuristic(boolean reciprocal) {
		this.isReciprocal = reciprocal;
	}

	
	
	@Override
	public double apply(A suffix, B remoteEval, B recentEval, Context ctx, int itemsetLength, boolean onRemote) {
		double oldFreq = remoteEval.getRelativeFrequency();
		double newFreq = recentEval.getRelativeFrequency();
		
		double value;
		if(!isReciprocal) {
			value = Math.max(oldFreq, newFreq)/Math.min(oldFreq, newFreq); 
		}else {
			value = Math.min(oldFreq, newFreq)/Math.max(oldFreq, newFreq);
		}
		
		return value;
	}

}

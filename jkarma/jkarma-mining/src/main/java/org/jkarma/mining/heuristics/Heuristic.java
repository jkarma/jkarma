/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.heuristics;

import org.jkarma.mining.providers.Context;

/**
 * Public interface for heuristic evaluation function to be used
 * when evaluating patterns during mining.
 * Class implementing the Heuristic interface can be used for
 * performing non-exhaustive pattern mining tasks, that is mining
 * patterns according to a beam search.
 * The heuristic may take into account every information associated to the
 * pattern at hand: the suffix, the evaluation, the mining context and the
 * itemset length.
 * @author Angelo Impedovo
 *
 * @param <A> Type denoting the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 */
public interface Heuristic<A,B>{
	
	/**
	 * Computes a heuristic evaluation of a pattern.
	 * @param suffix The last item added to the pattern
	 * @param remoteEval The evaluation of the pattern with past data
	 * @param recentEval The evaluation of the pattern involving new data
	 * @param ctx The mining context
	 * @param itemsetLength The number of items in the pattern
	 * @param onRemote A flag indicating whether to consider the first or the second evaluation (for unary heuristics). 
	 * @return A numerical evaluation of the pattern
	 */
	public double apply(A suffix, B remoteEval, B recentEval, Context ctx, int itemsetLength, boolean onRemote);

}

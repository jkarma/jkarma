/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.interfaces;

/**
 * Public interface for a node of a given tree.
 * A node in a tree data structure may have at most a parent node,
 * different children nodes, an associated informative content and
 * different siblings.
 * Every node, with his descendants node, forms a tree rooted in the
 * node itself.
 * For this reasone te Node interface also extends the Tree interface.
 * @author Angelo Impedovo
 * @param <A> Type of informative content associated to the node.
 */
public interface Node<A> extends Tree<A> {
	
	/**
	 * Returns the informative content associated to the node.
	 * @return The informative content associated to the node.
	 */
	public A getData();
	
	
	
	/**
	 * Returns the parent node.
	 * @return The parent node if present, null otherwise.
	 */
	public Node<A> getParent();
	
	
	
	/**
	 * Returns the next sibling of the node.
	 * @return The next sibling if present, null otherwise.
	 */
	public Node<A> getNextSibling();
	
	
	
	/**
	 * Returns the previous sibling of the node.
	 * @return The previous sibling if present, null otherwise.
	 */
	public Node<A> getPreviousSibling();
	
	
	
	/**
	 * Returns the first child of the node.
	 * @return The first child if any, null otherwise.
	 */
	public Node<A> getFirstChild();
	
	
	
	/**
	 * Returns the last child of the node.
	 * @return The last child if any, null otherwise.
	 */
	public Node<A> getLastChild();
	
	
	
	/**
	 * Returns the list of children nodes.
	 * @return The list of children nodes.
	 */
	public NodeList<? extends Node<A>> getChildren();
	
	
	
	/**
	 * Returns the count of descendants node.
	 * @return The count of descendants node.
	 */
	public int getDescendantsCount();
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.windows;

import org.jkarma.mining.joiners.Aggregator;

/**
 * A windowing strategy defines the logic of grouping transactions
 * coming from the data source, and deciding when to keep and when to
 * discard old ones.
 * In jMiner, WindowingStrategy components are intended to be used for 
 * evaluating patterns in data streams according to a time window model.
 * Every pattern possess a Pair of generic evaluations, of type A, the
 * first according to the all observed transactions (w1 hereafter), and
 * the second according to the transactions in the last block (w2 hereafter).
 * The only purpose of the WindowingStrategy is to arrange these two 
 * evaluations according to a precise windowing scheme.
 * For this reason, the WindowingStrategy is backed by an Aggregator object.
 * 
 * @author Angelo Impedovo
 * @param <A> Type of pattern evaluation.
 */
public interface WindowingStrategy<A> {
	
	/**
	 * Chose or create an evaluation for remote transactions.
	 * The evaluation is built upon a time window based on w1 and w2.
	 * @param w1 The evaluation built on the previously observed transactions.
	 * @param w2 The evaluation built on the last block of transactions.
	 * @return Returns the remote window according to w1, w2 or an aggregation 
	 *  between the two.
	 */
	public A getRemoteWindow(A w1, A w2);
	
	
	
	/**
	 * Chose or create an evaluation for recent transactions.
	 * The evaluation is built upon a time window based on w1 and w2.
	 * @param w1 The evaluation built on the previously observed transactions.
	 * @param w2 The evaluation built on the last block of transactions.
	 * @return Returns the recent window according to w1, w2 or an aggregation 
	 *  between the two.
	 */
	public A getRecentWindow(A w1, A w2);
	
	
	
	/**
	 * Chose or create an evaluation for the transactions which will be
	 * considered as "new remote transactions".
	 * The evaluation is built upon a time window based on w1, w2, and the
	 * last selected recent window.
	 * @param w1 The evaluation built on the previously observed transactions.
	 * @param w2 The evaluation built on the last block of transactions.
	 * @return Returns an aggregated evaluation, concerning transactions from w1
	 *  and w2 which will be kept.
	 */
	public A andThen(A w1, A w2);
	
	
	
	
	/**
	 * Returns the aggregator of pattern evaluations backed by the windowing strategy.
	 * @return The aggregator of pattern evaluations backed by the windowing strategy.
	 */
	public Aggregator<A> getAggregator();
	
	
	
	
	/**
	 * Sets the aggregator of pattern evaluations to be used by the windowing strategy.
	 * @param aggregator The aggregator of pattern evaluations to be used by the windowing strategy.
	 */
	public void setAggregator(Aggregator<A> aggregator);
}

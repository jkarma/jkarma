/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.windows;


/**
 * This class serves as a base class for more complex windowing
 * strategy based on the Landmark time window model. In the 
 * landmark model, the window increases in the size with incoming
 * transactions. By default, no dropping mechanism for old
 * transactions is specified in the landmark model, so this is left
 * as an abstract functionality to be implemented in concrete subclass.
 * @author Angelo Impedovo
 * 
 * @param <A> Type of content from the time window.
 */
public abstract class AbstractLandmarkStrategy<A> extends AbstractWindowingStrategy<A> {

	/**
	 * Chose or create an evaluation for remote transactions.
	 * The evaluation is built upon a time window based on w1 and w2.
	 * In the Landmark window model the evaluation of a pattern in the remote 
	 * window always correspond to w1.
	 * @param w1 The evaluation built on the previously observed transactions.
	 * @param w2 The evaluation built on the last block of transactions.
	 * @return Returns w1.
	 */
	@Override
	public A getRemoteWindow(A w1, A w2) {
		return w1;
	}

	
	
	/**
	 * Chose or create an evaluation for recent transactions.
	 * The evaluation is built upon a time window based on w1 and w2.
	 * In the Landmark window model the evaluation of a pattern in the recent 
	 * window always correspond to the evaluation according to all the transactions
	 * involved in w1 and w2.
	 * @param w1 The evaluation built on the previously observed transactions.
	 * @param w2 The evaluation built on the last block of transactions.
	 * @return Returns the reduction between w1 and w2.
	 */
	@Override
	public A getRecentWindow(A w1, A w2) {
		return this.getAggregator().reduce(w1, w2);
	}

}

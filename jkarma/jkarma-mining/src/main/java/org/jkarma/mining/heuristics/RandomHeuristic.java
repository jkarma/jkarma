/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.heuristics;

import java.util.Random;

import org.jkarma.mining.joiners.FrequencyEvaluation;
import org.jkarma.mining.providers.Context;

/**
 * This class implements a random pattern heuristic evaluation.
 * The randomness is controlled by a numeric seed while repetition on the same evaluation
 * is guaranteed by the evaluation hashCode.
 * @author Angelo Impedovo
 *
 * @param <A> Type denoting the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 */
public class RandomHeuristic<A,B extends FrequencyEvaluation> implements Heuristic<A,B>{
	
	private long initSeed;
	private Random random;
	
	public RandomHeuristic(long seed) {
		this.initSeed = seed;
		this.random = new Random(seed);
	}

	/**
	 * Computes a random score of a pattern.
	 * @param suffix The last item added to the pattern
	 * @param pastEval The evaluation of the pattern on past data
	 * @param newEval The evaluation of the pattern with incoming data
	 * @param ctx The mining context
	 * @param itemsetLength The number of items in the pattern 
	 * @param onRemote Flag indicating whether the area should be computed on pastEval
	 *  rather than newEval
	 * @return A numerical evaluation of the pattern
	 */
	public double apply(A suffix, B pastEval, B newEval, Context ctx, int itemsetLength, boolean onRemote) {
		B target;
		//we use the reference object to compute the hashcode
		if(onRemote) {
			target = pastEval;
		}else {
			target = newEval;
		}
		this.random.setSeed(target.hashCode() * this.initSeed);
		return this.random.nextDouble();
	}

}

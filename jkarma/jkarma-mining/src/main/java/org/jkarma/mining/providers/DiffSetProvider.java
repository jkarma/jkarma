/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.providers;

import org.jkarma.mining.joiners.DiffSet;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.windows.WindowingStrategy;
import org.jkarma.model.Transaction;


/**
 * A provider of DiffSet objects starting from transactions of type A.
 * @author Angelo Impedovo
 *
 * @param <A> Type of transactions.
 */
public class DiffSetProvider<A> extends WindowedProvider<A, DiffSet> {

	
	/**
	 * Constructs a new DiffSetProvider based on a given time window model.
	 * @param windowing The time window model.
	 */
	public DiffSetProvider(WindowingStrategy<DiffSet> windowing) {
		super(windowing);
	}

	
	
	/**
	 * Returns a new empty DiffSet.
	 */
	@Override
	public DiffSet getNewInstance() {
		return new DiffSet();
	}

	
	
	/**
	 * Consumes a single transaction updating the recent DiffSet objects associated
	 * to items from the transaction. Note that if there's an item that has
	 * never been observed before, the DiffSet is created and therefore updated.
	 */
	@Override
	public void accept(Transaction<A> transaction) {
		//we consume only not null transaction
		if(transaction!=null) {
			int tid = transaction.getId();
			
			//we register the received transactionId for each item 
			for(A item : transaction) {
				Pair<DiffSet> pair = super.apply(item);
				DiffSet diffset = pair.getIncrement();
				diffset.add(tid);
			}
			
			//we increment the transaction counter
			this.summary.consumedTransactionsCount++;
		}
	}
}

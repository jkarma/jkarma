/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

/**
 * An exception made for handling event during the control-flow
 * of transactional operations.
 * This exception should be thrown when interleaved operations
 * are not allowed (e.g. altering an object before commit or 
 * rollback of previous operations).
 * @author Angelo Impedovo
 */
@SuppressWarnings("serial")
public class TransactionException extends Exception {

}

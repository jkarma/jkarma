/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.windows;

/**
 * Class for accessing and constructing different time window models.
 * The time window models available concern two orthogonal dimensions:
 * sliding/landmark and cumulative/blockwise.
 * Note that this class is not used to build time windows, but to build
 * time window models instead.
 * A time window models express a particular aggregation logic of temporal
 * objects. In particular it is also responsible on how to forget and 
 * discard old objects in favor of new ones.
 * Every time window model is backed by a WindowingStrategy object.
 * @author Angelo Impedovo
 */
public class Windows {
	
	/**
	 * Returns a cumulative sliding window model.
	 * @param <B> Type of content from the time window.
	 * @return A cumulative sliding window model.
	 */
	public static <B> WindowingStrategy<B> cumulativeSliding(){
		return new CumulativeSlidingStrategy<B>();
	}
	
	
	
	/**
	 * Returns a cumulative landmark window model.
	 * @param <B> Type of content from the time window.
	 * @return A cumulative landmark window model.
	 */
	public static <B> WindowingStrategy<B> cumulativeLandmark(){
		return new CumulativeLandmarkStrategy<B>();
	}
	
	
	
	/**
	 * Returns a blockwise sliding window model.
	 * @param <B> Type of content from the time window.
	 * @return A blockwise sliding window model.
	 */
	public static <B> WindowingStrategy<B> blockwiseSliding(){
		return new BlockwiseSlidingStrategy<B>();
	}
	
	
	
	/**
	 * Returns a blockwise landmark window model.
	 * @param <B> Type of content from the time window.
	 * @return A blockwise landmark window model.
	 */
	public static <B> WindowingStrategy<B> blockwiseLandmark(){
		return new BlockwiseLandmarkStrategy<B>();
	}
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.windows;

import org.jkarma.mining.joiners.Aggregator;


/**
 * Abstract implementation of a windowing strategy backed by an
 * aggregator of pattern evaluations. This class implements the
 * WindowingStrategy interface without providing any additional
 * abstractions.
 * @author Angelo Impedovo
 * @param <A> Type of pattern evaluations.
 */
public abstract class AbstractWindowingStrategy<A> implements WindowingStrategy<A> {
	
	
	
	/**
	 * The pattern evaluations aggregator object to be used by the strategy.
	 */
	private Aggregator<A> dataAggregator;
	
	
	
	/**
	 * Returns the aggregator of pattern evaluations backed by the windowing strategy.
	 * @return The aggregator of pattern evaluations backed by the windowing strategy.
	 */
	@Override
	public Aggregator<A> getAggregator(){
		return this.dataAggregator;
	}
	
	
	
	/**
	 * Sets the aggregator of pattern evaluations to be used by the windowing strategy.
	 * @param aggregator The aggregator of pattern evaluations to be used by the windowing strategy.
	 */
	@Override
	public void setAggregator(Aggregator<A> aggregator) {
		this.dataAggregator = aggregator;
	}

}

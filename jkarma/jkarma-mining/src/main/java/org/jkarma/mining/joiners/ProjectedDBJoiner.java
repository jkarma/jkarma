/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

import org.jkarma.mining.providers.Context;

/**
 * Implements the joining algorithms for ProjectedDB objects.
 * In particular ProjectedDB objects are joined when refining patterns during 
 * mining and they can also be reduced.
 * The only postcondition to be checked on projected databases is that they
 * exhibits at least a minimum relative frequency.
 * @author Angelo Impedovo
 */
public class ProjectedDBJoiner<A extends Comparable<A>> implements Aggregator<ProjectedDB<A>>{
	
	/**
	 * The minimum frequency threshold to be used when evaluating the
	 * postcondition.
	 */
	private double minimumSupport;
	
	
	
	/**
	 * Constructs a projected db joiner on a given minimum relative frequency
	 * threshold.
	 * @param minimumSupport The minimum frequency threshold.
	 */
	public ProjectedDBJoiner(double minimumSupport){
		if(minimumSupport<0 || minimumSupport>1){
			throw new IllegalArgumentException();
		}
		this.minimumSupport = minimumSupport;
	}	

	

	/**
	 * Joins two projected dbs t and u associated to two patterns having same heights.
	 * The join operation computes a new projected db which is equal to the projection
	 * of t considering u.
	 * The joined projected db acts on the same number of transactions.
	 */
	@Override
	public ProjectedDB<A> apply(ProjectedDB<A> t, ProjectedDB<A> u, int height) {
		if(t==null || u==null) {
			throw new NullPointerException();
		}else {
			if(t.getTransactionCount()>0 && u.getTransactionCount()>0) {
				if(u.getTransactionCount()!=u.getTransactionCount()) {
					throw new IllegalArgumentException("trying to tidset on different number of transactions");
				}
			}
		}
		
		ProjectedDB<A> result = t.project(u.getProjectionKey());//new ProjectedDB<A>(0);
		if(t.getTransactionCount()>0 && u.getTransactionCount()>0) {
			result.transactionCount = t.transactionCount;
			result.retainAll(u);
		}
		
		return result;
	}


	
	/**
	 * Merges two projected dbs t and u in a new projected db which is equal to the
	 * set-union of t and u.
	 * The merged db acts on the sum of the transaction's number of t and u.
	 */
	@Override
	public ProjectedDB<A> reduce(ProjectedDB<A> t, ProjectedDB<A> u) {
		if(t==null || u==null) {
			throw new NullPointerException();
		}
		
		ProjectedDB<A> result = new ProjectedDB<A>(u);
		result.addAll(t); //adds u.transactionCount also
		
		return result;
	}

	
	
	/**
	 * Always returns true since no pre-conditions on projected dbs are evaluated. 
	 */
	@Override
	public boolean testPrecondition(ProjectedDB<A> first, ProjectedDB<A> second, Context ctx, int height) {
		return true;
	}

	
	
	/**
	 * Returns true if the relative frequency exceeds the minimum frequency threshold,
	 * false otherwise.
	 */
	@Override
	public boolean testPostcondition(ProjectedDB<A> db, Context ctx, int height) {
		boolean isFrequent = db.getRelativeFrequency() >= this.minimumSupport;
		return isFrequent;
	}



	/**
	 * Returns the minimum frequency threshold currently associated to the joiner.
	 * @return The minimum frequency threshold currently associated to the joiner.
	 */
	public double getMinimumFrequency() {
		return this.minimumSupport;
	}



	/**
	 * Associates a minimum frequency threshold currently to the joiner.
	 * @param minFrequency The minimum frequency threshold.
	 */
	public void setMinimumFrequency(float minFrequency) {
		if(minFrequency<0 || minFrequency>1) {
			throw new IllegalArgumentException();
		}
		this.minimumSupport = minFrequency;
	}
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 * Class implementing a pattern frequency evaluation through DiffSet as in
 * the dEclat algorithm by Zaki et. al
 * 
 * @author Angelo Impedovo
 * @see <a href="https://doi.org/10.1145/956750.956788">Proceedings of the Ninth 
 * ACM SIGKDD International Conference on Knowledge Discovery and Data Mining, 2003, 
 * Fast vertical mining using diffsets</a>
 */
@SuppressWarnings("serial")
public class DiffSet extends TreeSet<Integer> implements FrequencyEvaluation {
	
	/**
	 * It is the absolute frequency associated to the parent
	 * diffset PX of this diffset PXY.
	 * This value is necessary to compute the absolute frequency.
	 */
	protected int pFreq;
	
	/**
	 * The number of transaction monitored by this diffset.
	 * It is the relative frequency's denominator.
	 */
	protected int tCount;

	
	/**
	 * Creates a new empty DiffSet monitoring a certain number of transaction.
	 * The DiffSet created is equivalent to an empty TidSet.
	 * 
	 * @param tCount the number of transaction to be monitored
	 * @param pFreq the frequency of parent diffset
	 * @param elems the elements to be added in the diffset
	 */
	public DiffSet(int tCount, int pFreq, Set<Integer> elems) {
		if(tCount<0 || pFreq<0) {
			throw new IllegalArgumentException(); 
		}
		if(elems==null || elems.size()>tCount) {
			throw new IllegalArgumentException();
		}
		this.tCount = tCount;
		this.pFreq = pFreq;
		this.addAll(elems);
	}
	
	
	
	/**
	 * Creates a new empty DiffSet monitoring a certain number of transaction.
	 * The DiffSet created is equivalent to an empty TidSet with a parent frequency
	 * equals to 0.
	 * @param tCount the number of transaction to be monitored
	 * @param pFreq the frequency of parent diffset
	 */
	public DiffSet(int tCount, int pFreq) {
		this(tCount,pFreq,Collections.emptySet());
	}
	
	
	
	/**
	 * Creates a new empty DiffSet monitoring zero transactions.
	 * The DiffSet created is equivalent to an empty TidSet with a parent frequency
	 * equals to 0.
	 */
	public DiffSet() {
		this(0,0,Collections.emptySet());
	}
	
	
	
	/**
	 * Computes the absolute frequency starting from the informative
	 * content of the DiffSet. 
	 */
	@Override
	public int getAbsoluteFrequency() {
		int result = 0;
		if(this.pFreq==0) {
			result = this.size();
		}else {
			result = this.pFreq - this.size();
		}
		return result;
	}
	
	
	
	/**
	 * Computes the relative frequency starting from the informative
	 * content of the DiffSet.
	 */
	@Override
	public double getRelativeFrequency() {
		double result = 0;
		if(this.getTransactionCount()>0) {
			result = (double)this.getAbsoluteFrequency()/(double)this.getTransactionCount();
		}
		
		return result;
	}

	
	
	/**
	 * Returns weather the relative frequency associated to the DiffSet
	 * exceeds a given minimum threshold.
	 * 
	 * @param minSup the minimum threshold
	 * @return true if the relative frequency exceeds minSup, false otherwise
	 */
	@Override
	public boolean isFrequent(double minSup) {
		boolean result = this.getRelativeFrequency() >= minSup;
		return result;
	}

	

	/**
	 * Returns the number of transactions monitored by the DiffSet.
	 * The number of transactions is the denominator of the relative frequency.
	 * 
	 * @return the number of transactions monitored by the DiffSet.
	 */
	@Override
	public int getTransactionCount() {
		return this.tCount;
	}

	
	
	/**
	 * Sets the number of transactions monitored by the DiffSet.
	 * The number of transactions is the denominator of the relative freuqency,
	 * therefore it should never be explicitly set by the user.
	 * 
	 * @param t the number of transactions monitored by the DiffSet.
	 */
	@Override
	public void setTransactionCount(int t) {
		if(t<0) {
			throw new IllegalArgumentException();
		}
		this.tCount = t;
	}
	
	
	/**
	 * Returns a String representation of the DiffSet
	 * 
	 * @return a DiffSet representation of the DiffSet
	 */
	@Override
	public String toString() {
		return super.toString()+"#"+this.getAbsoluteFrequency()+"/"+this.tCount+" ("+this.getRelativeFrequency()+")";
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

import org.jkarma.mining.providers.Context;

/**
 * Defines the public interface of a Joiner delegate which is able
 * to join objects, of the same type, given a set of preconditions 
 * and postconditions.
 * The interface extends the Mapper interface with two methods which
 * are able to evaluate some preconditions and postconditions to be
 * considered before and after the map phase (realized by the Mapper
 * interface).
 * To the pattern mining purposes, the Joiner interface is the base
 * interface for implementing fully working linguistic constraints and
 * evaluations on patterns.
 * @author Angelo Impedovo
 *
 * @param <A> The type of elements to be joined.
 */
public interface Joiner<A> extends Mapper<A,A> {
	
	
	/**
	 * Checks if two objects sharing the same height and mining context
	 * can be joined, according to a given precondition.
	 * @param first The first object to be joined.
	 * @param second The second object to be joined.
	 * @param ctx The mining context.
	 * @param height The height shared by the two objects to be joined.
	 * @return True if the two objects satisfy the precondition, false otherwise.
	 */
	public boolean testPrecondition(A first, A second, Context ctx, int height);
	
	
	
	/**
	 * Checks if the result of the join operation, between two objects at height-1
	 * and sharing the same mining context, satisfies a given postcondition.
	 * @param result The joined object.
	 * @param ctx The mining context.
	 * @param height The height of the joined object.
	 * @return True if the joined object satisfy the postcondition, false otherwise.
	 */
	public boolean testPostcondition(A result, Context ctx, int height);
}

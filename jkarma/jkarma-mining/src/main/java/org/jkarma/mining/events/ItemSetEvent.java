/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.events;

import org.jkarma.mining.interfaces.ItemSet;

/**
 * Abstract implementation of a event associated to itemset related operations
 * during their mining by the Strategy components.
 * The event encapsulate the associated itemset.
 * @author Angelo Impedovo
 *
 * @param <A> Type expressing the alphabet of allowed itemset symbols.
 * @param <B> Type expressing the evaluation of itemsets.
 */
public abstract class ItemSetEvent<A,B> {
	
	private ItemSet<A,B> itemset;
	
	
	/**
	 * Constructs a generic ItemSetEvent associated to a specific itemset.
	 * @param itemset The itemset associated to this event.
	 */
	public ItemSetEvent(ItemSet<A, B> itemset) {
		this.itemset = itemset;
	}

	
	/**
	 * Returns the itemset associated to the event.
	 * @return The itemset associated to the event.
	 */
	public ItemSet<A,B> getItemSet(){
		return this.itemset;
	}

}

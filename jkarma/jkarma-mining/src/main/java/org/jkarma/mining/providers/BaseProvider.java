/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.providers;

import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.function.Consumer;

import org.jkarma.mining.interfaces.Provider;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.windows.WindowingStrategy;
import org.jkarma.model.Transaction;

/**
 * Abstract class defining an abstract based data provider which acts as a mapping
 * from the domain of the items to the domain of the evaluation.
 * The BaseProvider create elements without following a specific windowing
 * scheme, it only uses the aggregator of a windowing strategy in order
 * to continuously aggregating elements.
 * The class is a Consumer of Transactions, but the logic is not implemented as
 * this clearly depends on the type of evaluations to be provided.
 * @author Angelo Impedovo
 *
 * @param <A> The type of items to be mapped,
 * @param <B> The type of evaluations to be provided.
 */
public abstract class BaseProvider<A,B> implements Provider<A,B>, Consumer<Transaction<A>>{
	
	
	
	/**
	 * The mining context provided by the summary.
	 */
	protected Context summary;
	
	
	
	/**
	 * The windowing strategy of this provider.
	 * Evaluations are provided and cached according to the semantics of a certain
	 * time window models.
	 */
	protected WindowingStrategy<B> windowing;
	
	
	
	/**
	 * The content of this provider.
	 * Stores an index of the elements mapped with respect to symbols, in particular
	 * the mapped content is a pair of evaluations: the first is the cached evaluation
	 * of previously consumed transactions, the second is the the stored evaluation of
	 * recently consumed transactions.
	 */
	protected TreeMap<A, Pair<B>> cache;
	
	
	
	/**
	 * Builds an empty BaseProvider given a time window model.
	 * @param windowing The time window model.
	 */
	public BaseProvider(WindowingStrategy<B> windowing) {
		if(windowing==null) {
			throw new NullPointerException();
		}
		this.summary = new Context();
		this.cache = new TreeMap<>();
		this.windowing = windowing;
	}
	
	
	
	/**
	 * Returns the cached evaluation for the specified item built according
	 * to the previously consumed transactions.
	 * If the item has not been observed before, an empty evaluation (not null)
	 * is returned.
	 * @param key The item to be searched.
	 * @return The cached evaluation associated to item.
	 */
	public B getPastByKey(A key) {
		Pair<B> content = this.apply(key);
		return content.getAggregate();
	}
	
	
	
	/**
	 * Returns the recent evaluation for the specified item built according
	 * to the recently consumed transactions.
	 * If the item has not been observed before, an empty evaluation (not null)
	 * is returned.
	 * @param key The item to be searched
	 * @return The recent evaluation associated to item.
	 */
	public B getByKey(A key) {
		Pair<B> content = this.apply(key);
		return content.getIncrement();
	}
	
	
	
	/**
	 * Returns the mining context associated with this provider.
	 * @return The mining context associated with this provider.
	 */
	public Context getContext() {
		return this.summary;
	}
	
	
	
	/**
	 * Returns the pair made of cached and recent evaluations associated to a given
	 * item.
	 */
	@Override
	public Pair<B> apply(A key){
		Pair<B> result = this.cache.get(key);
		if(result==null) {
			result = Pair.of(this.getNewInstance(), this.getNewInstance());
			this.cache.put(key, result); //side effecting?
		}
		return result;
	}
	
	
	
	/**
	 * Drops every cached evaluation, this method also changes the state
	 * of the mining context by setting the count of cached transactions to 0.
	 */
	@Override
	public void forgetCached() {
		for(Entry<A, Pair<B>> entry : this.cache.entrySet()) {
			Pair<B> providedMemory = entry.getValue();
			if(providedMemory!=null) {
				providedMemory.setAggregate(this.getNewInstance());
			}
		}
		
		//we update the context accordingly
		this.summary.cachedTransactionsCount = 0;
	}
	
	
	
	/**
	 * Drops every recent evaluation, this method also changes the state
	 * of the mining context by setting the count of consumed transactions to 0.
	 */
	@Override
	public void forgetRecent() {
		for(Entry<A, Pair<B>> entry : this.cache.entrySet()) {
			Pair<B> providedMemory = entry.getValue();
			if(providedMemory!=null) {
				providedMemory.setIncrement(this.getNewInstance());
			}
		}
		
		//we update the context accordingly
		this.summary.consumedTransactionsCount = 0;
	}
	
	
	
	/**
	 * Returns the time window model associated with this provider.
	 */
	@Override
	public WindowingStrategy<B> getWindowModel() {
		return this.windowing;
	}
	
}

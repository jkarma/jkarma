/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

/**
 * Convenience class representing a pair of objects of the same type.
 * @author Angelo Impedovo
 *
 * @param <B> The type of the objects.
 */
public class Pair<B>{
	
	/**
	 * Static method for constructing pair of objects.
	 * @param <C> The type of the objects.
	 * @param first The first element of the pair.
	 * @param second The second element of the pair
	 * @return The pair made of the two objects.
	 */
	public static <C> Pair<C> of(C first, C second){
		return new Pair<C>(first, second);
	}
	
	
	/**
	 * The first element of the pair.
	 */
	private B first;
	
	
	/**
	 * The second element of the pair.
	 */
	private B second;
	
	
	
	/**
	 * Constructs a pair of two objects.
	 * @param first The first element of the pair.
	 * @param second The second element of the pair.
	 */
	protected Pair(B first, B second) {
		if(first==null || second==null) {
			throw new NullPointerException();
		}
		this.first = first;
		this.second = second;
	}
	
	
	
	/**
	 * Returns the first element of the pair.
	 * @return The first element of the pair.
	 */
	public B getAggregate() {
		return this.first;
	}
	
	
	
	/**
	 * Sets the first element of the pair.
	 * @param aggregate The element to be set.
	 */
	public void setAggregate(B aggregate) {
		this.first = aggregate;
	}
	
	
	
	/**
	 * Returns the second element of the pair.
	 * @return The second element of the pair.
	 */
	public B getIncrement() {
		return this.second;
	}
	
	
	
	/**
	 * Sets the second element of the pair.
	 * @param aggregate The element to be set.
	 */
	public void setIncrement(B aggregate) {
		this.second = aggregate;
	}
	
	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		if(this.first==null) {
			builder.append("n/a");
		}else {
			builder.append(first.toString());
		}
		builder.append(" -> ");
		if(this.second==null) {
			builder.append("n/a");
		}else {
			builder.append(second.toString());
		}
		builder.append("]");
		return builder.toString();
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if(obj instanceof Pair<?>) {
			Pair<?> pair = (Pair<?>)obj;
			result = (this.first.equals(pair.first) && this.second.equals(pair.second));
		}else {
			throw new ClassCastException();
		}
		return result;
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import java.util.Collections;
import java.util.Set;

import org.jkarma.mining.joiners.Aggregator;
import org.jkarma.mining.joiners.DiffSet;
import org.jkarma.mining.joiners.DiffSetJoiner;
import org.jkarma.mining.joiners.Joiner;
import org.jkarma.mining.joiners.PeriodSet;
import org.jkarma.mining.joiners.PeriodSetJoiner;
import org.jkarma.mining.joiners.ProjectedDB;
import org.jkarma.mining.joiners.ProjectedDBJoiner;
import org.jkarma.mining.joiners.TidSet;
import org.jkarma.mining.joiners.TidSetJoiner;

/**
 * Builder delegate used to build a fully functional mining strategy.
 * The proxy encapsulates any detail about the symbolic strategy.
 * @author Angelo Impedovo
 *
 * @param <A> Type of allowed pattern symbols.
 */
public class SymbolicStrategyProxy<A extends Comparable<A>> {
	
	
	
	/**
	 * The maximum length of patterns to be discovered by the mining strategy.
	 */
	protected int maxDepth;
	
	
	
	/**
	 * The items to be considered in the pattern language.
	 */
	protected Set<A> items;
	
	
	
	/**
	 * The lexicographic joiner implementing the pattern language grammar.
	 */
	protected Joiner<A> grammar;
	
	
	
	/**
	 * Builds a builder delegate based on a particular lexicographic joiner.
	 * @param joiner The lexicographic joiner.
	 */
	protected SymbolicStrategyProxy(Joiner<A> joiner) {
		if(joiner==null) {
			throw new IllegalArgumentException();
		}
		this.grammar = joiner;
		this.items = Collections.emptySet();
		this.maxDepth = 3;
	}
	
	
	
	/**
	 * Decorates the proxy with the items to be considered in the 
	 * mining strategy.
	 * @param items The allowed items.
	 * @return The decorated SymbolicStrategyProxy.
	 */
	public SymbolicStrategyProxy<A> symbols(Set<A> items){
		this.items = items;
		return this;
	}
	
	
	
	/**
	 * Decorates the proxy with the	maximum length of patterns to be discovered 
	 * by the mining strategy.
	 * @param maxDepth The maximum pattern lengths. 
	 * @return The decorated SymbolicStrategyProxy.
	 */
	public SymbolicStrategyProxy<A> limitDepth(int maxDepth) {
		this.maxDepth = maxDepth;
		return this;
	}
	
	
	/**
	 * Decorates the proxy with a custom pattern evaluator for discovering
	 * patterns according to a given pattern language.
	 * This methods is useful when none of the provided algorithm is 
	 * relevant for the user.
	 * @param <B> The type implementing the pattern quality criterion.
	 * @param aggregator The delegate implementing the pattern quality criterion.
	 * @return The decorated MiningStrategyProxy.
	 */
	public <B> MiningStrategyProxy<A,B> byEvaluating(Aggregator<B> aggregator){
		Selector<A,B> selector = new DFSSelector<A,B>();
		return new MiningStrategyProxy<A,B>(this, selector, aggregator);
	}
	
	
	
	/**
	 * Decorates the proxy with the Eclat mining logic for discovering frequent
	 * patterns according to a given pattern language.
	 * The Eclat algorithm computes the support of patterns by intersecting
	 * tidsets.
	 * @param minimumSupport The minimum relative frequency of allowed patterns.
	 * @return The decorated MiningStrategyProxy.
	 */
	public MiningStrategyProxy<A,TidSet> eclat(float minimumSupport){
		Selector<A,TidSet> selector = new DFSSelector<A,TidSet>();
		Aggregator<TidSet> aggregator = new TidSetJoiner(minimumSupport);
		return new MiningStrategyProxy<A,TidSet>(this, selector, aggregator);
	}
	
	
	
	/**
	 * Decorates the proxy with the Eclat mining logic for discovering frequent
	 * patterns according to a given pattern language.
	 * This alternative version of the Eclat algorithm computes the support 
	 * of patterns by intersecting tidsets in a ProjectedDB data structure.
	 * @param minimumSupport The minimum relative frequency of allowed patterns.
	 * @return The decorated MiningStrategyProxy.
	 */
	public MiningStrategyProxy<A,ProjectedDB<A>> projectedEclat(float minimumSupport){
		Selector<A,ProjectedDB<A>> selector = new DFSSelector<A,ProjectedDB<A>>();
		Aggregator<ProjectedDB<A>> aggregator = new ProjectedDBJoiner<A>(minimumSupport);
		return new MiningStrategyProxy<A,ProjectedDB<A>>(this, selector, aggregator);
	}
	
	
	
	/**
	 * Decorates the proxy with the dEclat mining logic for discovering frequent
	 * patterns according to a given pattern language.
	 * The dEclat algorithm computes the support of patterns by adopring
	 * diffsets.
	 * @param minimumSupport The minimum relative frequency of allowed patterns.
	 * @return The decorated MiningStrategyProxy.
	 */
	public MiningStrategyProxy<A,DiffSet> diffEclat(float minimumSupport){
		Selector<A,DiffSet> selector = new DFSSelector<A,DiffSet>();
		Aggregator<DiffSet> aggregator = new DiffSetJoiner(minimumSupport);
		return new MiningStrategyProxy<A,DiffSet>(this, selector, aggregator);
	}
	
	
	
	/**
	 * Decorates the proxy with the PFPM mining logic for discovering periodic
	 * patterns according to a given pattern language.
	 * The PFPM algorithm computes the minimum, maximum and average
	 * periodicity of patterns.
	 * @param minPer The minimum periodicity of patterns.
	 * @param maxPer The maximum periodicity of patterns.
	 * @param minAvgPer The lower bound for the average periodicity of patterns.
	 * @param maxAvgPer The upper bound for the average periodicity of patterns.
	 * @return The decorated MiningStrategyProxy.
	 */
	public MiningStrategyProxy<A,PeriodSet> pfpm(int minPer, int maxPer, double minAvgPer, double maxAvgPer){
		Selector<A,PeriodSet> selector = new DFSSelector<A,PeriodSet>();
		Aggregator<PeriodSet> aggregator = new PeriodSetJoiner(minPer, maxPer, minAvgPer, maxAvgPer);
		return new MiningStrategyProxy<A,PeriodSet>(this, selector, aggregator);
	}
	
	
	
	/**
	 * Decorates the proxy with the LCM mining logic for discovering frequent
	 * closed patterns according to a given pattern language.
	 * The LCM algorithm computes the support of patterns and their closure
	 * by inspecting the content of a ProjectedDB data structures.
	 * @param minimumSupport The minimum relative frequency of allowed patterns.
	 * @return The decorated MiningStrategyProxy.
	 */
	public MiningStrategyProxy<A,ProjectedDB<A>> lcm(float minimumSupport){
		Selector<A,ProjectedDB<A>> selector = new DFSPpcClosedSelector<A,ProjectedDB<A>>();
		Aggregator<ProjectedDB<A>> aggregator = new ProjectedDBJoiner<A>(minimumSupport);
		return new MiningStrategyProxy<A,ProjectedDB<A>>(this, selector, aggregator);
	}
	
	
	
	/**
	 * Decorates the proxy with the LCM-max mining logic for discovering frequent
	 * maximal patterns according to a given pattern language.
	 * The LCM-max algorithm computes the support of patterns and their maximality
	 * by inspecting the content of a ProjectedDB data structures.
	 * @param minimumSupport The minimum relative frequency of allowed patterns.
	 * @return The decorated MiningStrategyProxy.
	 */
	public MiningStrategyProxy<A,ProjectedDB<A>> lcmMax(float minimumSupport) {
		Selector<A,ProjectedDB<A>> selector = new DFSPpcMaxSelector<A,ProjectedDB<A>>(minimumSupport);
		Aggregator<ProjectedDB<A>> aggregator = new ProjectedDBJoiner<A>(minimumSupport);
		return new MiningStrategyProxy<A,ProjectedDB<A>>(this, selector, aggregator);
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import org.jkarma.mining.heuristics.Heuristic;
import org.jkarma.mining.joiners.Aggregator;
import org.jkarma.mining.providers.BaseProvider;

/**
 * Builder delegate used to build a fully functional mining strategy
 * from a SymbolicStrategyProxy.
 * The MiningStrategyProxy injects the traverse strategy of the search
 * space of patterns (the lattice).
 * @author Angelo Impedovo
 *
 * @param <A> Type of allowed pattern symbols.
 * @param <B> Type of pattern evaluation.
 */
public class MiningStrategyProxy<A extends Comparable<A>,B> {
	
	/**
	 * The SymbolicStrategyProxy to decorate with the traverse strategy.
	 */
	private SymbolicStrategyProxy<A> language;
	
	/**
	 * The pattern evaluator.
	 */
	private Aggregator<B> evaluator;
	
	/**
	 * The selector delegate.
	 */
	private Selector<A,B> selector;
	
	
	/**
	 * Builds a new MiningStrategyProcy based on a particular traverse strategy.
	 * @param symbolicStrategy
	 * @param selector
	 * @param evaluator
	 */
	
	protected MiningStrategyProxy(SymbolicStrategyProxy<A> symbolicStrategy, Selector<A,B> selector, Aggregator<B> evaluator) {
		if(symbolicStrategy==null || evaluator==null || selector==null) {
			throw new IllegalArgumentException();
		}
		this.language = symbolicStrategy;
		this.evaluator = evaluator;
		this.selector = selector;
	}
	
	
	
	/**
	 * Finalizes an exhaustive DFS MiningStrategy by passing a data accessor.
	 * @param dataSource The data accessor.
	 * @return An exhaustive DFS MiningStrategy. 
	 */
	public MiningStrategy<A,B> dfs(BaseProvider<A,B> dataSource){
		return new DFSUnifiedStrategy<A,B>(language.items, language.grammar, dataSource, evaluator, selector, language.maxDepth);
	}
	
	
	
	/**
	 * Finalizes a beam-search DFS MiningStrategy by passing the heuristic
	 * criteria and the data source.
	 * @param dataSource The data accessor.
	 * @param heuristic The heuristic criteria to be used in the beam-search.
	 * @param k The beam size.
	 * @return A non-exhaustive DFS MiningStrategy based on beam-search.
	 */
	public MiningStrategy<A,B> beam(BaseProvider<A,B> dataSource, Heuristic<A,B> heuristic, int k){
		return new DFSUnifiedStrategy<A,B>(language.items, language.grammar, dataSource, evaluator, selector, heuristic, k, language.maxDepth);
	}

}

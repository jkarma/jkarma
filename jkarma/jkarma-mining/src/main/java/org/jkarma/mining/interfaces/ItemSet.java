/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.interfaces;

import java.util.Map.Entry;

/**
 * Defines the public interface for representing itemsets.
 * An itemset is seen as a word of items, of type A, with an
 * evaluation, of type B.
 * Since every itemset is a word, it makes sense to define the
 * length as the count the items in the itemset. Furthermore, 
 * an itemset can be also seen as made of an itemset prefix which 
 * have been appended to a suffix item. 
 * @author Angelo Impedovo
 * @param <A> Type of allowed items.
 * @param <B> Type of the evaluation.
 */
public interface ItemSet<A,B> extends Entry<A,B>{

	/**
	 * Returns the length of the itemset (length of the prefix itemset plus 1)-
	 * @return The length of the itemset
	 */
	public int getLength();
	
	
	
	/**
	 * Returns the prefix itemset of this itemset.
	 * @return The prefix itemset of this itemset.
	 */
	public ItemSet<A, B> getPrefix();
	
	
	/**
	 * Returns the suffix item appended to this itemset.
	 * @return The suffix item appended to this itemset.
	 */
	public A getSuffix();
	
	
	
	/**
	 * Returns the evaluation of this itemset (the evaluation associated to the suffix item).
	 * @return The evaluation of this itemset.
	 */
	public B getEval();
	
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import java.util.EnumSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.MutablePair;
import org.jkarma.mining.interfaces.ItemSet;
import org.jkarma.mining.interfaces.Node;

/**
 * Class for constructing and manipulating set enumeraion tree nodes.
 * A SENode is a wrapper of an informative content stored in a SETree.
 * The class implements both the ItemSet and the Node interfaces. As a Node, a
 * SENode is also a pointer to a parent SENode and allows to keep references to
 * child nodes (via a SENodeList object). As an ItemSet, a SENode is also a pattern
 * made of the items contained in the nodes belonging to the root-to-node path in
 * the SETree.
 * The informative content of a SENode is a symbol (of type A) and a node evaluation
 * (of type B), as also specified in the SETree class.
 * Since SENodes are added and removed from the SETree by a commit/rollback call,
 * each node is marked with a status (e.g. marked for add, updated, marked for delete).
 * Also a SENode stores two convenience children lists. 
 * @author Angelo Impedovo
 * 
 * @param <A> The type of symbols allowed in nodes.
 * @param <B> The type of node evaluations.
 */
class SENode<A,B> implements Node<Entry<A,B>>, ItemSet<A,B>{
	
	protected ItemSetProxy data;
	protected SENode<A,B> parent;
	protected SENodeList<A,B> children;
	protected SENodeList<A,B> discardedChildren;
	private EnumSet<SENodeStatus> status;
	
	
	/**
	 * Constructs an empty node.
	 */
	protected SENode() {
		this(MutablePair.of(null, null), EnumSet.of(SENodeStatus.UPDATED));
	}
    
	
	
	/**
	 * Constructs a SENode according to a pair symbol-evaluation with a given
	 * status.
	 * @param <C> Type of symbol-evaluation pair.
	 * @param entry The symbol-evaluation pair to be associated to the node.
	 * @param status The initial status of the node.
	 */
    protected <C extends Entry<A,B>> SENode(C entry, EnumSet<SENodeStatus> status){
    	this.data = new ItemSetProxy(entry);
    	this.parent = null;
    	this.children = new SENodeList<>();
    	this.discardedChildren = new SENodeList<>();
    	this.status = status;
    }
    
    
    
    /**
     * Returns the status of the node.
     * @return
     */
    public EnumSet<SENodeStatus> getStatus() {
    	return this.status;
    }
    
    
    
    /**
     * Returns true if the node is marked as deleted, false otherwise.
     * @return True if the node is marked as deleted, false otherwise.
     */
    public boolean isMarkedAsRemoved() {
    	return this.status.contains(SENodeStatus.DELETED);
    }
    
    
    
    /**
     * Returns true if the node is marked as added, false otherwise.
     * @return True if the node is marked as added, false otherwise.
     */
    public boolean isMarkedAsAdded() {
    	return this.status.contains(SENodeStatus.ADDED);
    }
    
    
    
    /**
     * Returns true if the node is ordinary, false otherwise.
     */
    public boolean isOrdinaryNode() {
    	return !this.status.contains(SENodeStatus.DISCARDED);
    }
    
    
    public void setOrdinary(boolean isOrdinary) {
    	if(isOrdinary) {
    		this.status.remove(SENodeStatus.DISCARDED);
    	}else {
    		this.status.add(SENodeStatus.DISCARDED);
    	}
    	
    }
    
   
    public void setUpdated() {
    	this.status.remove(SENodeStatus.ADDED);
    	this.status.remove(SENodeStatus.DELETED);
    	this.status.add(SENodeStatus.UPDATED);
    }
    
    
    public void setMarkedAsAdded() {
    	this.status.remove(SENodeStatus.UPDATED);
    	this.status.remove(SENodeStatus.DELETED);
    	this.status.add(SENodeStatus.ADDED);
    }
    
    public void setMarkedAsRemoved() {
    	this.status.remove(SENodeStatus.UPDATED);
    	this.status.remove(SENodeStatus.ADDED);
    	this.status.add(SENodeStatus.DELETED);
    }
	
	
    /**
     * Returns the symbol associated to the node.
     */
	@Override
	public A getKey() {
		return this.data.suffix;
	}


	
	/**
	 * Returns the evaluation associated to the node.
	 */
	@Override
	public B getValue() {
		return this.data.eval;
	}


	
	/**
	 * Sets the evaluation of the node.
	 */
	@Override
	public B setValue(B value) {
		return this.data.setValue(value);
	}


	
	@Override
	public int getLength() {
		return this.data.getLength();
	}


	@Override
	public ItemSet<A, B> getPrefix() {
		return this.data.getPrefix();
	}


	@Override
	public A getSuffix() {
		return this.data.suffix;
	}


	@Override
	public B getEval() {
		return this.data.eval;
	}


	
	/**
	 * Register a given node as child of this node.
	 * The node is registered in the primary children list.
	 * @param node The node to be registered.
	 */
	public void addChild(SENode<A,B> node) {
		if(node==null) {
			throw new NullPointerException();
		}
		if(node.getParent()!=null) {
			throw new IllegalArgumentException();
		}
		
		this.removeChild(node);
		this.children.add(node);
		node.parent = this;
	}
	
	
	
	/**
	 * Register a given node as child of this node.
	 * The node is registered in the secondary children list.
	 * @param node The node to be registered.
	 */
	public void addDiscardedChild(SENode<A, B> node){
		if(node==null) {
			throw new NullPointerException();
		}
		if(node.getParent()!=null) {
			throw new IllegalArgumentException();
		}
		
		this.removeChild(node);
		this.discardedChildren.add(node);
		node.parent = this;
	}


	
	
	/**
	 * Removes a node as child of this node.
	 * The reference is removed from the primary children list.
	 * @param node The node to be removed.
	 */
	public void removeChild(Node<Entry<A,B>> content) {
		if(content==null) {
			throw new NullPointerException();
		}
		
		A key = content.getData().getKey();
		SENode<A,B> child = this.children.nodes.get(key);
		if(child!=null) {
			this.children.remove(child);
			child.parent = null;
		}
	}
	
	
	
	/**
	 * Removes a node as child of this node.
	 * The reference is removed from the secondary children list.
	 * @param node The node to be removed.
	 */
	public void removeDiscardedChild(SENode<A, B> content) {
		if(content==null) {
			throw new NullPointerException();
		}
		
		A key = content.getKey();
		SENode<A,B> child = this.discardedChildren.nodes.get(key);
		if(child!=null) {
			this.discardedChildren.remove(child);
			child.parent = null;
		}
	}

    
    

	@Override
	public SENode<A,B> getRoot() {
		return this;
	}

	@Override
	public ItemSet<A,B> getData() {
		return this.data;
	}

	@Override
	public SENode<A,B> getParent() {
		return this.parent;
	}

	@Override
	public SENode<A,B> getNextSibling() {
		SENode<A,B> sibling = null;
		if(this.parent!=null) {
			sibling = this.parent.children.getNextSibling(this);
		}
		return sibling;
	}

	@Override
	public SENode<A,B> getPreviousSibling() {
		SENode<A,B> sibling = null;
		if(this.parent!=null) {
			sibling = this.parent.children.getPreviousSibling(this);
		}
		return sibling;
	}

	@Override
	public SENode<A,B> getFirstChild() {
		return this.children.getFirstChild();
	}

	@Override
	public SENode<A,B> getLastChild() {
		return this.children.getLastChild();
	}

	@Override
	public SENodeList<A,B> getChildren() {
		return this.children;
	}
	
	@Override
	public int getDescendantsCount() {
		int count = this.children.count();
		for(SENode<A,B> child : this.children) {
			count += child.getDescendantsCount();
		}
		return count;
	}
	
	@Override
	public String toString() {
		LinkedList<String> list = new LinkedList<>();
		SENode<A,B> currentNode = this;
		while(currentNode!=null){
			if(currentNode.getParent()!=null){
				list.addFirst(currentNode.getData().getKey().toString());
			}else{
				break;
			}
			currentNode = currentNode.getParent();
		}
		
		return "{"+String.join(",", list)+"}";
	}
	
	
	
	class ItemSetProxy implements ItemSet<A,B>{
		
		protected A suffix;
		protected B eval;
		
		protected <C extends Entry<A,B>> ItemSetProxy(C original) {
			this.suffix = original.getKey();
			this.eval = original.getValue();
		}
		
		protected ItemSetProxy() {
			this.suffix = null;
			this.eval = null;
		}
		
		public SENode<A,B> getNode(){
			return SENode.this;
		}

		@Override
		public int getLength() {
			int result = 0;
			SENode<A,B> currentNode = parent;
			while(currentNode!=null){
				currentNode = currentNode.parent;
				result++;
			}
			return result;
		}

		@Override
		public ItemSet<A, B> getPrefix() {
			ItemSet<A,B> result = null;
			if(parent!=null) {
				result = parent.getData();
			}
			return result;
		}

		@Override
		public A getSuffix() {
			return this.suffix;
		}

		@Override
		public B getEval() {
			return this.eval;
		}

		@Override
		public A getKey() {
			return this.suffix;
		}

		@Override
		public B getValue() {
			return this.eval;
		}

		@Override
		public B setValue(B value) {
			B result = this.eval;
			this.eval = value;
			return result;
		}
		
		@Override
		public String toString() {
			return SENode.this.toString();
		}
		
	}


	
	/**
	 * Returns the stream-view of the rooted-subtree in this node.
	 * @return The stream-view of the rooted-subtree in this node.
	 */
	public Stream<ItemSet<A, B>> stream() {
		return Stream.concat(Stream.of(this), this.children.nodes.values().stream().flatMap(SENode::stream));
	}
		
}

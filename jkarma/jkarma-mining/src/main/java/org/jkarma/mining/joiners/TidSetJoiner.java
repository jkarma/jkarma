/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

import org.jkarma.mining.providers.Context;


/**
 * Implements the joining algorithms for tidsets.
 * In particular tidsets are joined when refining patterns during mining
 * and they can also be reduced.
 * The only postcondition to be checked on tidsets is that they exhibits
 * at least a minimum relative frequency.
 * @author Angelo Impedovo
 */
public class TidSetJoiner implements Aggregator<TidSet>{
	
	/**
	 * The minimum frequency threshold to be used when evaluating the
	 * postcondition.
	 */
	private double minimumSupport;
	
	
	
	/**
	 * Constructs a tidset joiner on a given minimum relative frequency
	 * threshold.
	 * @param minimumSupport The minimum frequency threshold.
	 */
	public TidSetJoiner(double minimumSupport){
		if(minimumSupport<0 || minimumSupport>1){
			throw new IllegalArgumentException();
		}
		this.minimumSupport = minimumSupport;
	}	

	
	
	/**
	 * Joins two tidsets t and u associated to two patterns having same heights.
	 * The join operation computes a new tidset which is equal to the set-intersection
	 * of t and u.
	 * The joined tidset acts on the same number of transactions.
	 */
	@Override
	public TidSet apply(TidSet t, TidSet u, int height) {
		if(t==null || u==null) {
			throw new NullPointerException();
		}else {
			if(t.transactionCount>0 && u.transactionCount>0) {
				if(u.getTransactionCount()!=u.getTransactionCount()) {
					throw new IllegalArgumentException("joining tidset with different number of transactions");
				}
			}
		}
		
		TidSet result = new TidSet(0);
		if(t.transactionCount>0 && u.transactionCount>0) {
			result.transactionCount = t.transactionCount;
			result.addAll(t);
			result.retainAll(u);
		}
		return result;
	}


	
	/**
	 * Merges two tidsets t and u in a new tidset which is equal to the
	 * set-union of t and u.
	 * The merged tidset acts on the sum of the transaction's number of t and u.
	 */
	@Override
	public TidSet reduce(TidSet t, TidSet u) {
		if(t==null || u==null) {
			throw new NullPointerException();
		}
		
		TidSet result = new TidSet(t.transactionCount+u.transactionCount);
		result.addAll(t);
		result.addAll(u);
		return result;
	}

	
	
	/**
	 * Always returns true since no pre-conditions on tidsets are evaluated. 
	 */
	@Override
	public boolean testPrecondition(TidSet first, TidSet second, Context ctx, int height) {
		return true;
	}

	
	
	/**
	 * Returns true if the relative frequency exceeds the minimum frequency threshold,
	 * false otherwise.
	 */
	@Override
	public boolean testPostcondition(TidSet tidlist, Context ctx, int height) {
		double support = tidlist.getRelativeFrequency();
		return support >= this.minimumSupport;
	}



	/**
	 * Returns the minimum frequency threshold currently associated to the joiner.
	 * @return The minimum frequency threshold currently associated to the joiner.
	 */
	public double getMinimumFrequency() {
		return this.minimumSupport;
	}



	/**
	 * Associates a minimum frequency threshold currently to the joiner.
	 * @param minFrequency The minimum frequency threshold.
	 */
	public void setMinimumFrequency(float minFrequency) {
		if(minFrequency<0 || minFrequency>1) {
			throw new IllegalArgumentException();
		}
		this.minimumSupport = minFrequency;
	}
}

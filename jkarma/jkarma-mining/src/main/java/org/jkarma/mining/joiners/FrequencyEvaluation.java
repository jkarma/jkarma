/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

/**
 * Defines the public interface of pattern evaluation objects allowing
 * the inspection of some frequency-related condition.
 * Since the evaluation may rely on different notions of frequency, the
 * public interface allows to return different informations.
 * @author Angelo Impedovo
 */
public interface FrequencyEvaluation {
	
	/**
	 * Returns the absolute frequency associated with the evaluation.
	 * @return The absolute frequency associated with the evaluation.
	 */
	public int getAbsoluteFrequency();
	
	
	
	/**
	 * Returns the number of transactions associated with the relative
	 * frequency of the evaluation.
	 * @return The number of transactions associated with the relative
	 * frequency of the evaluation.
	 */
	public int getTransactionCount();
	
	
	
	/**
	 * Associate a certain number of transactions to the evaluation.
	 * The number of transaction usually is the denominator of the relative
	 * frequency.
	 * @param t The number of transactions to be associated with the
	 *  evaluation.
	 */
	public void setTransactionCount(int t);
	
	
	
	/**
	 * Returns the relative frequency associated with the evaluation.
	 * @return The relative frequency associated with the evaluation.
	 */
	public double getRelativeFrequency();
	
	
	
	/**
	 * Returns whether the evaluation denotes a frequent object.
	 * In practice, returns true if the relative frequency exceeds
	 * a minimum relative frequency threshold, false otherwise.
	 * @param minSup The minimum relative frequency threshold.
	 * @return True if the relative frequency exceeds
	 *  the minimum relative frequency threshold, false otherwise
	 */
	public boolean isFrequent(double minSup);
}

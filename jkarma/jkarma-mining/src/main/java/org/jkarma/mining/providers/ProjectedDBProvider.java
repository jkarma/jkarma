/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.providers;

import org.jkarma.mining.joiners.ProjectedDB;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.windows.WindowingStrategy;
import org.jkarma.model.Transaction;


/**
 * A provider of ProjectedDB objects starting from transactions of type A.
 * @author Angelo Impedovo
 *
 * @param <A> Type of transactions.
 */
public class ProjectedDBProvider<A extends Comparable<A>> extends WindowedProvider<A, ProjectedDB<A>> {

	
	/**
	 * Constructs a new ProjectedDBProvider based on a given time window model.
	 * @param windowing The time window model.
	 */
	public ProjectedDBProvider(WindowingStrategy<ProjectedDB<A>> windowing) {
		super(windowing);
	}

	
	
	/**
	 * Returns a new empty ProjectedDB.
	 */
	@Override
	public ProjectedDB<A> getNewInstance() {
		return new ProjectedDB<A>(0);
	}

	
	
	/**
	 * Consumes a single transaction updating the recent ProjecteDB objects associated
	 * to items from the transaction. Note that if there's an item that has
	 * never been observed before, the ProjectedDB is created and therefore updated.
	 */
	@Override
	public void accept(Transaction<A> transaction) {
		//we consume only not null transaction
		if(transaction!=null) {
			//we register the received transaction for each item 
			for(A item : transaction.getItems()) {
				Pair<ProjectedDB<A>> pair = super.apply(item);
				ProjectedDB<A> db = pair.getIncrement();
				
				//db projection
				db.addItem(transaction);
				db.i = item;
				//pair.setIncrement(db.project(item));
			}
			
			//we increment the transaction counter
			this.summary.consumedTransactionsCount++;
		}
	}
	
	
	
	@Override
	public Pair<ProjectedDB<A>> apply(A key){
		Pair<ProjectedDB<A>> result = this.cache.get(key);
		if(result==null) {
			ProjectedDB<A> s1 = this.getNewInstance();
			ProjectedDB<A> s2 = this.getNewInstance();
			s1.i = key;
			s2.i = key;
			result = Pair.of(s1, s2);
			this.cache.put(key, result); //side effecting?
		}else {
			ProjectedDB<A> aggr = result.getAggregate();
			ProjectedDB<A> incr = result.getIncrement();
			if(aggr.i==null && incr.i!=null) {
				aggr.i = incr.i;
			}else if(incr.i==null && aggr.i!=null) {
				incr.i = aggr.i;
			}
		}
		
		//as for WindowedProvider
		ProjectedDB<A> first = result.getAggregate();
		ProjectedDB<A> second = result.getIncrement();
		first.setTransactionCount(this.summary.getCachedCount());
		second.setTransactionCount(this.summary.getConsumedCount());
		
		return result;
	}
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.heuristics;

import org.jkarma.mining.joiners.FrequencyEvaluation;
import org.jkarma.mining.providers.Context;

/**
 * This class implements a pattern heuristic evaluation based on the pattern's area.
 * The area of a pattern is defined as the product between the pattern length and the
 * associated absolute frequency.
 * Necessary condition to compute the absolute frequency is that B implements the
 * FrequencyEvaluation interface. 
 * @author Angelo Impedovo
 *
 * @param <A> Type denoting the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 * @see <a href='https://doi.org/10.1007/978-3-540-30214-8_22'>Discovery Science 2004, Tiling Databases</a>
 */
public class AreaHeuristic<A,B extends FrequencyEvaluation> implements Heuristic<A,B>{

	/**
	 * If true the heuristic will compute the inverse of area.
	 * Otherwise the classic area will be computed.
	 */
	boolean isReciprocal = false;
	
	
	
	/**
	 * Instantiate an area heuristic.
	 * If the reciprocal flag is set to true, then the inverse area will be computed. 
	 * Otherwise, the classic area will be computed.
	 * @param reciprocal The reciprocal flag.
	 */
	public AreaHeuristic(boolean reciprocal) {
		this.isReciprocal = reciprocal;
	}
	
	/**
	 * Computes the area of a pattern, defined as the product between the itemset length
	 * and the absolute frequency.
	 * @param suffix The last item added to the pattern
	 * @param pastEval The evaluation of the pattern on past data
	 * @param newEval The evaluation of the pattern with incoming data
	 * @param ctx The mining context
	 * @param itemsetLength The number of items in the pattern 
	 * @param onRemote Flag indicating whether the area should be computed on pastEval
	 *  rather than newEval
	 * @return A numerical evaluation of the pattern
	 */
	public double apply(A suffix, B pastEval, B newEval, Context ctx, int itemsetLength, boolean onRemote) {
		B target;
		double area;
		if(onRemote) {
			target = pastEval;
		}else {
			target = newEval;
		}
		
		if(!isReciprocal) {
			area = target.getAbsoluteFrequency() * itemsetLength;
		}else {
			area = (target.getTransactionCount()-target.getAbsoluteFrequency()) * itemsetLength;
		}
		
		return area;
	}

}

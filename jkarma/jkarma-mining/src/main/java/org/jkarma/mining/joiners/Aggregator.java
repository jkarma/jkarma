/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

/**
 * Defines the public interface for aggregator objects.
 * An aggregator implements the merging logic for a specified type A,
 * that is the computational steps to perform when merging two objects
 * of type A into a new object always of type A (like the reduce semantic
 * in a map-reduce-like programming paradigm).
 * The Aggregator extends the Joiner interface on the same type A.
 * 
 * @author Angelo Impedovo
 *
 * @param <A> Type of objects to be aggregated
 */
public interface Aggregator<A> extends Joiner<A> {
	
	/**
	 * Produces a new instance of A aggregating the content
	 * of two objects of type A.
	 * @param first The first object to be aggregated.
	 * @param second The second object to be aggregated.
	 * @return The resulting aggregation of the two objects.
	 */
	public A reduce(A first, A second);
	
}

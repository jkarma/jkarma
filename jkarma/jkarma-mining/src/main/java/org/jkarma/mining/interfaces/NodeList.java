/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.interfaces;


/**
 * Public interface of the list of children nodes associated to a node in a tree.
 * The list is an iterable data structure of nodes.
 * 
 * @author Angelo Impedovo
 * @param <B> Type of informative content associated to the node.
 */
public interface NodeList<B extends Node<?>> extends Iterable<B> {
	
	/**
	 * Returns the first node in the list of children.
	 * @return The first node in the list of children.
	 */
	public B getFirstChild();
	
	
	
	/**
	 * Returns the last node in the list of children.
	 * @return The last node in the list of children.
	 */
	public B getLastChild();
	
	
	/**
	 * Returns the next sibling of a node in the list, if any.
	 * @param node The node to be searched in the list.
	 * @return The next sibling of node in this list.
	 */
	public B getNextSibling(B node);
	
	
	
	/**
	 * Returns the previous sibling of a node in the list, if any.
	 * @param node The node to be searched in the list.
	 * @return The previous sibling of node in this list.
	 */
	public B getPreviousSibling(B node);
	
	
	/**
	 * The number of nodes in the list of children nodes.
	 * @return The number of nodes in the list of children nodes.
	 */
	public int count();
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import java.util.List;
import java.util.Set;

import org.jkarma.mining.providers.Context;
import org.jkarma.mining.windows.WindowingStrategy;

/**
 * Public interface for defining components which selects patterns
 * starting from given one, according to some unspecified criteria.
 * @author Angelo Impedovo
 * @param <A> Type of allowed pattern symbols.
 * @param <B> Type of pattern evaluation.
 */
public interface Selector<A extends Comparable<A>,B> {
	
	/**
	 * Selects what items to consider for building 1-itemsets.
	 * @param ctx The mining context.
	 * @param windowing The time window model adopted by the mining strategy.
	 * @param lattice The lattice of patterns.
	 * @param generators The set of generators from which to select items.
	 * @param onRemote Flag indicating whether the selector should decide on
	 *  the basis of evaluations computed on remote or recent evaluation of patterns.
	 * @return The selection (a subset) of items to be considered when searching
	 *  1-itemsets in the mining process.
	 */
	public List<A> select(Context ctx, WindowingStrategy<B> windowing, SETree<A, Pair<B>> lattice, Set<A> generators, boolean onRemote);
	
	
	/**
	 * Selects what siblings of a given n-itemset to consider for building 
	 * (n+1)-itemsets starting.
	 * @param ctx The mining context.
	 * @param windowing The time window model adopted by the mining strategy.
	 * @param node The n-itemset.
	 * @param height The length of the n-itemset (n).
	 * @param onRemote Flag indicating whether the selector should decide on
	 *  the basis of evaluations computed on remote or recent evaluation of patterns.
	 * @return The selection (a subset) of sibling to be joined with node to 
	 *  build the (n+1)-itemsets. 
	 */
	public List<A> select(Context ctx, WindowingStrategy<B> windowing, SENode<A, Pair<B>> node, int height, boolean onRemote);
}

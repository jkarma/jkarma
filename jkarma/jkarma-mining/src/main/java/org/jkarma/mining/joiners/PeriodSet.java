/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Implements a periodicity evaluation backed by a tidset.
 * In particular, the periodicity is evaluated based on the gaps between the
 * tids registered in the tidset.
 * A gap is a time-difference between two transactions covering the same pattern.
 * @author Angelo Impedovo
 *
 */
@SuppressWarnings("serial")
public class PeriodSet extends TidSet implements PeriodicityEvaluation {

	/**
	 * The start time point from which the periodicity is evaluated.
	 */
	public int startIndex;

	
	
	/**
	 * Constructs a new PeriodSet starting at a given transaction id
	 * and concerning a given number of transactions.
	 * @param startIndex The starting transaction id.
	 * @param tCount The number of transactions.
	 */
	public PeriodSet(int startIndex, int tCount) {
		super(tCount);
		if(startIndex<0) {
			throw new IllegalArgumentException();
		}
		this.startIndex = startIndex;
	}
	
	
	
	/**
	 * Returns the minimum periodicity.
	 * The minimum periodicity is computed as the minimum gap observed
	 * between consecutive tids.
	 */
	@Override
	public int getMinimumPeriodicity() {
		return Collections.min(this.getGaps());
	}

	
	
	/**
	 * Returns the maximum periodicity.
	 * The minimum periodicity is computed as the maximum gap observed 
	 * between consecutive tids.
	 */
	@Override
	public int getMaximumPeriodicity() {
		return Collections.max(this.getGaps());
	}

	
	
	/**
	 * Returns the average periodicity.
	 * The average periodicity is computed as the average gap observed
	 * observed between consecutive tids.
	 */
	@Override
	public double getAveragePeriodicity() {
		return this.getTransactionCount()/((double)this.getAbsoluteFrequency()+1);
	}


	
	/**
	 * Returns true if the average periodicty is bounded between the average periodicity bounds,
	 * the minimum periodicity is greater than the periodicity lower bound and the maximum
	 * periodicity is lower than the periodicity upper bound, false otherwise.
	 * The periodicity condition is the same adopted by the PFPM algorithm.
	 * 
	 * @see <a href="https://doi.org/10.5772/66780">Proceedings of the 2nd Czech-China 
	 * Scientific Conference 2016, 2017, PFPM: Discovering Periodic Frequent Patterns 
	 * with Novel Periodicity Measures</a>
	 */
	@Override
	public boolean isPeriodic(int minPer, int maxPer, double minAvgPer, double maxAvgPer) {
		double avgPer = this.getAveragePeriodicity();
		boolean minPerCondition = this.getMinimumPeriodicity() >= minPer;
		boolean maxPerCondition = this.getMaximumPeriodicity() <= maxPer;
		boolean avgPerCondition = minAvgPer <= avgPer && avgPer <= maxAvgPer;
		return minPerCondition && avgPerCondition && maxPerCondition;
	}


	
	/**
	 * Convenience method, returns an intermediate representation of the tidset
	 * as a sequence of gaps between consecutive tids.
	 * @return The sequence of gaps between consecutive tids.
	 */
	private List<Integer> getGaps(){
		List<Integer> periods = new ArrayList<>();
		if(this.size()>0) {
			int lastVal = this.startIndex;
			for(Integer tid : this) {
				int gap = Math.abs(lastVal-tid);
				periods.add(gap);
				lastVal = tid;
			}
			//periods.add(Math.abs(lastVal-endIndex));
		}else {
			periods.add(this.transactionCount);
		}
		
		
		return periods;
	}
	
	@Override
	public String toString() {
		return super.toString()+" -> startAt:"+this.startIndex+":"+this.getGaps();
	}
}

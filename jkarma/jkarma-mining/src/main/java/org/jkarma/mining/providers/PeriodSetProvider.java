/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.providers;

import org.jkarma.mining.joiners.PeriodSet;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.windows.WindowingStrategy;
import org.jkarma.model.Transaction;


/**
 * A provider of PeriodSet objects starting from transactions of type A.
 * @author Angelo Impedovo
 *
 * @param <A> Type of transactions.
 */
public class PeriodSetProvider<A> extends WindowedProvider<A, PeriodSet> {

	/**
	 * Id of the last transaction consumed.
	 */
	private int lastTid;
	
	
	/**
	 * Last periodicity starting point that has been considered.
	 */
	private int lastStart;
	
	
	
	/**
	 * Constructs a new PeriodSetProvider based on a given time window model.
	 * @param windowing The time window model.
	 */
	public PeriodSetProvider(WindowingStrategy<PeriodSet> windowing){
		super(windowing);
		this.lastTid = 0;
		this.lastStart = 0;
	}

	
	
	/**
	 * Returns a new empty PeriodSet starting at the last starting poing considered.
	 */
	@Override
	public PeriodSet getNewInstance() {
		return new PeriodSet(this.lastStart,0);
	}

	
	
	/**
	 * Consumes a single transaction updating the recent PeriodSet objects associated
	 * to items from the transaction. Note that if there's an item that has
	 * never been observed before, the PeriodSet is created and therefore updated.
	 */
	@Override
	public void accept(Transaction<A> transaction) {
		//we consume only not null transaction
		if(transaction!=null) {
			int tid = transaction.getId();
			
			//we register the received transactionId for each item 
			for(A item : transaction) {
				Pair<PeriodSet> pair = super.apply(item);
				PeriodSet tidset = pair.getIncrement();
				tidset.add(tid);
			}
			
			//we increment the transaction counter
			this.summary.consumedTransactionsCount++;
			this.lastTid = tid;
		}
	}
	
	
	
	/**
	 * Overrides the default WindowedProvider.flatten method in order to keep
	 * consistency on the periodicity starting point. When aggregating evaluations, new
	 * produced PeriodSet should start at the id of the last consumed transaction.
	 */
	@Override
	public void flatten() {
		super.flatten();
		this.lastStart = this.lastTid;
	}
}

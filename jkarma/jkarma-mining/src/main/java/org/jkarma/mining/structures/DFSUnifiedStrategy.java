package org.jkarma.mining.structures;

import java.util.Comparator;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.jkarma.mining.events.ItemSetAddedEvent;
import org.jkarma.mining.events.ItemSetAddedRegisteredEvent;
import org.jkarma.mining.events.ItemSetRemovedEvent;
import org.jkarma.mining.events.ItemSetRemovedRegisteredEvent;
import org.jkarma.mining.events.ItemSetUpdatedEvent;
import org.jkarma.mining.heuristics.Heuristic;
import org.jkarma.mining.interfaces.ItemSet;
import org.jkarma.mining.interfaces.Lattice;
import org.jkarma.mining.joiners.Aggregator;
import org.jkarma.mining.joiners.Joiner;
import org.jkarma.mining.providers.BaseProvider;
import org.jkarma.mining.providers.Context;
import org.jkarma.mining.windows.WindowingStrategy;

/**
 * Implements an incremental mining strategy for streaming data sources
 * with a unified search algorithm. The unified algorithm implements a
 * general search strategy approach that supports both DFS and beam-search 
 * based on heuristics and beam-size k.
 * The mining strategy is unified since both exhaustive (DFS) and
 * non-exhaustive mining (beam search) is implemented in a transparent way.
 * In particular the logic is selector-transparent, meaning that the beam size
 * works only on the pattern selected according to the selector at hand (such as
 * DFSSelector, DFSPpcClosedSelector and DFSPpcMaxSelector).
 * In particular, non-exhaustive mining strategies depend on heuristics and
 * a beam-size k. 
 * @author Angelo Impedovo
 * @param <A> Type of allowed pattern symbols.
 * @param <B> Type of pattern evaluation.
 */
public class DFSUnifiedStrategy<A extends Comparable<A>, B> extends MiningStrategy<A,B> {

	/**
	 * A selector delegate for selecting the patterns to be considered in the
	 * recursive steps of the mining procedure.
	 */
	protected Selector<A,B> selector;


	/**
	 * Builds a DFSUnifiedStrategy implementing an exhaustive mining algorithm
	 * based on the selector provided.
	 * 
	 * @param generators The items to be considered in the mining process.
	 * @param terminologicalExpander The lexicographic joiner delegate used to build patterns.
	 * @param supportProvider The data accessor delegate used to build 1-itemset evaluations.
	 * @param supportJoiner The evaluator delegate used to evaluate patterns.
	 * @param selector The ranking criteria for patterns 
	 * @param maxDepth The maximum length of patterns to be discovered.
	 */
	public DFSUnifiedStrategy(Set<A> generators, Joiner<A> terminologicalExpander, BaseProvider<A, B> supportProvider, 
			Aggregator<B> supportJoiner, Selector<A, B> selector, int maxDepth) {
		super(generators, terminologicalExpander, supportProvider, supportJoiner, maxDepth);
		this.selector = new UnifiedSelector(selector);
	}


	/**
	 * Builds a DFSUnifiedStrategy implementing a non-exhaustive mining algorithm
	 * based on the selector and the heuristic provided.
	 * The non-exhaustive is performed with a beam-search approach which selects
	 * only the top-beamSize patterns when joining them.
	 * 
	 * @param generators The items to be considered in the mining process.
	 * @param terminologicalExpander The lexicographic joiner delegate used to build patterns.
	 * @param supportProvider The data accessor delegate used to build 1-itemset evaluations.
	 * @param supportJoiner The evaluator delegate used to evaluate patterns.
	 * @param selector The selector delegate used to select patterns to be joined.
	 * @param heuristic The ranking criteria for selected patterns
	 * @param beamSize The maximum number of patterns to be considered in the ranking. 
	 * @param maxDepth The maximum length of patterns to be discovered.
	 */
	public DFSUnifiedStrategy(Set<A> generators, Joiner<A> terminologicalExpander, BaseProvider<A, B> supportProvider, 
			Aggregator<B> supportJoiner, Selector<A, B> selector, Heuristic<A,B> heuristic, int beamSize, int maxDepth) {
		super(generators, terminologicalExpander, supportProvider, supportJoiner, maxDepth);
		this.selector = new UnifiedSelector(selector, heuristic, beamSize);
	}

	
	@Override
	public Lattice<ItemSet<A, Pair<B>>> execute() throws TransactionException {
		//if not locked, locks the strategy and begins the mining
		if(!this.isLocked()) {
			this.setLocked(true);
			
			//we build a proper set enumeration tree for the first time
			if(this.lattice==null) {
				this.lattice = new SETree<>();
				this.lattice.root = new SENode<>();
				this.lattice.nodeCount = 1;
			}
			
			//then we update new nodes by accessing new content in the provider (if any)
			this.expand(true);
			//we first update past nodes (if possible)
			this.expand(false);
			
		}else{
			//if the strategy is locked, we raise an exception.
			throw new TransactionException();
		}
		
		return this.lattice;
	}
	
	

	
	/**
	 * Method implementing the mining base step.
	 * @param useNewData
	 */
	protected void expand(boolean useNewData) {
		//the recursion starts at depth 1 of the SE-Tree with the root node
		Context ctx = this.provider.getContext();
		SENode<A,Pair<B>> node = this.lattice.root;
		int depth = 1;
		
		//a list of items is selected (either exhaustively or not) to be 1-itemset candidate
		//they are iterated in reverse order.
		List<A> symbols = this.selector.select(ctx, this.windowing, this.lattice, this.generators, useNewData);

		
		//we first iterate on nodes that alreay are in the lattice that were not selected
		for(SENode<A, Pair<B>> child : node.children) {
			A symbol = child.getSuffix();
			if(!symbols.contains(child.getSuffix())) {
				//the node is not in the beam, we update it and we mark it for deletion
				if(useNewData) {
					Pair<B> provided = this.provider.apply(symbol);
					child.data.eval.setAggregate(this.windowing.andThen(
						child.data.eval.getAggregate(), child.data.eval.getIncrement()
					));
					child.data.eval.setIncrement(provided.getIncrement());
				}
				child.setMarkedAsRemoved();
			}
		}
		
		//then we iterate on the selected item (which could also contain not previously observes ones)
		ListIterator<A> iterator = symbols.listIterator(symbols.size());
		while(iterator.hasPrevious()) {
			A symbol = iterator.previous();
			SENode<A,Pair<B>> child = node.children.get(symbol);
			
			//we prepare the evaluation
			Pair<B> evaluation = null;
			if(super.generators.contains(symbol)) {
				//if the symbol is an active generator...
				B increment, aggregate = null;
				if(child!=null) {
					//and is even a child of the root node, then...
					//...we should update his evaluation!
					if(useNewData) {
						Pair<B> provided = this.provider.apply(symbol); //SIDE EFFECT HERE
						aggregate = this.windowing.andThen(
							child.data.eval.getAggregate(), child.data.eval.getIncrement()
						);
						increment = provided.getIncrement();
					}else {
						aggregate = child.data.eval.getAggregate();
						increment = child.data.eval.getIncrement();
					}
				}else {
					Pair<B> provided = this.provider.apply(symbol);
					increment = provided.getIncrement();
					aggregate = provided.getAggregate();
				}
				evaluation = Pair.of(aggregate, increment);
			}else {
				//if the symbol is neither an active generator nor a child of the root node, then...
				if(child!=null) {
					child.setMarkedAsRemoved();
					//dummy empty increment
					this.eventBus.post(new ItemSetRemovedRegisteredEvent<>(child));
				}
			}			
			
			
			//if we managed to compute a proper evaluation object, then we test it...
			if(evaluation!=null) {
				B eval;
				if(useNewData) {
					eval = this.windowing.getRemoteWindow(evaluation.getAggregate(), evaluation.getIncrement());
				}else {
					eval = this.windowing.getRecentWindow(evaluation.getAggregate(), evaluation.getIncrement());
				} 
				
				
				boolean hasPassedEvaluation = this.evaluator.testPostcondition(eval, ctx, depth);
				if(hasPassedEvaluation){
					//the object has passed the evaluation
					if(child!=null) {
						//there is an existing child, we update its content
						child.data.eval = evaluation;
						child.setUpdated();
						this.eventBus.post(new ItemSetUpdatedEvent<>(child));
					}else {
						if(hasPassedEvaluation) {
							//there is not an existing child, we add a new node
							child = new SENode<>(MutablePair.of(symbol, evaluation), EnumSet.of(SENodeStatus.UPDATED));
							node.addChild(child);
							this.eventBus.post(new ItemSetUpdatedEvent<>(child));
						}else {
							//there is not an existing child, we add a new node
							child = new SENode<>(MutablePair.of(symbol, evaluation), EnumSet.of(SENodeStatus.ADDED));
							node.addChild(child);
							this.eventBus.post(new ItemSetAddedRegisteredEvent<>(child));
						}
					}
				}else {
					//the object has not passed the evaluation
					if(child!=null) {
						child.data.eval = evaluation;
						child.setMarkedAsRemoved();
						this.eventBus.post(new ItemSetRemovedRegisteredEvent<>(child));
					}else {
						
					}
				}
			}else {
				//nothing to do here
			}
		}		
		
		//we iterate on every node, regardeles of whether it was selected/not-selected valid/invalid
		for(SENode<A,Pair<B>> child : this.lattice.root.children) {
			this.expand(child, depth+1, useNewData);
		}
	}
	
	
	/**
	 * Method implementing the mining recursive step.
	 * @param node
	 * @param depth
	 * @param willAddNewNodes
	 * @param useNewData
	 */
	protected void expand(SENode<A,Pair<B>> node, int depth, boolean useNewData) {
		Context ctx = this.provider.getContext();
		
		//depth recursion limit
		if(depth <= this.getMaxDepth()) {
			List<A> symbolsSelected = this.selector.select(ctx, this.windowing, node, depth, useNewData);
			ListIterator<A> iterator = symbolsSelected.listIterator(symbolsSelected.size());
			
			//let's iterate on the candidate symbols
			while(iterator.hasPrevious()) {
				A symbol = iterator.previous();			

				//we test any precondition on the symbol
				boolean symbolPassedPreTest = this.expander.testPrecondition(node.data.suffix, symbol, ctx, depth);
				if(symbolPassedPreTest) {
					//we get the sibling of the node associated with the symbol
					SENode<A,Pair<B>> sibling = this.getChildBySymbol(node.parent, symbol, depth);
					//then we join the symbol of the node with the symbol of the sibling
					A candidateSymbol = this.expander.apply(node.data.suffix, sibling.data.suffix, depth);
					//we test the candidate symbol against any post condition
					boolean symbolCondition = this.expander.testPostcondition(candidateSymbol, ctx, depth);
					//and we get the node associated to the candidate symbol (if any)
					SENode<A,Pair<B>> child = this.getChildBySymbol(node, candidateSymbol, depth);
					
					//and then we also join the evaluation of the node with that of the sibling...
					Pair<B> evaluation;
					if(child!=null) {
						if(useNewData) {
							//we update the existing evaluation
							evaluation = this.updatedEvaluation(child.data.eval, node.data.eval, sibling.data.eval, depth);
						}else {
							//we re-use the existing evaluation
							evaluation = child.data.eval;
						}
						
					}else {
						//we reconstruct the overall evaluation
						evaluation = this.computeEvaluation(node.data.eval, sibling.data.eval, depth);
					}
					
					//...so we can test it
					B eval;
					if(useNewData) {
						eval = this.windowing.getRemoteWindow(evaluation.getAggregate(), evaluation.getIncrement());
					}else {
						eval = this.windowing.getRecentWindow(evaluation.getAggregate(), evaluation.getIncrement());
					} 
					
					boolean hasPassedPostTest = this.evaluator.testPostcondition(eval, ctx, depth);
					if(hasPassedPostTest) {
						//the symbol passed the evaluation
						if(child==null) {
							Entry<A,Pair<B>> entry = MutablePair.of(candidateSymbol, evaluation);
							child = new SENode<A,Pair<B>>(entry, EnumSet.of(SENodeStatus.UPDATED));
							child.setOrdinary(symbolCondition);
							this.addChildBySymbol(node, child, symbolCondition, depth);
							if(symbolCondition) {
								this.eventBus.post(new ItemSetUpdatedEvent<>(child));
							}
						}else {
							//we notify an update event (only in case of valid symbol)
							child.data.eval = evaluation;
							child.setUpdated();
							child.setOrdinary(symbolCondition);
							if(symbolCondition) {
								this.eventBus.post(new ItemSetUpdatedEvent<>(child));
							}
						}
					}else {
						//the symbol hasn't passed the evaluation
						if(child!=null) {
							child.data.eval = evaluation;
							child.setMarkedAsRemoved();
							child.setOrdinary(symbolCondition);
							
							//we notify a new deletion event (only in case of valid symbol)
							if(symbolCondition) {
								this.eventBus.post(new ItemSetRemovedRegisteredEvent<>(child));
							}
						}
					}
				}else {
					//symbol not allowed by symbolic precondition
				}
			}
		}
		
		//recursion steps
		for(SENode<A,Pair<B>> child : node.children) {
			if(!child.isMarkedAsRemoved()) {
				this.expand(child, depth+1, useNewData);
			}
		}
	}



	@Override
	public Lattice<ItemSet<A, Pair<B>>> forget() throws TransactionException {
		if(!this.isLocked()) {
			this.setLocked(true);
			//we forget the past transactions for each of the current itemsets
			Iterator<ItemSet<A,Pair<B>>> iterator = this.lattice.completeIterator();
			while(iterator.hasNext()) {
				ItemSet<A, Pair<B>> itemset = iterator.next();
				SENode<A,Pair<B>> node = (SENode<A,Pair<B>>)itemset;
				
				//except for the root itemset
				if(node.getEval()!=null) {
					Pair<B> eval = node.getEval();
					//we forget any aggregate evaluation
					eval.setAggregate(this.provider.getNewInstance());
					//eval.setIncrement(eval.this.provider.getNewInstance());
				}
			}
			
			this.expand(false);
			this.commit();
		}else{
			throw new TransactionException();
		}
		return this.lattice;
	}
	
	


	@Override
	public void commit() throws TransactionException {
		if(this.isLocked()) {
			Iterator<ItemSet<A,Pair<B>>> iterator = this.lattice.completeIterator();
			while(iterator.hasNext()) {
				SENode<A,Pair<B>> node = (SENode<A,Pair<B>>)iterator.next();
				if(node.isMarkedAsAdded()) {
					//any commit operation must set as "updated" every "added" node
					//however, the event must be raised AFTER updating nodes and only for non-discarded ones
					if(node.isOrdinaryNode()) {
						node.setUpdated();
						super.eventBus.post(new ItemSetAddedEvent<>(node.data));
						this.lattice.nodeCount++;
					}else {
						node.setUpdated();
					}
					
				}else if(node.isMarkedAsRemoved()) {
					//any commit operation must prune every "removed" node from the lattice
					//however the event must be raised BEFORE removing nodes and only for non-discarded ones
					if(node.isOrdinaryNode()) {
						super.eventBus.post(new ItemSetRemovedEvent<>(node.data));
						iterator.remove();
						this.lattice.nodeCount--;
					}else {
						iterator.remove();
					}
				}
			}
			
			//operation completed flag
			this.setLocked(false);
			
			//in case of deferred evaluations we need to reconstruct the lattice
			//of all the accepted itemsets in the expanded landmark windows
			if(this.provider.getContext().isDeferredEvaluation()) {
				this.provider.getContext().deferredEvaluation = false;
				this.setLocked(true);
				this.expand(false);
				this.commit();
				this.provider.getContext().deferredEvaluation = true;
			}
		}else{
			//throw exception
			throw new TransactionException();
		}
		
	}
	
	
	
	@Override
	public void rollback() throws TransactionException {
		if(this.isLocked()) {
			Iterator<ItemSet<A,Pair<B>>> iterator = this.lattice.completeIterator();
			while(iterator.hasNext()) {
				SENode<A,Pair<B>> node = (SENode<A,Pair<B>>)iterator.next();
				if(node.isMarkedAsAdded()) {
					//any rollback operation must prune any node markead as "added"
					//regardless whether they are ordinary nodes or not.
					iterator.remove();
				}else if(node.isMarkedAsRemoved()) {
					//any rollback operation must set as "updated" any node marked as "removed"
					//regardless whether they are ordinary nodes or not.
					node.setUpdated();
				}
				
				//we forget the latest increment in the evaluation (except the root)...
				if(node.getEval()!=null) {
					node.getEval().setIncrement(null);
				}	
			}
			
			//operation completed flag
			this.setLocked(false);
		}else {
			//throw exception
			throw new TransactionException();
		}
	}

	
	
	private void addChildBySymbol(SENode<A,Pair<B>> node, SENode<A,Pair<B>> child, boolean fromChild, int depth){
		if(fromChild) {
			node.addChild(child);
		}else {
			node.addDiscardedChild(child);
		}
	}
	
	
	
	private SENode<A,Pair<B>> getChildBySymbol(SENode<A,Pair<B>> node, A symbol, int depth){
		SENode<A,Pair<B>> child = null;
		Context ctx = this.provider.getContext();
		
		//let's retrieve the associated child (considering the symbolic 
		//postconditional evaluation);
		boolean isJoinable = this.expander.testPostcondition(symbol, ctx, depth);
		if(isJoinable) {
			child = node.children.get(symbol);
		}else {
			child = node.discardedChildren.get(symbol);
		}
		
		return child;
	}
	
		
	
	private Pair<B> updatedEvaluation(Pair<B> evalToUpdate, Pair<B> firstAncestorEval, Pair<B> secondAncestorEval, int depth) {
		Pair<B> updatedEvaluation = evalToUpdate;
		
		//il nodo esiste, aggiorno la valutazione
		B evaluatedIncrement;
		B childLastAggregate = evalToUpdate.getAggregate();
		B childLastIncrement = evalToUpdate.getIncrement();
		B nodeLastIncrement = firstAncestorEval.getIncrement();
		B pairLastIncrement = secondAncestorEval.getIncrement();
		evaluatedIncrement = this.evaluator.apply(nodeLastIncrement, pairLastIncrement, depth);
		
		//aggrego i dati precedenti, quindi aggiungo l'ultimo incremento
		updatedEvaluation.setAggregate(this.windowing.andThen(childLastAggregate, childLastIncrement));
		updatedEvaluation.setIncrement(evaluatedIncrement);
		
		return updatedEvaluation;
	}
	
	
	
	
	private Pair<B> computeEvaluation(Pair<B> firstAncestorEval, Pair<B> secondAncestorEval, int depth){
		B evaluatedIncrement, evaluatedAggregate;
		
		//il nodo non esiste, ricreo il nodo, la valutazione e lo aggiungo
		B nodeLastIncrement = firstAncestorEval.getIncrement();
		B nodeLastAggregate = firstAncestorEval.getAggregate();
		B pairLastIncrement = secondAncestorEval.getIncrement();
		B pairLastAggregate = secondAncestorEval.getAggregate();
		evaluatedIncrement = this.evaluator.apply(nodeLastIncrement, pairLastIncrement, depth);
		evaluatedAggregate = this.evaluator.apply(nodeLastAggregate, pairLastAggregate, depth);
		Pair<B> result = Pair.of(evaluatedAggregate, evaluatedIncrement);
		
		//System.out.println("merging "+firstAncestorEval+" + "+secondAncestorEval+" = "+result);
		//preparo la valutazione complessiva
		return result;
	}



	private class UnifiedSelector implements Selector<A,B>{
		

		/**
		 * The heuristic to be evaluated when selecting patterns.
		 * When no heuristic has been passed, the selection is exhaustive.
		 */
		private Heuristic<A,B> heuristic;


		/**
		 * The beam size controlling the number of selected patterns.
		 */
		private int beamSize;


		/**
		 * The base selector returning the patterns to be evaluated by
		 * this selector. 
		 */
		private Selector<A,B> baseSelector;
		
		
		/**
		 * Instantiate a selector running in exhaustive modeon top
		 * of items selected according to some logic. All of them are
		 * returned.
		 * @param selector
		 */
		public UnifiedSelector(Selector<A,B> baseSelector) {
			if(baseSelector==null) {
				throw new IllegalArgumentException();
			}
			this.baseSelector = baseSelector;
			this.heuristic = null;
			this.beamSize = 0;
		}

		
		
		/**
		 * Instantiate a selector running in non-exhaustive mode on top
		 * of items selected according to some logic. Only top-k of them
		 * are returned, according to an heuristic criteria.
		 * @param baseSelector
		 * @param heuristic
		 * @param beamSize
		 */
		public UnifiedSelector(Selector<A,B> baseSelector, Heuristic<A, B> heuristic, int beamSize) {
			if(baseSelector==null || heuristic==null || beamSize<1) {
				throw new IllegalArgumentException();
			}
			this.baseSelector = baseSelector;
			this.beamSize = beamSize;
			this.heuristic = heuristic;
		}

		
		
		/**
		 * Given the items, we should consider only
		 * the top-k among them according to an heuristic evaluation
		 * function.
		 */
		@Override
		public List<A> select(Context ctx, WindowingStrategy<B> windowing, SETree<A, Pair<B>> lattice,
				Set<A> generators, boolean onRemote) {
			List<A> topK;
			
			//we get the base selection of items
			List<A> items = this.baseSelector.select(ctx, windowing, lattice, generators, onRemote);
			if(!this.isExhaustive()) {
				//if running in non-exhaustive node, we filter the selection
				//by selecting the most promising top-k items according to the heuristic.
				topK = this.topK(items, ctx, windowing, onRemote);
			}else {
				//otherwise, if running in exhaustive mode, we return the base selection as is.
				topK = items;
			}
			
			return topK;
		}

		
		
		/**
		 * Given the candidates selected, we should consider only
		 * the top-k among them according to an heuristic evaluation
		 * function.
		 */
		@Override
		public List<A> select(Context ctx, WindowingStrategy<B> windowing, SENode<A, Pair<B>> node, 
				int height, boolean onRemote) {
			List<A> topK;
			
			//we get the base selection of items
			List<A> items = this.baseSelector.select(ctx, windowing, node, height, onRemote);
			if(!this.isExhaustive()) {
				//if running in non-exhaustive node, we filter the selection
				//by selecting the most promising top-k items according to the heuristic.
				topK = this.topK(items, ctx, windowing, onRemote);
			}else {
				//otherwise, if running in exhaustive mode, we return the base selection as is.
				topK = items;
			}
			return topK;
		}
		
		
		
		/**
		 * Returns true whether this selector is running in exhaustive mode or not.
		 * @return
		 */
		public boolean isExhaustive() {
			return this.heuristic==null;
		}
		
		
		
		/**
		 * Private method which ranks a selection of items according to the heuristic
		 * criteria, thus selecting only the most promising top-k items.
		 * @param selection
		 * @param ctx
		 * @param windowing
		 * @return
		 */
		private List<A> topK(List<A> selection, Context ctx, WindowingStrategy<B> windowing, boolean onRemote){
			TreeSet<Entry<A,Double>> ranking = new TreeSet<>(
				new Comparator<Entry<A,Double>>(){
					@Override
					public int compare(Entry<A, Double> arg, Entry<A, Double> arg1) {
						int result = Double.compare(arg.getValue(), arg1.getValue()) * -1; //decresing order;
						if(result==0) {
							result = arg.getKey().compareTo(arg1.getKey()); //decreasing order
						}
						return result;
					}
				}
			);
				
			//we compute the ranking according to the heuristic
			for(A item : selection) {
				B pastData = provider.getPastByKey(item);
				B newBlock = provider.getByKey(item);
				B e1 = windowing.getRemoteWindow(pastData, newBlock);
				B e2 = windowing.getRecentWindow(pastData, newBlock);
				double score = heuristic.apply(item, e1, e2, ctx, 1, onRemote);
				ranking.add(ImmutablePair.of(item,  score));
			}
				
			//we return only the most top-k promising symbols
			return ranking.stream().limit(beamSize).map(e -> e.getKey())
					.sorted().collect(Collectors.toList());
		}
	
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import java.util.LinkedList;
import java.util.List;

import org.jkarma.mining.joiners.ProjectedDB;
import org.jkarma.mining.providers.Context;
import org.jkarma.mining.windows.WindowingStrategy;


/**
 * Selector to be used for implementing an exhaustive DFS search of patterns
 * according to the PPC condition expressed in the LCM-Max algorithm.
 * @author Angelo Impedovo
 * @param <A> Type of allowed pattern symbols.
 * @param <B> Type of pattern evaluation.
 */
public class DFSPpcMaxSelector<A extends Comparable<A>, B extends ProjectedDB<A>> extends DFSSelector<A,B> {
	
	/**
	 * The minimum frequency threshold used to evaluate the maximality.
	 */
	private double minsup;
	
	/**
	 * Constructs a selector used for implementing an exhaustive DFS search of patterns
	 * according to the PPC condition for maximality. 
	 * The condition is tested among frequent patterns by considering a minimum frequency
	 * threshold.
	 * 
	 * @param minsup The minimum frequency threshold.
	 */
	public DFSPpcMaxSelector(double minsup) {
		if(minsup<0 || minsup>1) {
			throw new IllegalArgumentException();
		}
		this.minsup = minsup;
	}
		
	@Override
	public List<A> select(Context ctx, WindowingStrategy<B> ws, SENode<A, Pair<B>> node, int height, boolean onRemote) {
		List<A> symbols = new LinkedList<>();
		
		B eval;
		if(onRemote) {
			eval = ws.getRemoteWindow(node.data.eval.getAggregate(), node.data.eval.getIncrement());
		}else {
			eval = ws.getRecentWindow(node.data.eval.getAggregate(), node.data.eval.getIncrement());
		}
		
		if(eval.isPPCMax(this.minsup)) {
			symbols = super.select(ctx, ws, node, height, onRemote);
		}else {
			//nothing to do
		}
		return symbols;
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.jkarma.model.Transaction;

/**
 * A class implementing the concept of i-conditional database, which is 
 * common in pattern mining with constraints.
 * The i-conditional database is a transactional database consisting
 * only of transactions with items j lexicographically greater than i.
 * From this point of view, the i-conditional database is the projected
 * database on the item i.
 * @author Angelo Impedovo
 *
 * @param <A> Type of allowed pattern symbols.
 */
public class ProjectedDB<A extends Comparable<A>> implements FrequencyEvaluation, ClosureEvaluation, MaximalityEvaluation{

	/**
	 * The projected vertical database as a map between items and set
	 * of transactions containing them.
	 */
	private TreeMap<Integer, Collection<A>> db;
	
	/**
	 * The number of transactions monitored by this projected database.
	 */
	protected int transactionCount;
	
	/**
	 * The itemset prefix associated to this projected database.
	 */
	public TreeSet<A> itemset;
	
	/**
	 * the item suffix associated to this projected database.
	 * the "i" on which the projection is performed, this item
	 * is also referred to as the projection key.
	 */
	public A i;
	
	
	
	/**
	 * Constructs a projected database by copy cloning another database.
	 * @param originalDB The database to be copy-cloned.
	 */
	protected ProjectedDB(ProjectedDB<A> originalDB) {
		this.transactionCount = originalDB.transactionCount;
		this.itemset = new TreeSet<>(originalDB.itemset);
		this.db = new TreeMap<>(originalDB.db);
		this.i = originalDB.i;
	}
	
	
	
	/**
	 * Constructs a projected database by cloning the content of another database.
	 * The constructed db is "projection-ready" meaning that a projection key
	 * is considered but the projection has not already been performed.
	 * @param originalDB The database to be copied.
	 * @param i The new projection key.
	 */
	protected ProjectedDB(ProjectedDB<A> originalDB, A i) {
		this.transactionCount = originalDB.transactionCount;
		this.itemset = new TreeSet<>(originalDB.itemset);
		if(originalDB.i!=null) {
			this.itemset.add(originalDB.i);
		}
		this.db = new TreeMap<>(originalDB.db);
		this.i = i;
	}

	

	/**
	 * Constructs a new projected database from a set of transactions.
	 * The projected database also monitors a given number of transactions.
	 * @param transactions The set of transactions.
	 * @param tCount The number of transactions to be monitored.
	 */
	protected ProjectedDB(Set<? extends Transaction<A>> transactions, int tCount) {
		this(tCount);

		if(transactions!=null) {
			for(Transaction<A> t : transactions) {
				int tid = t.getId();
				this.db.put(tid, t.getItems());
			}
		}
	}



	/**
	 * Constructs an empty projected database monitoring a given number of transactions.
	 * @param tCount The number of transactions to be monitored.
	 */
	public ProjectedDB(int tCount) {
		if(tCount < 0) {
			throw new IllegalArgumentException();
		}

		this.db = new TreeMap<>();
		this.itemset = new TreeSet<>();
		this.transactionCount = tCount;
		this.i = null;
	}

	
	public Set<A> getItems(){
		return this.db.values().stream()
			.flatMap(t -> t.stream()).collect(Collectors.toSet());
	}
	
	public ProjectedDB<A> negate(ProjectedDB<A> ref, Set<A> items){
		ProjectedDB<A> result = new ProjectedDB<A>(0);
		result.i = this.i;
		result.transactionCount = this.transactionCount;
		result.itemset = this.itemset;
		
		//we negate the db, we should also negate the item?
		ref.db.entrySet().stream()
		.filter(e -> !this.db.containsKey(e.getKey()))
		.forEach(e -> {
			Set<A> nCopy = items.stream()
				.filter(i -> !e.getValue().contains(i))
				.collect(Collectors.toSet());
			result.db.put(e.getKey(), nCopy);
		});
		
		return result;
	}


	
	/**
	 * Retain all the items in the vertical layout of this projected db
	 * that are also present in another projected db.
	 * @param db The projected database to be considered.
	 */
	public void retainAll(ProjectedDB<A> db){
		Set<Integer> tidset = db.db.keySet();
		this.db.keySet().retainAll(tidset);
	}



	/**
	 * Adds the vertical layout of a projected database to the layout
	 * of this database. 
	 * The two layout are semantically merged.
	 * @param database The projected database to be considered.
	 */
	public void addAll(ProjectedDB<A> database) {
		this.transactionCount += database.transactionCount;
		this.db.putAll(database.db);
	}


	
	/**
	 * Adds a new transaction to the projected database.
	 * @param t The transaction to be added.
	 */
	public void addItem(Transaction<A> t) {
		this.db.put(t.getId(), t.getItems());
	}



	/**
	 * Returns the projection key of this projected database.
	 * @return The projection key of this projected database.
	 */
	public A getProjectionKey() {
		return this.i;
	}



	/**
	 * Projects this projected database by considering a given
	 * projection key.
	 * @param i The projection key.
	 * @return The i-projected db.
	 */
	public ProjectedDB<A> project(A i) {
		return new ProjectedDB<A>(this, i);
	}



	@Override
	public int getAbsoluteFrequency() {
		return this.db.size();
	}



	@Override
	public int getTransactionCount() {
		return this.transactionCount;
	}



	@Override
	public void setTransactionCount(int t) {
		if(t<0) {
			throw new IllegalArgumentException();
		}
		this.transactionCount = t;
	}



	@Override
	public double getRelativeFrequency() {
		double result = 0;
		if(this.getTransactionCount()>0) {
			result = (double)this.getAbsoluteFrequency()/(double)this.getTransactionCount();
		}

		return result;
	}



	@Override
	public boolean isFrequent(double minSup) {
		return this.getRelativeFrequency() >= minSup;
	}



	@Override
	public boolean isClosed() {
		/*
		 * We implement a naive closure check.
		 */
		
		//itemset building
		TreeSet<A> pattern = new TreeSet<A>(this.itemset);
		if(this.i!=null) {
			pattern.add(this.i);
		}
		
		//closure
		TreeSet<A> items = new TreeSet<A>();
		boolean firstTrans = true;
		for(Collection<A> t : this.db.values()) {
			if(firstTrans) {
				items = new TreeSet<A>(t);
				firstTrans = false;
			}else {
				items.retainAll(t);
			}
		}
		//System.out.println(items+" vs "+pattern);
    	
		//return the evaluation
		return items.equals(pattern);
	}
	
	
	
	/**
	 * Returns true if this projected database is associated to a prefix preserving closed
	 * maximal (PPCMax) itemset. 
	 * The PPCMax (prefix preserving closure for maximality check) is implemented as in 
	 * "https://github.com/matfax/spmf/blob/master/src/main/java/ca/pfv/spmf/algorithms/
	 * frequentpatterns/lcm/AlgoLCMMax.java"
	 * @param minsup The minimum frequency threshold to be considered.
	 * @return True if the PPCMax condition holds, false otherwise.
	 */
	public boolean isPPCMax(double minsup) {
		/*
		 * We implement the PPC Max(prefix preserving closure for maximality check) as 
		 * in https://github.com/matfax/spmf/blob/master/src/main/java/ca/pfv/spmf/
		 * algorithms/frequentpatterns/lcm/AlgoLCMMax.java
		 */
		boolean isPPC = true;
		
		// We do a loop on each item i of the first transaction
		if(!this.db.isEmpty() && this.i!=null) {
			Entry<Integer, Collection<A>> entry = this.db.entrySet().stream().findFirst().get();
	    	Collection<A> firstTrans = entry.getValue();
	    	int tid = entry.getKey();
	    	List<A> items = firstTrans.stream()
	    		.filter(i -> i.compareTo(this.i) < 0)
	    		.collect(Collectors.toList());
	    	
	    	if(!items.isEmpty()) {
		    	for(A item : items) {
		    		// if p does not contain item i < e and item i is present in all transactions, 
		        	// then it PUe is not a ppc
		    		boolean c2 = !this.itemset.contains(item);
		    		boolean c3 = this.isItemInAtLeastMinsupTransactionsExcept(item, tid, minsup);
		    		boolean res = c2 && c3;
		    		//System.out.println("checking "+item+" in "+this.itemset+" with result ("+c2+", "+c3+") -> "+res);
		            if(res) {
		                isPPC = false;
		                break;
		            }
		        }
	    	}
		}else {
			isPPC = false;
		}
    	
		//return the evaluation
		return isPPC;
	}
	
	
	


	/**
	 * Returns true if this projected database is associated to a prefix preserving closed
	 * (PPC) itemset. 
	 * The PPC (prefix preserving closure check) is implemented as discussed in "LCM: An 
	 * Efficient Algorithm for Enumerating Frequent Closed ITem Sets" by Uno et al.
	 * @return True if the PPC condition holds, false otherwise.
	 */
	public boolean isPPC() {
		/*
		 * We implement the PPC (prefix preserving closure check) as discussed in 
		 * "LCM: An Efficient Algorithm for Enumerating Frequent Closed ITem Sets" by Uno et al.
		 * (Section 3, "checking the closedness in occurrence deliver").
		 */
		boolean isPPC = true;
		
		// We do a loop on each item i of the first transaction
		if(!this.db.isEmpty() && this.i!=null) {
			Entry<Integer, Collection<A>> entry = this.db.entrySet().stream().findFirst().get();
	    	Collection<A> firstTrans = entry.getValue();
	    	int tid = entry.getKey();
	    	List<A> items = firstTrans.stream()
	    		.filter(i -> i.compareTo(this.i) < 0)
	    		.collect(Collectors.toList());
	    	
	    	if(!items.isEmpty()) {
		    	for(A item : items) {
		    		// if p does not contain item i < e and item i is present in all transactions, 
		        	// then it PUe is not a ppc
		    		boolean c2 = !this.itemset.contains(item);
		    		boolean c3 = this.isItemInAllTransactionsExcept(item, tid);
		    		boolean res = c2 && c3;
		    		//System.out.println("checking "+item+" in "+this.itemset+" with result ("+c2+", "+c3+") -> "+res);
		            if(res) {
		                isPPC = false;
		                break;
		            }
		        }
	    	}
		}else {
			isPPC = false;
		}
    	
		//return the evaluation
		return isPPC;
	}
	
	
	private boolean isItemInAllTransactionsExcept(A item, int tid) {
		boolean contained = true;
		
		for(Entry<Integer, Collection<A>> t : this.db.entrySet()) {
			if(t.getKey() != tid) {
				if(!t.getValue().contains(item)) {
					contained = false;
					break;
				}
			}
		}
		return contained;
	}
	
	
	private boolean isItemInAtLeastMinsupTransactionsExcept(A item, int tid, double minsup) {
		boolean contained = false;
		int count = 1; //starting with 1 since already contained in transaction with tid
		
		for(Entry<Integer, Collection<A>> t : this.db.entrySet()) {
			if(t.getKey() != tid) {
				if(!t.getValue().contains(item)) {
					count++;
				
					double frq = ((double)count)/((double)this.transactionCount);
					if(frq >= minsup) {
						contained = true;
						break;
					}
				}
			}
		}
		return contained;
	}
	
	

	@Override
	public String toString() {
		return this.db.keySet().toString()+"#"+this.getAbsoluteFrequency()+"/"+this.transactionCount
				+" ("+this.getRelativeFrequency()+"), ppc:"
				+this.isPPC()+", clo:"+this.isClosed();
	}


	@Override
	public boolean isMaximal(double minSup) {
		/*
		 * We implement a naive maximality check.
		 * PUe is maximal iff for every transaction t_i the preposition
		 * t_0 intersect {e} = t_1 intersect {e} = .. = t_n intersect {e}
		 * holds.
		 */
		
		boolean isMaximal = true;
		boolean isFrequent = this.isFrequent(minSup);
		
		//histogram building for checking the frequency with lookahead
		TreeMap<A, Integer> hist = new TreeMap<A, Integer>();
		for(Collection<A> t : this.db.values()) {
			for(A item : t) {
				if(item.compareTo(i)!=0 && !itemset.contains(item)) {
					hist.merge(item, 1, (oldValue, one) -> oldValue + one);
				}
			}
		}
		
		//maximality check
		for(Entry<A,Integer> entry : hist.entrySet()) {
			Integer count = entry.getValue();
			double freq = 0;
			if(this.transactionCount>0) {
				freq = ((double)count)/((double)this.transactionCount);
			}
			if(freq>=minSup) {
				isMaximal = false;
				break;
			}
		}

		
		//return the evaluation
		return isFrequent & isMaximal;
	}


}

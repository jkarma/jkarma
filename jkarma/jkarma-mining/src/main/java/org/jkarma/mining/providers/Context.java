/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.providers;



/**
 * Simple bean containing some informations related to the mining task at hand.
 * Since evaluations cannot directly access the status of the mining process, this
 * class serves as a simple bean which is passed to the symbolic and pattern evaluations
 * by the mining strategy.
 * The class provides access to the number of consumed and cached transactions.
 * @author Angelo Impedovo
 */
public class Context {
	
	/**
	 * The number of recently consumed transactions.
	 */
	public int consumedTransactionsCount;
	
	
	
	/**
	 * The number of previously consumed (cached) transactions.
	 */
	public int cachedTransactionsCount;
	
	
	
	/**
	 * Status flag for deferred evaluation building.
	 */
	public boolean deferredEvaluation;
	
	
	
	/**
	 * Constructs an empty mining context with 0 cached and consumed
	 * transactions, in the not deferred setting.
	 */
	public Context() {
		this.consumedTransactionsCount = 0;
		this.cachedTransactionsCount = 0;
		this.deferredEvaluation = false;
	}
	
	
	
	/**
	 * Returns true in case of deferred evaluations, false otherwise.
	 * @return True in case of deferred evaluations, false otherwise.
	 */
	public boolean isDeferredEvaluation() {
		return this.deferredEvaluation;
	}
	
	
	
	/**
	 * Returns the number of recently consumed transactions by the provider.
	 * @return The number of recently consumed transactions by the provider.
	 */
	public int getConsumedCount() {
		return this.consumedTransactionsCount;
	}

	
	
	/**
	 * Returns the number of previously consumed (cached) transactions by the provider.
	 * @return The number of previously consumed (cached) transactions by the provider.
	 */
	public int getCachedCount() {
		return this.cachedTransactionsCount;
	}
}

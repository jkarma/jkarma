/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.interfaces;

import org.jkarma.mining.structures.TransactionException;

/**
 * A interface for objects that can perform transactional
 * and non-overlapping operations.
 * Each transactional operation is intended to be atomic, this
 * means that a new operation can be performed only after a commit
 * or after a rollback on the previous one. 
 * @author Angelo Impedovo
 *
 */
public interface Committable {
	
	/**
	 * Commits the last operation releasing any lock.
	 * The commit operation will mark as "permanent" any pending
	 * update.
	 * @throws TransactionException if there is no lock on the object.
	 */
	public void commit() throws TransactionException;
	
	
	/**
	 * Rollbacks the last operation releasing any lock.
	 * The rollback operation will restore the state of the object
	 * discarding any pending update.
	 * @throws TransactionException if there is no lock on the object.
	 */
	public void rollback() throws TransactionException;
	
	
	/**
	 * Return true if the object is locked and, therefore, waiting
	 * for a commit or a rollback operation. 
	 * @return True if the object is locked, false otherwise.
	 */
	public boolean isLocked();
}

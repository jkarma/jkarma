/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

import java.util.TreeSet;


/**
 * Implements the tidset associated to a pattern.
 * A tidset is the set of transaction ids (tids) covering the pattern.
 * @author Angelo Impedovo
 */
@SuppressWarnings("serial")
public class TidSet extends TreeSet<Integer> implements FrequencyEvaluation {
	
	/**
	 * The number of transactions on which the tidset is computed.
	 */
	protected int transactionCount;

	
	
	/**
	 * Constructs an empty tidset on a given number of transactions.
	 * @param tCount The number of transactions.
	 */
	public TidSet(int tCount) {
		if(tCount<0) {
			throw new IllegalArgumentException(); 
		}
		this.transactionCount = tCount;
	}
	

	
	/**
	 * Returns the absolute frequency of this tidset, that is the number
	 * of tids in the set.
	 */
	@Override
	public int getAbsoluteFrequency() {
		return this.size();
	}

	
	
	@Override
	public double getRelativeFrequency() {
		double result = 0;
		if(this.getTransactionCount()>0) {
			result = (double)this.getAbsoluteFrequency()/(double)this.getTransactionCount();
		}
		
		return result;
	}

	
	
	@Override
	public boolean isFrequent(double minSup) {
		boolean result = this.getRelativeFrequency() >= minSup;
		return result;
	}


	@Override
	public int getTransactionCount() {
		return this.transactionCount;
	}


	@Override
	public void setTransactionCount(int t) {
		if(t<0) {
			throw new IllegalArgumentException();
		}
		this.transactionCount = t;
	}
	
	@Override
	public String toString() {
		return super.toString()+"#"+this.getAbsoluteFrequency()+"/"+this.transactionCount+" ("+this.getRelativeFrequency()+")";
	}

}

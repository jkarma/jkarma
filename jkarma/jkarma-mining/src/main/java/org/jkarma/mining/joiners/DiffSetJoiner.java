/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

import org.jkarma.mining.providers.Context;


/**
 * Implements the joining algorithms for diffsets.
 * In particular diffsets are joined when refining patterns during mining
 * and they can also be reduced.
 * The only postcondition to be checked on diffsets is that they exhibits
 * at least a minimum relative frequency.
 * @author Angelo Impedovo
 */
public class DiffSetJoiner implements Aggregator<DiffSet>{
	
	/**
	 * The minimum frequency threshold to be used when evaluating the
	 * postcondition.
	 */
	private double minimumSupport;
	
	
	
	/**
	 * Constructs diffset joiner on a given minimum relative frequency
	 * threshold.
	 * @param minimumSupport The minimum frequency threshold.
	 */
	public DiffSetJoiner(double minimumSupport){
		if(minimumSupport<0 || minimumSupport>1){
			throw new IllegalArgumentException();
		}
		this.minimumSupport = minimumSupport;
	}	

	

	/**
	 * Joins two diffsets t and u associated to two patterns having same heights.
	 * The join operation computes a new diffset d(PX) which is equal to:
	 * <ul>
	 * 	<li>d(PX) = t(P) - t(X), set-difference between the tidsets of P and X</li>
	 * 	<li>d(PXY) = d(PY) - d(PX), set difference between the diffsets of PY and PX</li>
	 * </ul>
	 * The joined diffset acts on the same number of transactions.
	 */
	@Override
	public DiffSet apply(DiffSet t, DiffSet u, int height) {
		if(t==null || u==null) {
			throw new NullPointerException();
		}else {
			if(t.getTransactionCount()>0 && u.getTransactionCount()>0) {
				if(t.getTransactionCount()!=u.getTransactionCount()) {
					throw new IllegalArgumentException("joining diffset with different transaction counts");
				}
			}
		}
		
		
		//we compute the new diffset by following the paper by Zaki et al.
		int abs = t.getAbsoluteFrequency();
		DiffSet diff = new DiffSet(0, abs);
		if(t.tCount>0 && u.tCount>0) {
			if(height==2){
				/*
				 * two 1-itemsets join into a 2-itemset,
				 * then d(PX) = t(P)-t(X) where d(PX) is the diffset of
				 * PX and t(X) is the diffset of X.
				 */ 
				diff.addAll(t);
				diff.removeAll(u);
			}else{
				/*
				 * two n-itemsets join into a (n+1)-itemset,
				 * then d(PXY) = d(PY)-d(PX) where d(PX) is the diffset of PX.
				 */
				diff.addAll(u);
				diff.removeAll(t);
			}
			diff.tCount = t.tCount;
		}
		return diff;
	}



	/**
	 * Merges two diffsets t and u in a new diffset which is equal to the
	 * set-union of t and u.
	 * The merged diffset acts on the sum of the transaction's number of t and u.
	 */
	@Override
	public DiffSet reduce(DiffSet t, DiffSet u) {
		if(t==null || u==null) {
			throw new NullPointerException();
		}
		
		DiffSet diff = new DiffSet(t.tCount+u.tCount, t.pFreq+u.pFreq);
		diff.addAll(t);
		diff.addAll(u);
		return diff;
	}

	
	
	/**
	 * Always returns true since no pre-conditions on tidsets are evaluated. 
	 */
	@Override
	public boolean testPrecondition(DiffSet first, DiffSet second, Context ctx, int height) {
		return true;
	}

	
	
	/**
	 * Returns true if the relative frequency exceeds the minimum frequency threshold,
	 * false otherwise.
	 */
	@Override
	public boolean testPostcondition(DiffSet diffset, Context ctx, int height) {
		double support = diffset.getRelativeFrequency();
		return support >= this.minimumSupport;
	}



	/**
	 * Returns the minimum frequency threshold currently associated to the joiner.
	 * @return The minimum frequency threshold currently associated to the joiner.
	 */
	public double getMinimumFrequency() {
		return this.minimumSupport;
	}



	/**
	 * Associates a minimum frequency threshold currently to the joiner.
	 * @param minFrequency The minimum frequency threshold.
	 */
	public void setMinimumFrequency(float minFrequency) {
		if(minFrequency<0 || minFrequency>1) {
			throw new IllegalArgumentException();
		}
		this.minimumSupport = minFrequency;
	}
}

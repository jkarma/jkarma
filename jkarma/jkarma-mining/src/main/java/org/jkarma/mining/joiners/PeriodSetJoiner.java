/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

import org.jkarma.mining.providers.Context;


/**
 * Implements the joining algorithms for PeriodSet objectss.
 * In particular PeriodSet objects are joined when refining patterns during 
 * mining and they can also be reduced.
 * The only postcondition to be checked on PeriodSet is that they satisfy some
 * periodicity conditions.
 * @author Angelo Impedovo
 */
public class PeriodSetJoiner implements Aggregator<PeriodSet>{
	
	/**
	 * The periodicity lower bound.
	 */
	private int minPeriodicity;
	
	
	
	/**
	 * The periodicity upper bound.
	 */
	private int maxPeriodicity;
	
	
	
	/**
	 * The average periodicity lower bound.
	 */
	private double minAvgPeriodicity;
	
	
	
	/**
	 * The average periodicity upper bound.
	 */
	private double maxAvgPeriodicity;
	
	
	
	/**
	 * Constructs a PeriodSet joiner on a given periodicity bounds.
	 * @param minPer The periodicity lower bound.
	 * @param maxPer The periodicity upper bound.
	 * @param minAvgPer The average periodicity lower bound.
	 * @param maxAvgPer The average periodicity upper bound.
	 */
	public PeriodSetJoiner(int minPer, int maxPer, double minAvgPer, double maxAvgPer){
		if(minPer<0 || maxPer <0 || maxPer<minPer){
			throw new IllegalArgumentException();
		}else if(minAvgPer<0 || maxAvgPer<0 || maxAvgPer<minAvgPer) {
			throw new IllegalArgumentException();
		}
		this.minPeriodicity = minPer;
		this.maxPeriodicity = maxPer;
		this.minAvgPeriodicity = minAvgPer;
		this.maxAvgPeriodicity = maxAvgPer;
	}	

	
	
	/**
	 * Joins two PeriodSet objects t and u associated to two patterns having same heights.
	 * The join operation computes a new PeriodSet which is equal to the set-intersection
	 * of t and u (since t and u are backed by tidsets).
	 * The joined PeriodSet acts on the same number of transactions and starts at the
	 * minimum start index of t and u.
	 */
	@Override
	public PeriodSet apply(PeriodSet t, PeriodSet u, int height) {
		if(t==null || u==null) {
			throw new NullPointerException();
		}else {
			if(t.getTransactionCount()>0 && u.getTransactionCount()>0) {
				if(u.getTransactionCount()!=u.getTransactionCount()) {
					throw new IllegalArgumentException("trying to tidset on different number of transactions");
				}
			}
		}
		
		int start = Math.min(t.startIndex, u.startIndex);
		PeriodSet result = new PeriodSet(start, 0);
		if(t.getTransactionCount()>0 && u.getTransactionCount()>0) {
			result.transactionCount = t.transactionCount;
			result.addAll(t);
			result.retainAll(u);
		}
		
		return result;
	}


	
	/**
	 * Merges two PeriodSet objects t and u in a new PeriodSet which is equal to the
	 * set-union of t and u (since t and u are backed by tidsets).
	 * The merged PeriodSet acts on the sum of the transaction's number of t and u and
	 * starts at the minimum start index of t and .
	 */
	@Override
	public PeriodSet reduce(PeriodSet t, PeriodSet u) {
		if(t==null || u==null) {
			throw new NullPointerException();
		}
		
		PeriodSet result = new PeriodSet(
			Math.min(t.startIndex, u.startIndex),
			t.getTransactionCount()+u.getTransactionCount());
		result.addAll(t);
		result.addAll(u);
		
		return result;
	}

	
	
	/**
	 * Always returns true since no pre-conditions on tidsets are evaluated. 
	 */
	@Override
	public boolean testPrecondition(PeriodSet first, PeriodSet second, Context ctx, int height) {
		return true;
	}

	
	
	/**
	 * Returns true if the maximum periodicity do not exceeds the upper bound and the average
	 * periodicity exceeds a lambda value.
	 * The refinement post-condition is the same used in the PFPM algorithm.
	 * 
	 * @see <a href="https://doi.org/10.5772/66780">Proceedings of the 2nd Czech-China 
	 * Scientific Conference 2016, 2017, PFPM: Discovering Periodic Frequent Patterns 
	 * with Novel Periodicity Measures</a>
	 */
	@Override
	public boolean testPostcondition(PeriodSet tidlist, Context ctx, int height) {
		double lambda = (tidlist.getTransactionCount()/this.maxAvgPeriodicity)-1;
		boolean avgPerConstraint = tidlist.getAveragePeriodicity() >= lambda;
		boolean maxPerConstraint = tidlist.getMaximumPeriodicity() <= this.maxPeriodicity;
		return avgPerConstraint && maxPerConstraint;
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

import org.jkarma.mining.providers.Context;


/**
 * Implements the simplest pattern language of itemsets.
 * The LexicographicJoiner is a Joiner of Comparable objects, this is a necessary
 * condition for arranging itemsets into a SETree data structure.
 * Given an alphabet of symbols of type A, an itemsets is a word on the alphabet
 * in which the symbols are sorted via a lexicographic order.
 * Every word is legal, that is no precondition or postcondition is required.
 * The joining mechanism is the following: when joining two word P1=Pt and P2=Pu,
 * sharing the same prefix P, to form P3=Ptu, the lexicographically greatest symbol
 * between t and u is chosen.
 * @author Angelo Impedovo
 *
 * @param <A> Type of allowed symbols
 */
public class LexicographicJoiner<A extends Comparable<A>> implements Joiner<A> {

	
	/**
	 * Selects the greatest elements between t and u when joining
	 * two itemsets P1=Pt and P2=Pu to form P3=Ptu.
	 * Clearly, P1 and P2 shares the same prefix P and therefore
	 * have same length. 
	 */
	@Override
	public A apply(A t, A u, int height) {
		return u;
	}


	
	/**
	 * Always returns true since no post-conditions on itemsets are evaluated. 
	 */
	@Override
	public boolean testPrecondition(A first, A second, Context ctx, int height) {
		return true;
	}

	
	
	/**
	 * Always returns true since no post-conditions on itemsets are evaluated.
	 */
	@Override
	public boolean testPostcondition(A result, Context ctx, int heigth) {
		return true;
	}

}

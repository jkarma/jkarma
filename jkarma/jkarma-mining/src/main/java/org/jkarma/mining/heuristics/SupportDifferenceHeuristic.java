package org.jkarma.mining.heuristics;

import org.jkarma.mining.joiners.FrequencyEvaluation;
import org.jkarma.mining.providers.Context;


/**
 * This class implements a pattern heuristic evaluation based on the pattern's support
 * difference (risk difference, absolute growth-rate, etc).
 * The support difference of a pattern is a contrast measure defined as the subtraction 
 * between the support on the first window and the support on the second one.
 * Necessary condition to compute the absolute frequency is that B implements the
 * FrequencyEvaluation interface.
 * The standard support difference is computed by subtracting the lowest support support 
 * on the two window to the greatest. The symmetric version simply subtracts, in absolute
 * value, the greatest support to the lowest.
 * @author Angelo Impedovo
 *
 * @param <A> Type denoting the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 * @see <a href='https://dblp.org/rec/bib/books/crc/dong13/Bailey13'>
 * Statistical Measures for Contrast Patterns, Contrast Data Mining: 
 * Concepts, Algorithms, and Applications</a>
 */
public class SupportDifferenceHeuristic<A,B extends FrequencyEvaluation> implements Heuristic<A,B>{
	
	/**
	 * If true the heuristic will compute the symmetric support difference.
	 * Otherwise the standard difference will be computed.
	 */
	boolean isSymmetric = false;
	
	
	
	/**
	 * Instantiate a support difference heuristic.
	 * If the symmetric flag is set to true, then the symmetric support difference 
	 * will be computed. Otherwise, the symmetric version will be computed.
	 * @param symmetric The reciprocal flag.
	 */
	public SupportDifferenceHeuristic(boolean symmetric) {
		this.isSymmetric = symmetric;
	}

	
	
	@Override
	public double apply(A suffix, B remoteEval, B recentEval, Context ctx, int itemsetLength, boolean onRemote) {
		long oldFreq = remoteEval.getAbsoluteFrequency();
		long newFreq = recentEval.getAbsoluteFrequency();
		
		double value;
		if(!isSymmetric) {
			value = Math.max(newFreq, oldFreq) - Math.min(newFreq, oldFreq);
		}else {
			value = Math.abs(Math.min(newFreq, oldFreq) - Math.max(newFreq,  oldFreq));
		}
		
		return value;
	}
}
/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import java.util.Set;

import org.jkarma.mining.interfaces.ItemSet;
import org.jkarma.mining.interfaces.Lattice;
import org.jkarma.mining.joiners.Aggregator;
import org.jkarma.mining.joiners.Joiner;
import org.jkarma.mining.providers.BaseProvider;
import org.jkarma.mining.windows.WindowingStrategy;


/**
 * Implements an incremental mining strategy which is able to
 * continuously update the set of discovered patterns with subsequent calls
 * to the execute method by using time window models.
 * Moreover the mining strategy is able to change the set of allowed items 
 * (the generators) between two subsequent executions.
 * @author Angelo Impedovo
 * @param <A> Type of allowed pattern symbols.
 * @param <B> Type of pattern evaluation.
 */
public abstract class MiningStrategy<A extends Comparable<A>,B> extends Strategy<A,Pair<B>>{
	
	/**
	 * Status flag indicating whether this strategy is locked by the execute method.
	 */
	private boolean locked;
	
	
	/**
	 * The delegate responsibile of evaluating patterns and aggregating two evaluations.
	 */
	protected Aggregator<B> evaluator;
	
	
	
	/**
	 * The data accessor delegate responsible of building the evaluation for 1-itemsets
	 * from what observed on data.
	 */
	protected BaseProvider<A,B> provider;
	
	
	
	/**
	 * Inner pointer to the lattice of patterns.
	 * This mining strategy is incremental, meaning that two subsequent calls to the
	 * execute method should update the same Lattice.
	 */
	protected SETree<A,Pair<B>> lattice;
	
	
	
	/**
	 * The time window model adopted by this mining strategy.
	 */
	protected WindowingStrategy<B> windowing;
	
	
	
	/**
	 * 
	 * @param generators The items (pattern symbols) considered in this mining strategy.
	 * @param terminologicalExpander The joiner of allowed pattern symbols.
	 * @param supportProvider The provider of pattern evaluations.
	 * @param supportJoiner The joiner of pattern evaluations.
	 * @param maxDepth The maximum length of patterns.
	 */
	public MiningStrategy(Set<A> generators, Joiner<A> terminologicalExpander, 
			BaseProvider<A,B> supportProvider, Aggregator<B> supportJoiner, int maxDepth) {
		super(generators, terminologicalExpander, maxDepth);
		this.evaluator = supportJoiner;
		this.provider = supportProvider;
		this.windowing = provider.getWindowModel();
		this.windowing.setAggregator(this.evaluator);
		this.locked = false;
	}
	
	
	
	/**
	 * Adds a new item to be considered in the mining process.
	 * @param generator The item to be considered in the mining process.
	 */
	public void setGenerator(A generator) {
		this.generators.add(generator);
	}
	
	
	
	/**
	 * Checks whether a generator is already considered in the mining process.
	 * @param generator The generator to be evaluated
	 * @return True if generator is already considered, False otherwise.
	 */
	public boolean hasGenerator(A generator) {
		return this.generators.contains(generator);
	}
	
	
	
	/**
	 * Removes an item from those considered in the mining process.
	 * @param generator The item to be removed.
	 */
	public void unsetGenerator(A generator) {
		this.generators.remove(generator);
	}
	
	
	
	/**
	 * Returns the pattern evaluator delegate used by the mining strategy.
	 * @return The pattern evaluator delegate used by the mining strategy.
	 */
	public Aggregator<B> getEvaluator(){
		return (Aggregator<B>)this.evaluator;
	}
	
	
	
	/**
	 * Returns the time window model used by the mining strategy.
	 * @return The time window model used by the mining strategy.
	 */
	public WindowingStrategy<B> getWindowing(){
		return this.windowing;
	}
	
	
	
	/**
	 * Return the data accessor acting as 1-itemsets evaluation provider
	 * used by the mining strategy.
	 * @return The data accessor used by the mining strategy.
	 */
	public BaseProvider<A,B> getEvaluationProvider(){
		return this.provider;
	}
	
	
	
	/**
	 * Checks whether the mining strategy is locked by the execute method.
	 * @return True if the mining strategy is locked by the executed method.
	 */
	@Override
	public boolean isLocked() {
		return this.locked;
	}
	
	
	
	/**
	 * Set the mining strategy as locked.
	 * @param locked Boolean flag indicating whether the mining strategy is locked.
	 */
	protected void setLocked(boolean locked) {
		this.locked = locked;
	}
	
	
	
	/**
	 * Abstract method implementing an incremental pattern mining strategy using time window models.
	 * Since the mining strategy adopts a learning algorithm based on time window models, this means
	 * that every pattern should be evaluated according to previously observed transactions and 
	 * new incoming transactions.
	 * For this reason every pattern is associated to a Pair of evaluations of type B.
	 * Every call to this method is responsible of updating the set of patterns in the lattice 
	 * according to the set of new data observed.
	 * Moreover it should be taken into account that what's performed by the method is temporary,
	 * meaning that the modifications can be undone by the rollback method, or confirmed by commit
	 * method.
	 * Ultimately, it may be useful to drop previously observed transactions (forget method) to release
	 * some of the occupied memory.
	 * This is possible by acting (merging or dropping) on the pair of evaluations associated to every
	 * pattern.
	 * @return The updated lattice of discovered patterns.
	 * @throws TransactionException if the object is already locked.
	 */
	public abstract Lattice<ItemSet<A, Pair<B>>> execute() throws TransactionException;
	
	
	
	/**
	 * Abstract method specifying the update logic behind the drop of previously observed
	 * transactions.
	 * In particular this method drops the past evaluation of every pattern and updated the set
	 * of discovered patterns accordingly.
	 * @return The updated lattice of discovered patterns.
	 * @throws TransactionException if the object is already locked.
	 */
	public abstract Lattice<ItemSet<A, Pair<B>>> forget() throws TransactionException;
	
}

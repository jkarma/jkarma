/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.jkarma.mining.interfaces.ItemSet;
import org.jkarma.mining.interfaces.Lattice;
import org.jkarma.mining.interfaces.Tree;


/**
 * Class for constructing and manipulating set enumeration trees (SETree).
 * A SETree is an imbalanced tree which arrange the nodes according to the
 * lexicographic order of the content, in this is a specific case of a prefix tree.
 * The SETree is a tree of nodes of a specific type and is used to implement the
 * Lattice interface. Any element of the set enumeration tree is a word of symbols
 * (of type A) that can be evaluated (the evaluation is of type B).
 * @author Angelo Impedovo
 * 
 * @param <A> The type of symbols allowed in nodes.
 * @param <B> The type of node evaluations.
 */
class SETree<A extends Comparable<A>,B> implements Tree<Entry<A,B>>, Lattice<ItemSet<A,B>> {
	
	/**
	 * The root node of this SETree.
	 */
	protected SENode<A,B> root;
	
	
	/**
	 * The number of nodes in the SETree.
	 */
	protected int nodeCount;
	
	
	/**
	 * Returns an iterator over the elements of the SETree.
	 * The iterator traverses the tree in a DFS fashion.
	 */
	@Override
	public Iterator<ItemSet<A,B>> iterator() {
		return new SEIterator<A,B>(this.root, false);
	}
	
	
	/**
	 * Returns an iterator over ALL the elements of the SETree.
	 * The iterator traverses the tree in a DFS fashion.
	 */
	public Iterator<ItemSet<A,B>> completeIterator() {
		return new SEIterator<A,B>(this.root, true);
	}

	
	
	/**
	 * Returns the number of nodes in the SETree.
	 */
	@Override
	public int getCount() {
		return this.nodeCount;
	}

	
	
	/**
	 * Returns all the elements of the lattice which are greater than a given element
	 * according to the order-relationship.
	 */
	@Override
	public Collection<ItemSet<A,B>> getGreaterElements(ItemSet<A,B> element) {
		LinkedList<ItemSet<A,B>> set = new LinkedList<ItemSet<A,B>>();
		SENode<A,B> currentNode = (SENode<A,B>)element;
		ArrayList<A> suffixQueue = new ArrayList<A>();
		
		//troviamo gli elementi da permutare
		while(currentNode.getSuffix()!=null){
			suffixQueue.add(currentNode.getSuffix());
			currentNode = currentNode.getParent();
		}
		
		for(int i=suffixQueue.size()-1; i>=0; i--){
			//costruisco la permutazione
			LinkedList<A> permutation = new LinkedList<A>();
			for(int j=suffixQueue.size()-1; j>=0; j--){
				if(i!=j){
					permutation.add(suffixQueue.get(j));
				}
			}
			
			//cerco la permutazione in profondità
			SENode<A,B> searchNode = currentNode;
			for(A item : permutation){
				searchNode = searchNode.getChildren().get(item);
				if(searchNode==null){
					break;
				}
			}
			
			//aggiungo il risultato
			if(searchNode!=null){
				set.add(searchNode);
			}
		}
		
		return set;
	}

	
	
	/**
	 * Returns all the elements of the lattice which are lower than a given element
	 * according to the order-relationship.
	 */
	@Override
	public Collection<ItemSet<A,B>> getLowerElements(ItemSet<A,B> element) {
		LinkedList<ItemSet<A,B>> set = new LinkedList<ItemSet<A,B>>();
		SENode<A,B> currentNode = (SENode<A,B>)element;
		SENode<A,B> lastNode = (SENode<A,B>)element;
		LinkedList<A> suffixQueue = new LinkedList<A>();
		
		//troviamo gli elementi da permutare
		while(currentNode.getSuffix()!=null){
			suffixQueue.push(currentNode.getSuffix());
			lastNode = currentNode;
			currentNode = currentNode.getParent();
		}
		
		if(lastNode.getParent()==null){
			lastNode = lastNode.getFirstChild();
		}else{
			lastNode = lastNode.getParent().getFirstChild();
		}
		
		//incede nell'analisi partendo dal livello pi� alto
		while(true){
			if(!suffixQueue.isEmpty()){
				A firstItem = suffixQueue.peek();
				
				while(lastNode.getSuffix().equals(firstItem)==false){
					//cerco il prefisso
					SENode<A,B> searchNode = lastNode;
					for(A item : suffixQueue){
						searchNode = searchNode.getChildren().get(item);
						if(searchNode==null){
							break;
						}
					}
					
					if(searchNode!=null){
						set.add(searchNode);
					}
					
					lastNode = lastNode.getNextSibling();
				}
				
				suffixQueue.poll();
				if(lastNode.children.count()>0){
					lastNode = lastNode.children.getFirstChild();
				}else{
					break;
				}
			}else{
				//quando ho esaurito lo stack significa che
				//ho trovato l'itemset passato come parametro, quindi
				//ne aggiungo i figli (che sono itemset lunghi k+1)
				while(lastNode!=null){
					set.add(lastNode);
					lastNode=lastNode.getNextSibling();
				}
				break;
			}
		}		
		
		return set;
	}

	
	
	/**
	 * Returns the join between two given elements of this lattice,
	 * according to the order-relationship.
	 */
	@Override
	public ItemSet<A,B> getJoin(ItemSet<A,B> firstElement, ItemSet<A,B> secondElement) {
		throw new UnsupportedOperationException();
	}

	
	
	/**
	 * Returns the meet between two given elements of this lattice,
	 * according to the order-relationship.
	 */
	@Override
	public ItemSet<A,B> getMeet(ItemSet<A,B> firstElement, ItemSet<A,B> secondElement) {
		throw new UnsupportedOperationException();
	}

	
	
	/**
	 * Return the minimun element (inferum) of this lattice.
	 */
	@Override
	public ItemSet<A,B> getMinimum() {
		return this.root;
	}

	
	
	/**
	 * Return the maximum element (supremum) of this lattice.
	 */
	@Override
	public ItemSet<A,B> getMaximum() {
		throw new UnsupportedOperationException();
	}

	
	
	/**
	 * Return whether or not the lattice is limited.
	 * A lattice is limited if has both a minimum and a maximum.
	 */
	@Override
	public boolean isLimited() {
		ItemSet<A,B> inferum = this.getMinimum();
		ItemSet<A,B> supremum = this.getMaximum();
		boolean isLimited = inferum!=null && supremum!=null;
		
		return isLimited;
	}

	
	
	/**
	 * Returns the root node of the SETree.
	 */
	@Override
	public SENode<A,B> getRoot() {
		return this.root;
	}



	@Override
	public Stream<ItemSet<A, B>> stream() {
		return this.root.stream();
	}
	
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.windows;

/**
 * This class serves as a base class for a cumulative sliding
 * windowing strategy based on the Sliding time window model.
 * In the cumulative sliding window model, the window slides
 * forward with the incoming block without dropping the last 
 * block of collected transactions.
 * @author Angelo Impedovo
 * 
 * @param <A> The type of pattern evaluations.
 */
public class CumulativeSlidingStrategy<A> extends AbstractSlidingStrategy<A> {

	
	/**
	 * Chose or create an evaluation for the transactions which will be
	 * considered as "new remote transactions".
	 * The evaluation is built upon a time window based on w1, w2, and the
	 * last selected recent window.
	 * In the cumulative sliding window model the reduction of w1 and w2 is kept.
	 * @param w1 The evaluation built on the previously observed transactions.
	 * @param w2 The evaluation built on the last block of transactions.
	 * @return Returns the reduction of w1 and w2.
	 */
	@Override
	public A andThen(A w1, A w2) {
		return this.getAggregator().reduce(w1, w2);
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import org.jkarma.mining.events.ItemSetEventListener;
import org.jkarma.mining.interfaces.Committable;
import org.jkarma.mining.interfaces.ItemSet;
import org.jkarma.mining.interfaces.Lattice;
import org.jkarma.mining.joiners.Joiner;

import com.google.common.eventbus.EventBus;

/**
 * Abstract class implementing a mining strategy.
 * The mining strategy implements a process to be executed for mining
 * patterns from data, according to a specific pattern language and
 * pattern evaluation criteria, producing the Lattice of discovered 
 * patterns as the output of the whole process.
 * This class acts as a convenience abstraction layer for more specific
 * mining strategy (e.g. incremental mining processes or more sophisticated
 * procedures).
 * Since the pattern mining process can be seen as a constrained search
 * of legal patterns in the search space of the Lattice of pattern ordered
 * by generality, the search has to obey some constraints on the language
 * of patterns or over the pattern evaluation.
 * The implementation is left unspecified since many traversal strategies
 * can be specified for the same purpose.
 * The class implements the Committable interface to prevent parallel or multiple
 * call to the execute method (whose execution can be reverted or committed). 
 * 
 * @author Angelo Impedovo
 *
 * @param <A> Type of allowed pattern symbols
 * @param <B> Type of pattern evaluation
 */
public abstract class Strategy<A,B> implements Committable {
	
	/**
	 * The set of allowed items, alphabet of pattern symbols.
	 */
	protected Set<A> generators;
	
	
	/**
	 * Delegate for lexicographic join of items implementing the grammar
	 * of the pattern language.
	 */
	protected Joiner<A> expander;
	
	
	/**
	 * Event dispatcher for pattern mining related events.
	 */
	protected EventBus eventBus;
	
	
	/**
	 * The max length (aka number of items) of discovered patterns.
	 */
	private int maxDepth;
	
	
	
	/**
	 * Builds an strategy by specifying the lexicographic delegate and
	 * the maximum length of patterns to be discovered.
	 * The strategy is initially based on an empty set of items.
	 * @param terminologicalExpander The lexicographic joiner delegate.
	 * @param maxDepth The maximum length of patterns to be discovered.
	 */
	public Strategy(Joiner<A> terminologicalExpander, int maxDepth) {
		this(Collections.emptySet(), terminologicalExpander, maxDepth);
	}
	
	
	/**
	 * Builds an strategy by specifying the set of items, the lexicographic 
	 * delegate and the maximum length of patterns to be discovered.
	 * @param generators The set of allowed items.
	 * @param terminologicalExpander The lexicographic joiner delegate.
	 * @param maxDepth The maximum length of patterns to be discovered.
	 */
	public Strategy(Set<A> generators, Joiner<A> terminologicalExpander, int maxDepth){
		this.generators = new TreeSet<>(generators);
		this.expander = terminologicalExpander;
		this.maxDepth = maxDepth;
		this.eventBus = new EventBus();
	}
	
	
	
	/**
	 * Returns the maximum length of patterns to be discovered.
	 * @return The maximum length of patterns to be discovered.
	 */
	public int getMaxDepth() {
		return this.maxDepth;
	}
	
	
	
	/**
	 * Returns the set of items to be considered when mining patterns.
	 * @return The set of items to be considered when mining patterns.
	 */
	public Set<A> getGenerators(){
		return this.generators;
	}
	
	
	
	/**
	 * Returns the lexicographic joiner delegate to be used when mining patterns.
	 * @return The lexicographic joiner delegate to be used when mining patterns.
	 */
	public Joiner<A> getExpander(){
		return this.expander;
	}
	
	
	
	/**
	 * Register an event listener which listens for pattern mining related events
	 * during the mining process.
	 * @param eventListener The event listener.
	 */
	public void registerListener(ItemSetEventListener<A,B> eventListener) {
		this.eventBus.register(eventListener);
	}
	
	
	
	/**
	 * Unregister an event listener which listens for pattern mining related events
	 * during the mining process.
	 * @param eventListener The event listener.
	 */
	public void unregisterListener(ItemSetEventListener<A,B> eventListener) {
		this.eventBus.unregister(eventListener);
	}
	
	
	
	/**
	 * Abstract method implementing the mining strategy process.
	 * The process should search, evaluate and test patterns in order to
	 * return only valid patterns over the data.
	 * For this reason the method should return the Lattice of discovered
	 * patterns ordered by generality.
	 * @return The Lattice of discovered patterns ordered by generality.
	 * @throws TransactionException if the object is already locked.
	 */
	public abstract Lattice<ItemSet<A,B>> execute() throws TransactionException;
	
}

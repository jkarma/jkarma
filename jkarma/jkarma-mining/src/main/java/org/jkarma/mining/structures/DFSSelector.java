/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.jkarma.mining.providers.Context;
import org.jkarma.mining.windows.WindowingStrategy;

/**
 * Selector to be used for implementing an exhaustive DFS search of patterns.
 * @author Angelo Impedovo
 * @param <A> Type of allowed pattern symbols.
 * @param <B> Type of pattern evaluation.
 */
public class DFSSelector<A extends Comparable<A>,B> implements Selector<A,B> {

	@Override
	public List<A> select(Context ctx, WindowingStrategy<B> ws, SETree<A,Pair<B>> lattice, Set<A> items, boolean onRemote) {
		TreeSet<A> symbols = new TreeSet<>();
		SENode<A,Pair<B>> node = lattice.getRoot();
		
		//since we are expanding the root node, them we add all 
		//the generators and all the first level items.
		symbols.addAll(node.children.nodes.keySet());
		symbols.addAll(items);		
		return new LinkedList<A>(symbols);
	}

	@Override
	public List<A> select(Context ctx, WindowingStrategy<B> ws, SENode<A, Pair<B>> node, int height, boolean onRemote) {
		LinkedList<A> symbols;
		
		//we iterate over the siblings
		symbols = new LinkedList<>();
		SENode<A,Pair<B>> sibling = node.getNextSibling();
			
		//so we can add their respective content as is
		while(sibling!=null) {
			symbols.addFirst(sibling.data.suffix);
			sibling = sibling.getNextSibling();
		}
		
		//we also add discarded nodes
		symbols.addAll(node.parent.discardedChildren.nodes.keySet());
		return symbols;
	}

}

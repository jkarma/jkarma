/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import java.util.Iterator;
import java.util.LinkedList;

import org.jkarma.mining.interfaces.ItemSet;


/**
 * An iterator over the itemsets stored in a SETree data structure.
 * The iterator can be a complete iterator, meaning that it can iterate
 * also over discarded elements according to the lexicographic constraints,
 * or not.
 * @author Angelo Impedovo
 *
 * @param <A> Type of allowed pattern symbols.
 * @param <B> Type of pattern evaluation.
 */
class SEIterator<A,B> implements Iterator<ItemSet<A,B>> {
	
	private SENode<A,B> last;
	private SENode<A,B> root;
	private LinkedList<SENode<A,B>> nodes;
	private boolean complete;
	
	/**
	 * Builds an iterator for the elements belonging to the SETree
	 * rooted in a given node.
	 * @param root The root of the SETree on which to iterate
	 * @param complete Flag indicating whether the iterator is complete
	 *  or not.
	 */
	protected SEIterator(SENode<A,B> root, boolean complete) {
		if(root==null) {
			throw new IllegalArgumentException();
		}else {
			this.last = null;
			this.root = root;
			this.complete = complete;
			this.nodes = new LinkedList<>();
			this.nodes.add(this.root);
		}
	}

	
	
	@Override
	public boolean hasNext() {
		return !this.nodes.isEmpty();
	}

	
	
	@Override
	public ItemSet<A,B> next() {
		SENode<A,B> node = this.nodes.pop();
		this.last = node;
		if(this.complete) {
			//itera su tutto
			for(SENode<A, B> child : node.children) {
				this.nodes.add(child);
			}
			
			for(SENode<A, B> child : node.discardedChildren) {
				this.nodes.add(child);
			}
		}else {
			//non itera sui nodi massimali
			for(SENode<A, B> child : node.children) {
				//if(child.status == SENodeStatus.UPDATED) {
					this.nodes.add(child);
				//}
			}
		}
		

		return node;
	}
	
	

	@Override
	public void remove() {
		if(this.last==null) {
			throw new IllegalStateException();
		}else {
			SENode<A,B> nodeToRemove = this.last;
			nodeToRemove.parent.removeChild(nodeToRemove);
			if(this.complete) {
				if(nodeToRemove.parent!=null) {
					nodeToRemove.parent.removeDiscardedChild(nodeToRemove);
				}
			}
			
			if(!nodes.isEmpty()) {
				while(nodes.getFirst().parent==nodeToRemove) {
					nodes.pop();
				}
			}
			this.last = null;
		}
	}

}

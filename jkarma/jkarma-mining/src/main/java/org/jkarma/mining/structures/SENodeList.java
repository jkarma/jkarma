/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import java.util.Iterator;
import java.util.Map.Entry;

import org.jkarma.mining.interfaces.NodeList;

import java.util.TreeMap;


class SENodeList<A,B> implements NodeList<SENode<A,B>>{
	
	protected TreeMap<A, SENode<A,B>> nodes;
	
	public SENodeList() {
		this.nodes = new TreeMap<>();
	}
	
	public SENode<A,B> getFirstChild(){
		SENode<A,B> child = null;
		if(!this.nodes.isEmpty()) {
			child = this.nodes.firstEntry().getValue();
		}
		return child;
	}
	
	public SENode<A,B> getLastChild(){
		SENode<A,B> child = null;
		if(!this.nodes.isEmpty()) {
			child = this.nodes.lastEntry().getValue();
		}
		return child;
	}
	
	
	public SENode<A,B> getNextSibling(SENode<A,B> node){
		SENode<A,B> sibling = null;
		Entry<A, SENode<A,B>> entry = this.nodes.higherEntry(node.getKey());
		if(entry!=null) {
			sibling = entry.getValue();
		}
		return sibling;
		
	}
	
	public SENode<A,B> getPreviousSibling(SENode<A,B> node){
		SENode<A,B> sibling = null;
		Entry<A, SENode<A,B>> entry = this.nodes.lowerEntry(node.getKey());
		if(entry!=null) {
			sibling = entry.getValue();
		}
		return sibling;
	}
	
	public void remove(SENode<A, B> child) {
		if(child==null) {
			throw new NullPointerException();
		}

		this.nodes.remove(child.getSuffix());
	}


	public void add(SENode<A, B> child) {
		if(child==null) {
			throw new NullPointerException();
		}
		
		this.nodes.put(child.getSuffix(), child);
	}
	
	
	public SENode<A,B> get(A key){
		return this.nodes.get(key);
	}
	

	@Override
	public Iterator<SENode<A, B>> iterator() {
		return this.nodes.values().iterator();
	}

	@Override
	public int count() {
		return this.nodes.size();
	}
	

}

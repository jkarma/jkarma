/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

import org.jkarma.mining.providers.Context;
import org.jkarma.model.LabeledEdge;

/**
 * Implements the subtrees language.
 * The SubtreeEdgeLexicographicJoiner is a Joiner of (Comparable) LabeledEdge objects, 
 * this is a necessary condition for arranging subgraphs into a SETree data structure.
 * Given an alphabet of labeled edges, a subtree is a word on the alphabet in which 
 * the edges are sorted via a lexicographic order.
 * Moreover in order for subtrees to be legal, they should be connected and acyclic 
 * (there exists a path connecting every two nodes in the sutree but not a cycle).
 * The joining mechanism is the following: when joining two subtrees G1=Gt and G2=Pu,
 * sharing the same prefix G, to form G3=Gtu, the lexicographically greatest edge
 * between t and u is chosen.
 * 
 * The connectivity status of G3 is computed during joining in order to be evaluated
 * as post-condition. In particular, when joining G1 and G2, G3 is a subtree only when:
 * <ul>
 * 	<li>(G1 is connected AND G2 is connected) AND height&gt;1 AND t linked u</li>
 * 	<li>(G1 is connected AND G2 is connected) AND height&lt;=1</li>
 * 	<li>(G1 is not connected OR G2 is not connected) AND t linked u</li>
 * </ul>
 * @author Angelo Impedovo
 */
public class SubtreeEdgeLexicographicJoiner implements Joiner<LabeledEdge> {

	
	/**
	 * When joining G1=Gt and G2=Gu, returns true if both t and u are not self loops.
	 */
	@Override
	public boolean testPrecondition(LabeledEdge first, LabeledEdge second, Context ctx, int height) {
		return !first.isSelfLoop() && !second.isSelfLoop();
	}
	
	
	
	/**
	 * Joins two subtrees G1=Gt and G2=Gu to form G3=Gtu.
	 * In particular, the edge u is appended as suffix to G3, since it is
	 * the lexicographically greatest element between t and u.
	 * The connectivity status of G3 is computed during the join phase, in particular
	 * G3 is a subtree in the following cases:
	 * <ul>
	 * 	<li>(G1 is connected AND G2 is connected) AND height&gt;1 AND t linked u</li>
	 * 	<li>(G1 is connected AND G2 is connected) AND height&lt;=1</li>
	 * 	<li>(G1 is not connected OR G2 is not connected) AND t linked u</li>
	 * </ul>
	 */
	@Override
	public LabeledEdge apply(LabeledEdge t, LabeledEdge u, int height) {
		LabeledEdge e = new LabeledEdge(u);
		
		if(t.connected && u.connected) {
			if(height>1) {
				e.connected = (t.isLinkedByStartNode(u) || t.isLinkedByEndNode(u));
			}else {
				e.connected = true;
			}
		}else{
			e.connected = (t.isLinkedByStartNode(u) || t.isLinkedByEndNode(u));
		}
		
		return e;
	}


	
	/**
	 * Returns true if the joined subgraph is connected (and hence a subtree).
	 */
	@Override
	public boolean testPostcondition(LabeledEdge edge, Context ctx, int height) {
		boolean result = true;
		if(height>1) {
			result = edge.connected;
		}
		return result;
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

/**
 * Defines the public interface of pattern evaluation objects allowing
 * the inspection of some closure condition (aka closure system).
 * @author Angelo Impedovo
 */
public interface ClosureEvaluation {
	
	/**
	 * Returns true if the closure condition is satisfied, false otherwise.
	 * @return True if the closure condition is satisfied, false otherwise.
	 */
	public boolean isClosed();
}

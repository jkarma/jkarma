/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

import java.util.Set;
import java.util.TreeSet;

import org.jkarma.mining.joiners.Joiner;
import org.jkarma.mining.joiners.LexicographicJoiner;
import org.jkarma.mining.joiners.SubgraphEdgeLexicographicJoiner;
import org.jkarma.mining.joiners.SubtreeEdgeLexicographicJoiner;
import org.jkarma.model.LabeledEdge;

/**
 * Main class used to access and defining custom mining strategies.
 * The construction of a mining strategy starts with the definition
 * of the language of patterns.
 * @author Angelo Impedovo
 */
public class Strategies {
	
	
	/**
	 * Builds a SymbolicStrategyProxy based on a custom pattern language.
	 * The SymbolicStrategyProxy needs to be further decorated with additional details
	 * in order to build a fully functional pattern mining strategy. 
	 * @param <A> Type of allowed pattern symbols.
	 * @param grammar The lexicographic joiner implementing the pattern language grammar.
	 * @return The SymbolicStrategyProxy based on a custom pattern language.
	 */
	public static <A extends Comparable<A>> SymbolicStrategyProxy<A> upon(Joiner<A> grammar){
		return new SymbolicStrategyProxy<A>(grammar);
	}
	
	
	
	/**
	 * Builds a SymbolicStrategyProxy based on a simple propositional pattern language.
	 * In the propositional pattern language, patterns are simple itemsets whose items 
	 * are objects with a total order.
	 * The SymbolicStrategyProxy needs to be further decorated with additional details
	 * in order to build a fully functional pattern mining strategy. 
	 * @param <A> Type of allowed pattern symbols.
	 * @param items The alphabet of the pattern language.
	 * @return The SymbolicStrategyProxy based on a propositional pattern language.
	 */
	public static <A extends Comparable<A>> SymbolicStrategyProxy<A> uponItemsets(Set<A> items){
		return new SymbolicStrategyProxy<A>(new LexicographicJoiner<A>()).symbols(items);
	}
	
	
	
	/**
	 * Builds a SymbolicStrategyProxy based on a simple propositional pattern language.
	 * In the propositional pattern language, patterns are simple itemsets whose items 
	 * are objects with a total order.
	 * The SymbolicStrategyProxy needs to be further decorated with additional details
	 * in order to build a fully functional pattern mining strategy. 
	 * In this SymbolicStrategyProxy, no initial set of items is known, this method is
	 * equivalent to calling uponItemsets with an empty set of items.
	 * @param <A> Type of allowed pattern symbols.
	 * @return The SymbolicStrategyProxy based on a propositional pattern language.
	 */
	public static <A extends Comparable<A>> SymbolicStrategyProxy<A> uponItemsets(){
		TreeSet<A> set = new TreeSet<A>();
		return Strategies.uponItemsets(set);
	}
	
	
	
	/**
	 * Builds a SymbolicStrategyProxy based on connected subgraphs pattern pattern language.
	 * In the connected subgraphs pattern language, patterns are connected subgraphs 
	 * whose items are labeled edges with a total order.
	 * The SymbolicStrategyProxy needs to be further decorated with additional details
	 * in order to build a fully functional pattern mining strategy. 
	 * @param edges The alphabet of the pattern language.
	 * @return The SymbolicStrategyProxy based on a connected subgraphs pattern language.
	 */
	public static SymbolicStrategyProxy<LabeledEdge> uponSubgraphs(Set<LabeledEdge> edges){
		return new SymbolicStrategyProxy<LabeledEdge>(new SubgraphEdgeLexicographicJoiner()).symbols(edges);
	}
	
	
	
	/**
	 * Builds a SymbolicStrategyProxy based on connected subgraphs pattern pattern language.
	 * In the connected subgraphs pattern language, patterns are connected subgraphs 
	 * whose items are labeled edges with a total order.
	 * The SymbolicStrategyProxy needs to be further decorated with additional details
	 * in order to build a fully functional pattern mining strategy. 
	 * In this SymbolicStrategyProxy, no initial set of items is known, this method is
	 * equivalent to calling uponSubgraphs with an empty set of labeled edges.
	 * @return The SymbolicStrategyProxy based on a connected subgraphs pattern language.
	 */
	public static SymbolicStrategyProxy<LabeledEdge> uponSubgraphs(){
		TreeSet<LabeledEdge> set = new TreeSet<LabeledEdge>();
		return Strategies.uponSubgraphs(set);
	}
	
	
	
	/**
	 * Builds a SymbolicStrategyProxy based on subtrees pattern pattern language.
	 * In the subtrees pattern language, patterns are subtrees whose items 
	 * are labeled edges with a total order.
	 * The SymbolicStrategyProxy needs to be further decorated with additional details
	 * in order to build a fully functional pattern mining strategy. 
	 * @param edges The alphabet of the pattern language.
	 * @return The SymbolicStrategyProxy based on the subtrees pattern language.
	 */
	public static SymbolicStrategyProxy<LabeledEdge> uponSubtrees(Set<LabeledEdge> edges){
		return new SymbolicStrategyProxy<LabeledEdge>(new SubtreeEdgeLexicographicJoiner()).symbols(edges);
	}
	
	
	
	/**
	 * Builds a SymbolicStrategyProxy based on subtrees pattern pattern language.
	 * In the subtrees pattern language, patterns are subtrees whose items 
	 * are labeled edges with a total order.
	 * The SymbolicStrategyProxy needs to be further decorated with additional details
	 * in order to build a fully functional pattern mining strategy. 
	 * In this SymbolicStrategyProxy, no initial set of items is known, this method is
	 * equivalent to calling uponSubtrees with an empty set of labeled edges.
	 * @return The SymbolicStrategyProxy based on the subtrees pattern language.
	 */
	public static SymbolicStrategyProxy<LabeledEdge> uponSubtrees(){
		TreeSet<LabeledEdge> set = new TreeSet<LabeledEdge>();
		return Strategies.uponSubtrees(set);
	}
	
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.events;

import com.google.common.eventbus.Subscribe;

/**
 * This interface prescribes the public interface of a custom Event Listener 
 * specifically designed for listening to itemset mining related events.
 * 
 * @author Angelo Impedovo
 * @param <A> Type expressing the alphabet of allowed itemset symbols.
 * @param <B> Type expressing the evaluation of itemsets.
 */
public interface ItemSetEventListener<A,B> {
	
	/**
	 * Method called when an itemset is added to the lattice.
	 * An itemset is added to the lattice only during commit operations performed
	 * by the MiningStrategy component.
	 * @param event An ItemSetAddedEvent associated to the event.
	 */
	@Subscribe
	public void itemsetAdded(ItemSetAddedEvent<A,B> event);
	
	
	
	/**
	 * Method called when an itemset is removed from the lattice.
	 * An itemset is removed from the lattice only during commit operations performed
	 * by the MiningStrategy component.
	 * @param event An ItemSetRemovedEvent associated to the event.
	 */
	@Subscribe
	public void itemsetRemoved(ItemSetRemovedEvent<A,B> event);
	
	
	
	/**
	 * Method called when an itemset is marked as updated.
	 * An itemset is updated during the mining phase performed by the MiningStrategy component,
	 * an updated itemset is an itemset which is neither added nor removed by the execute method.
	 * @param event An ItemSetUpdatedEvent associated to the event.
	 */
	@Subscribe
	public void itemsetUpdated(ItemSetUpdatedEvent<A,B> event);
	
	
	
	/**
	 * Method called when an itemset is marked to be added to the lattice.
	 * An itemset is marked as added during the mining phase performed by the MiningStrategy component,
	 * such itemset will be added to the lattice by the next commit operation.
	 * @param event An ItemSetAddedRegisteredEvent associated to the event.
	 */
	@Subscribe
	public void addedItemsetRegistered(ItemSetAddedRegisteredEvent<A,B> event);
	
	
	
	/**
	 * Method called when an itemset is marked to be removed from the lattice.
	 * An itemset is marked as removed during the mining phase performed by the MiningStrategy component,
	 * such itemset will be removed from the lattice by the next commit operation.
	 * @param event An ItemSetRemovedRegisteredEvent associated to the event.
	 */
	@Subscribe
	public void removedItemsetRegistered(ItemSetRemovedRegisteredEvent<A,B> event);

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.interfaces;

import java.util.function.Function;

import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.windows.WindowingStrategy;

/**
 * Interface for a provider object.
 * A provider is an object which is able to produced instances of type B
 * starting from objects of type A.
 * The provider act as a mapper (of elements of A into elements of B) and
 * holds a cache of produced elements.
 * Every provider object in jMiner acts as a data accessor as they are 
 * responsible for building evaluation of an 1-itemset (or item) starting 
 * from data.
 * @author Angelo Impedovo
 * @param <A> The type of items to be mapped
 * @param <B> The type of evaluations to be instantiated
 */
public interface Provider<A,B> extends Function<A,Pair<B>>{
	
	
	/**
	 * Forgets the cached elements for a specific item.
	 */
	public void forgetCached();
	
	
	
	/**
	 * Forgets the recent elements for a specific item.
	 */
	public void forgetRecent();
	
	
	/**
	 * Accumulates recent and cached elements according to the windowing strategy.
	 */
	public void flatten();
	
	
	
	/**
	 * Returns the windowing strategy associated to this provider.
	 * @return the windowing strategy associated to this provider.
	 */
	public WindowingStrategy<B> getWindowModel();
	
	
	/**
	 * Returns an empty instance of the type associated
	 * to the provider.
	 * @return A new empty evaluation.
	 */
	public B getNewInstance();
	
}

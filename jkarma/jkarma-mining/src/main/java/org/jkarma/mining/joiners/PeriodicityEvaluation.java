/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.joiners;

/**
 * Defines the public interface of pattern evaluation objects allowing
 * the inspection of some periodicity-related condition.
 * Since the evaluation may rely on different notions of periodicity, the
 * public interface allows to return different informations.
 * @author Angelo Impedovo
 */
public interface PeriodicityEvaluation {
	
	
	
	/**
	 * Returns the minimum periodicity associated with the evaluation.
	 * @return The minimum periodicity associated with the evaluation.
	 */
	public int getMinimumPeriodicity();
	
	
	
	/**
	 * Returns the maximum periodicity associated with the evaluation.
	 * @return The maximum periodicity associated with the evaluation.
	 */
	public int getMaximumPeriodicity();
	
	
	
	/**
	 * Returns the average periodicity associated with the evaluation.
	 * @return The average periodicity associated with the evaluation.
	 */
	public double getAveragePeriodicity();
	
	

	/**
	 * Checks if this evaluation satisfies some periodicity-based condition.
	 * Note that the periodicity-based condition may consider different bounds
	 * on the minimum, the maximum and the average periodicity.
	 * This choice is left to the user.
	 * @param minPer The periodicity lower bound.
	 * @param maxPer The periodicity upper bound.
	 * @param lowAvgPerBound The average periodicity lower bound. 
	 * @param highAvgPerBound The average periodicity upper bound.
	 * @return True if the evaluation exceeds the periodicity-based condition,
	 *  false otherwise.
	 */
	public boolean isPeriodic(int minPer, int maxPer, double lowAvgPerBound, double highAvgPerBound);
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.structures;

/**
 * Enumeration of the possible status of a node in a SETree.
 * In paritcular a pattern can be updated or marked to be deleted
 * or removed by the commit operation in the MiningStrategy class.
 * @author Angelo Impedovo
 *
 */
enum SENodeStatus {
	UPDATED, 
	DELETED,
	ADDED,
	DISCARDED
}

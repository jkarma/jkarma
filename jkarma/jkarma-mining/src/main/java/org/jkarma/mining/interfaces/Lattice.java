/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.interfaces;

import java.util.Collection;
import java.util.stream.Stream;


/**
 * Defines the public interface for a lattice.
 * A lattice is a mathematical entity corresponding to a (partially)
 * ordered set of elements according to an order relation.
 * @author Angelo Impedovo
 *
 * @param <A> Type of elements in the lattice
 */
public interface Lattice<A> extends Iterable<A> {
	
	/**
	 * Returns the number of elements in the lattice.
	 * @return The number of elements in the lattice.
	 */
	public int getCount();
	
	
	
	/**
	 * Returns the greater elements of an element in the lattice 
	 * according to the order relation.
	 * @param element The element of which to search the greater elements
	 * @return The collection of greater elements.
	 */
	public Collection<A> getGreaterElements(A element);
	
	
	
	/**
	 * Returns the lower elements of an element in the lattice 
	 * according to the order relation.
	 * @param element The element of which to search the lower elements
	 * @return The collection of lower elements.
	 */
	public Collection<A> getLowerElements(A element);
	
	
	
	/**
	 * Returns the join element between two other elements in the lattice 
	 * according to the order relation.
	 * @param firstElement The first element.
	 * @param secondElement The second element.
	 * @return The join between the first and the second element if any, null otherwise.
	 */
	public A getJoin(A firstElement, A secondElement);
	
	
	
	/**
	 * Returns the meet element between two other elements in the lattice 
	 * according to the order relation.
	 * @param firstElement The first element.
	 * @param secondElement The second element.
	 * @return The meet between the first and the second element if any, null otherwise.
	 */
	public A getMeet(A firstElement, A secondElement);
	
	
	
	/**
	 * Returns the minimum element in the lattice.
	 * @return The minimum element in the lattice if any, null otherwise.
	 */
	public A getMinimum();
	
	
	
	/**
	 * Returns the maximum element in the lattice.
	 * @return The maximum element in the lattice if any, null otherwise.
	 */
	public A getMaximum();
	
	
	
	/**
	 * Evaluates it the lattice is limited.
	 * A lattice is limited if there are both the maximum and the minimum element.
	 * @return True if the lattice is limited, false otherwise.
	 */
	public boolean isLimited();
	
	
	
	/**
	 * Returns the stream made of the elements belonging to the lattice.
	 * @return The stream made of the elements belonging to the lattice.
	 */
	public Stream<A> stream();
}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.events;

import org.jkarma.mining.interfaces.ItemSet;

/**
 * This class implements the event-related data associated to the update of an existing
 * itemset in the lattice of itemsets by the MiningStrategy component.
 * In particular, this event is raised when an itemset has been updated (the associated
 * evaluation has been updated) by the mining phase in the MiningStrategy.
 * Updated itemsets are neither added nor removed.
 * @author Angelo Impedovo
 * 
 * @param <A> Type expressing the alphabet of allowed itemset symbols.
 * @param <B> Type expressing the evaluation of itemsets.
 */
public class ItemSetUpdatedEvent<A,B> extends ItemSetEvent<A,B> {

	/**
	 * Constructs the event-related data upon a given itemset.
	 * @param itemset The itemset associated to the event.
	 */
	public ItemSetUpdatedEvent(ItemSet<A, B> itemset) {
		super(itemset);
	}

}

/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.interfaces;


/**
 * Defines the public interface for items.
 * Items constitute the alphabet of allowed symbols in itemsets.
 * Every item in an itemset is associated with a proper evaluation.
 * @author Angelo Impedovo
 *
 * @param <A> The type of allowed symbols.
 * @param <B> The type of item evaluation.
 */
public interface Item<A,B> {
	
	/**
	 * Returns the symbol associated to this item.
	 * @return The symbol associated to this item.
	 */
	public A getSymbol();
	
	
	/**
	 * Returns the evaluation associated to this item.
	 * @return The evaluation associated to this item.
	 */
	public B getEvaluation();
}

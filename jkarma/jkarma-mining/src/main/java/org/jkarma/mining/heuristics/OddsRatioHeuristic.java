package org.jkarma.mining.heuristics;

import org.jkarma.mining.joiners.FrequencyEvaluation;
import org.jkarma.mining.providers.Context;

/**
 * This class implements a pattern heuristic evaluation based on the pattern's odds
 * ratio between two time windows.
 * The odds ratio of a pattern is a contrast measure defined as the ratio between 
 * the odds on the first window and the odds on the second one, respectively.
 * Necessary condition to compute the odds ratio is that B implements the
 * FrequencyEvaluation interface. 
 * The odds ratio is computed so to have value in [1,Infinity) by dividing the greatest
 * odds in the two windows with the lowest, the reciprocal version has values in [0,1]. 
 * @author Angelo Impedovo
 *
 * @param <A> Type denoting the alphabet of allowed pattern symbols.
 * @param <B> Type denoting the pattern evaluation.
 * @see <a href='https://dblp.org/rec/bib/books/crc/dong13/Bailey13'>
 * Statistical Measures for Contrast Patterns, Contrast Data Mining: 
 * Concepts, Algorithms, and Applications</a>
 */
public class OddsRatioHeuristic<A, B extends FrequencyEvaluation> implements Heuristic<A,B> {

	/**
	 * If true the heuristic will compute the reciprocal odds ratio in [0,1].
	 * Otherwise the classic odds ratio in [1,Infinity) will be computed.
	 */
	boolean isReciprocal = false;
	
	
	
	/**
	 * Instantiate a odds ratio heuristic.
	 * If the reciprocal flag is set to true, then the reciprocal odds ratio in [0,1]
	 * will be computed. Otherwise, the classic odds ratio in [1, Infinity) will be
	 * computed.
	 * @param reciprocal The reciprocal flag.
	 */
	public OddsRatioHeuristic(boolean reciprocal) {
		this.isReciprocal = reciprocal;
	}

	
	@Override
	public double apply(A suffix, B remoteEval, B recentEval, Context ctx, int itemsetLength, boolean onRemote) {
		double oldFreq = remoteEval.getRelativeFrequency();
		double newFreq = recentEval.getRelativeFrequency();
		double oldOdd = oldFreq/(1-oldFreq);
		double newOdd = newFreq/(1-newFreq);
		
		double value;
		if(!isReciprocal) {
			value = Math.max(oldOdd, newOdd)/Math.min(oldOdd, newOdd); 
		}else {
			value = Math.min(oldOdd, newOdd)/Math.max(oldOdd, newOdd);
		}
		
		return value;
	}

}

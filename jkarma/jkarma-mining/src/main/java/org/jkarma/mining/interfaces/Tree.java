/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.interfaces;

/**
 * Defines the public interface of tree-like data structures.
 * The tree is a collection of nodes organized in a tree-like fashion,
 * in particular a node may possess many children but only a parent node.
 * The tree is said to be rooted in the root node, where the root node
 * is a node without a parent node.
 * @author Angelo Impedovo
 *
 * @param <A> Type of informative content associated to nodes.
 */
public interface Tree<A> {
	
	/**
	 * Returns the root node of the tree.
	 * @return The root node of the tree.
	 */
	public Node<A> getRoot();
}

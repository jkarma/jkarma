/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.mining.providers;

import org.jkarma.mining.joiners.TidSet;
import org.jkarma.mining.structures.Pair;
import org.jkarma.mining.windows.WindowingStrategy;
import org.jkarma.model.Transaction;


/**
 * A provider of TidSet objects starting from transactions of type A.
 * @author Angelo Impedovo
 *
 * @param <A> Type of transactions.
 */
public class NegativeTidSetProvider<A> extends WindowedProvider<A, TidSet> {
	
	private TidSet pastConsumed = new TidSet(0);
	private TidSet newConsumed = new TidSet(0);
	
	
	/**
	 * Constructs a new TidSetProvider based on a given time window model.
	 * @param windowing The time window model.
	 */
	public NegativeTidSetProvider(WindowingStrategy<TidSet> windowing) {
		super(windowing);
	}

	
	
	/**
	 * Returns a new empty TidSet.
	 */
	@Override
	public TidSet getNewInstance() {
		return new TidSet(0);
	}
	
	
	
	/**
	 * Convenience override, we compute negative tidset starting 
	 * from positive ones.
	 */
	@Override
	public Pair<TidSet> apply(A key){
		Pair<TidSet> result = super.apply(key);
		
		TidSet first = result.getAggregate();
		TidSet second = result.getIncrement();
		
		TidSet compFirst = new TidSet(first.getTransactionCount());
		TidSet compSecond = new TidSet(second.getTransactionCount());
		
		//tidset negation based on tids
		this.pastConsumed.stream().filter(p -> !first.contains(p)).forEach(compFirst::add);
		this.newConsumed.stream().filter(p -> !second.contains(p)).forEach(compSecond::add);
		return Pair.of(compFirst, compSecond);
	}

	
	
	/**
	 * Consumes a single transaction updating the recent TidSet objects associated
	 * to items from the transaction. Note that if there's an item that has
	 * never been observed before, the TidSet is created and therefore updated.
	 */
	@Override
	public void accept(Transaction<A> transaction) {
		//we consume only not null transaction
		if(transaction!=null) {
			int tid = transaction.getId();
			
			//we register the received transaction among all the recently consumed ones
			this.newConsumed.add(tid);
			
			//we register the received transactionId for each item 
			for(A item : transaction) {
				Pair<TidSet> pair = super.apply(item);
				TidSet tidset = pair.getIncrement();
				tidset.add(tid);
			}
			
			//we increment the transaction counter
			this.summary.consumedTransactionsCount++;
		}
	}
	
	
	@Override
	public void flatten() {
		super.flatten();

		//we also flatten according to the time window logic
		//also the past consumed tids and the new consumed tids.
		TidSet aggregated = this.windowing.andThen(
			this.pastConsumed, this.newConsumed 
		);
		this.pastConsumed = aggregated;
		this.newConsumed = this.getNewInstance();
	}
	
	
	
	
	/**
	 * Drops every cached evaluation, this method also changes the state
	 * of the mining context by setting the count of cached transactions to 0.
	 */
	@Override
	public void forgetCached() {
		super.forgetCached();
		this.pastConsumed = this.getNewInstance();
	}
	
	
	
	/**
	 * Drops every recent evaluation, this method also changes the state
	 * of the mining context by setting the count of consumed transactions to 0.
	 */
	@Override
	public void forgetRecent() {
		super.forgetRecent();
		this.newConsumed = this.getNewInstance();
	}
}
